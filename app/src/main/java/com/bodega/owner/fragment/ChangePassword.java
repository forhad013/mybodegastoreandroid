package com.bodega.owner.fragment;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.bodega.owner.R;
import com.bodega.owner.activity.DrawerActivity;
import com.bodega.owner.databasemanager.JSONParser;
import com.bodega.owner.databasemanager.Url;
import com.bodega.owner.utils.ConnectionDetector;
import com.bodega.owner.utils.SharePref;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by Nahid on 1/14/2016.
 */
public class ChangePassword extends Fragment {
    LinearLayout changePassword;
    EditText oldPassword, newPassword1, newPassword2;
    String oldPasswordString, newPasswordString;
    TextView TopBarText;

    ImageButton backBtn;

    Typeface mytTypeface, customFont;
    String msg, message, userID, password;
    int success = 0;
    ConnectionDetector cd;
    boolean isInternetOn = false;

    JSONParser jsonParser;
    JSONObject json;
    SharePref sharePref;
    Button change;
    SweetAlertDialog progressSweetAlertDialog, normalDialog, doneDialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_change_password, container, false);

        jsonParser = new JSONParser();
        sharePref = new SharePref(getActivity());
        TopBarText = (TextView) view.findViewById(R.id.barText);
        oldPassword = (EditText) view.findViewById(R.id.oldPass);
        newPassword1 = (EditText) view.findViewById(R.id.newPass1);
        newPassword2 = (EditText) view.findViewById(R.id.newPass2);
        //   change = (Button)view.findViewById(R.id.updatePassword);
        password = sharePref.getshareprefdatastring(SharePref.USERPASSWWORD);
        mytTypeface = Typeface.createFromAsset(getActivity().getAssets(), "fonts/RobotoCondensed-Regular.ttf");

        customFont = Typeface.createFromAsset(getActivity().getAssets(), "fonts/GothamRnd-Book.otf");

    //    TopBarText.setTypeface(mytTypeface);


        backBtn = (ImageButton) view.findViewById(R.id.backBtn);
        changePassword = (LinearLayout) view.findViewById(R.id.done);


        progressSweetAlertDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.PROGRESS_TYPE);
        progressSweetAlertDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        progressSweetAlertDialog.setTitleText("Loading");
        progressSweetAlertDialog.setCancelable(false);


        cd = new ConnectionDetector(getActivity());
        isInternetOn = cd.isConnectingToInternet();

        normalDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE);
        doneDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.SUCCESS_TYPE);

        userID = sharePref.getshareprefdatastring(SharePref.USERID);

        changePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (nullCheck()) {

                    if (newPassword1.getText().toString().trim().equals(newPassword2.getText().toString().trim())) {


                        oldPasswordString = oldPassword.getText().toString().trim();
                        newPasswordString = newPassword1.getText().toString().trim();
                            isInternetOn = cd.isConnectingToInternet();
                            if (isInternetOn) {
                                new AsyncTaskEdit().execute();
                            } else {

                                doneDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
                                doneDialog.setContentText("Please turn on your internet connection");
                                doneDialog.show();
                            }
                            /*
                            doneDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
                            doneDialog.setContentText("Old Password is incorrect");
                            doneDialog.show();*/


                    } else {

                        doneDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
                        doneDialog.setContentText("Password doesn't match");
                        doneDialog.show();

                    }

                }

            }
        });


        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //   Log.i("si:",(DrawerActivity.class).fragmentStack.size()+"");
                FragmentTransaction ft = ((DrawerActivity)getActivity()).mFragmentManager.beginTransaction();

                ((DrawerActivity)getActivity()).fragmentStack.lastElement().onPause();
                ft.remove(((DrawerActivity) getActivity()).fragmentStack.pop());

                ((DrawerActivity)getActivity()).fragmentStack.lastElement().onResume();
                ft.show(((DrawerActivity) getActivity()).fragmentStack.lastElement());
                //  Log.i("bbb",fragmentStack.lastElement()+"");


                ft.commit();
            }
        });

        return view;
    }


//    @Override
//    public void onActivityCreated(Bundle savedInstanceState)
//    {
//        super.onActivityCreated(savedInstanceState);
//
//        FontChangeCrawler fontChanger = new FontChangeCrawler(getActivity().getAssets(), "fonts/HelveticaNeueLTStd-LtCn.otf");
//        fontChanger.replaceFonts((ViewGroup) this.getView());
//    }

    public boolean nullCheck() {
        boolean flag = false;

        if (!oldPassword.getText().toString().trim().equalsIgnoreCase("")) {


            if (!newPassword1.getText().toString().trim().equalsIgnoreCase("")) {

                if (!newPassword1.getText().toString().trim().equalsIgnoreCase("")) {
                    return true;
                } else {
                    msg = "Please give new password!";
                }
            } else {
                msg = "Please give new password!";
            }
        } else {
            msg = "Please give old password!";
        }


        if (!flag) {
            new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE)
                    .setTitleText("Oops...")
                    .setContentText(msg)
                    .show();
        }

        return flag;
    }


    class AsyncTaskEdit extends AsyncTask<String, String, String> {


        @Override
        protected void onPreExecute() {

            try {

                progressSweetAlertDialog.show();
                changePassword.setEnabled(false);
                //   Toast.makeText(getActivity(), rateID, Toast.LENGTH_SHORT).show();
            } catch (IndexOutOfBoundsException e) {
                Toast.makeText(getActivity(), "no data", Toast.LENGTH_SHORT).show();
            }

        }

        @Override
        protected String doInBackground(String... params) {


            List<NameValuePair> pair = new ArrayList<NameValuePair>();
            pair.add(new BasicNameValuePair("userID", userID));
            pair.add(new BasicNameValuePair("newPassword", newPasswordString));
            pair.add(new BasicNameValuePair("oldPassword", oldPasswordString));

            json = jsonParser.makeHttpRequest(Url.CHANGEPASSWORD, "POST", pair);
            Log.e("pair", pair+"");
            Log.e("edit",userID+" "+newPasswordString+" "+ json + "");

            try {

                success = json.getInt("success");
                message = json.getString("message");

                Log.e("success", String.valueOf(success));
                Log.e("msh", message);

            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            progressSweetAlertDialog.dismiss();
            if (success == 1) {

                Log.e("post", "post");
                try {

                    sharePref.setshareprefdatastring(SharePref.USERPASSWWORD, newPasswordString);

                    doneDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.SUCCESS_TYPE);
                    doneDialog.setTitleText("Success");
                    doneDialog.setContentText(message);
                    doneDialog.show();
                    doneDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            Intent intent = new Intent(getActivity(), DrawerActivity.class);

                            startActivity(intent);
                        }
                    });


                } catch (Exception e) {

                }

            } else {
                doneDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.SUCCESS_TYPE);
                doneDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
                doneDialog.setContentText(message);
                doneDialog.show();
            }

            changePassword.setEnabled(true);
        }

    }
}

