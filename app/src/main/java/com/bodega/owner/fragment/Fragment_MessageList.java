package com.bodega.owner.fragment;


import android.content.Intent;
import android.graphics.Typeface;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;


import com.bodega.owner.R;
import com.bodega.owner.activity.DrawerActivity;
import com.bodega.owner.activity.SingleMessageAcitivity;
import com.bodega.owner.adapter.MessageListAdapter;
import com.bodega.owner.adapter.NewMessageListItem;
import com.bodega.owner.databasemanager.MyDatabase;

import java.util.ArrayList;

public class Fragment_MessageList extends Fragment {


    ArrayList<NewMessageListItem>  messageListItemArrayList;

    Typeface mytTypeface,customFont;

    ListView messageList;
    TextView barText;
    TextView emptyText;
    MyDatabase db;
    LinearLayout empty;



    ImageButton backBtn;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Get the view from fragmenttab1.xml
        View view = inflater.inflate(R.layout.activity_message_list, container,
                false);


        backBtn = (ImageButton) view.findViewById(R.id.backBtn);
        messageList = (ListView) view.findViewById(R.id.msgList);

        db = new MyDatabase(getActivity());

        messageListItemArrayList = db.getMessageList();

        emptyText = (TextView) view.findViewById(R.id.emptyText);


        barText = (TextView) view.findViewById(R.id.barText);
        empty = (LinearLayout) view.findViewById(R.id.empty);

        MessageListAdapter messageListAdapter = new MessageListAdapter(getActivity(),messageListItemArrayList);

        mytTypeface = Typeface.createFromAsset(getActivity().getAssets(),"fonts/RobotoCondensed-Regular.ttf");

        customFont = Typeface.createFromAsset(getActivity().getAssets(),"fonts/GothamRnd-Book.otf");

        emptyText.setTypeface(customFont);
        barText.setTypeface(mytTypeface);
        messageList.setAdapter(messageListAdapter);




//        if(messageListAdapter.isEmpty()){
//            empty.setVisibility(View.INVISIBLE);
//        }{
//            messageList.setVisibility(View.GONE);
//        }

        if(messageListItemArrayList.isEmpty()){
            empty.setVisibility(View.VISIBLE);

            messageList.setVisibility(View.INVISIBLE);
        }else{
            messageList.setVisibility(View.VISIBLE);
            empty.setVisibility(View.INVISIBLE);
        }





        messageList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                Intent intent = new Intent(getActivity(), SingleMessageAcitivity.class);
                intent.putExtra("shopID", messageListItemArrayList.get(position).getUserID());
                intent.putExtra("shopName", messageListItemArrayList.get(position).getUserName());

                startActivity(intent);

            }
        });

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //   Log.i("si:",(DrawerActivity.class).fragmentStack.size()+"");
                FragmentTransaction ft = ((DrawerActivity)getActivity()).mFragmentManager.beginTransaction();

                ((DrawerActivity)getActivity()).fragmentStack.lastElement().onPause();
                ft.remove(((DrawerActivity) getActivity()).fragmentStack.pop());

                ((DrawerActivity)getActivity()).fragmentStack.lastElement().onResume();
                ft.show(((DrawerActivity) getActivity()).fragmentStack.lastElement());
                //  Log.i("bbb",fragmentStack.lastElement()+"");


                ft.commit();
            }
        });


return view;
    }


//    @Override
//    public void onActivityCreated(Bundle savedInstanceState)
//    {
//        super.onActivityCreated(savedInstanceState);
//
//        FontChangeCrawler fontChanger = new FontChangeCrawler(getActivity().getAssets(), "fonts/HelveticaNeueLTStd-LtCn.otf");
//        fontChanger.replaceFonts((ViewGroup) this.getView());
//    }

//    @Override
//    public void onBackPressed() {
//
//        Intent intent = new Intent(getActivity(), MainAcitivity.class);
//
//
//
//        startActivity(intent);
//        getActivity().finish();
//
//        super.onBackPressed();
//    }
}
