package com.bodega.owner.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bodega.owner.R;
import com.bodega.owner.activity.ImageUploadActivity;
import com.bodega.owner.databasemanager.MyDatabase;
import com.bodega.owner.utils.SharePref;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.sw926.imagefileselector.ImageCropper;
import com.sw926.imagefileselector.ImageFileSelector;

import java.util.ArrayList;

/**
 * Created by Nahid on 1/19/2016.
 */
public class PictureFragment extends Fragment {


    String currentImageFile;

    String currentImageCategory;

    ImageLoader imageLoader;


    ImageView storePic, legalPic, profilePic;
    //    de.hdodenhof.circleimageview.CircleImageView imageView;
    String officePicString, legalPicString, profilePicString;
    SharePref sharePref;
    GridView gridView;
    MyDatabase db;
    Button upProfilePic, upStorePic, upLegalPic,upAdditionalPic;

    ArrayList additionalPic;

    LinearLayout additionalLin;
    String userID;
    View view1;

    ImageFileSelector mImageFileSelector;
    ImageCropper mImageCropper;

    DisplayImageOptions defaultOptions;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_picture, container, false);
        sharePref = new SharePref(getActivity());

        //the layout you set in `setContentView()`
          view1 = (View) view.findViewById(R.id.pictureLayout);

        profilePic = (ImageView) view.findViewById(R.id.profilePic);
        storePic = (ImageView) view.findViewById(R.id.storePic);
        legalPic = (ImageView) view.findViewById(R.id.legalPic);

        upProfilePic = (Button)view.findViewById(R.id.changeProfilePic);
        upStorePic = (Button)view.findViewById(R.id.updateStorePic);
        upLegalPic = (Button)view.findViewById(R.id.changeLegalPic);

        additionalLin = (LinearLayout) view.findViewById(R.id.additionalLin);

        upAdditionalPic = (Button) view.findViewById(R.id.addOfficeStorePic);

//        upProfilePic.setVisibility(View.INVISIBLE);
//        upStorePic.setVisibility(View.INVISIBLE);
//        upLegalPic.setVisibility(View.INVISIBLE);

        additionalPic = new ArrayList();

         userID = sharePref.getshareprefdatastring(SharePref.USERID);





        profilePicString = sharePref.getshareprefdatastring(SharePref.PROFILEPIC);
        officePicString = sharePref.getshareprefdatastring(SharePref.OFFICEPIC);
        legalPicString = sharePref.getshareprefdatastring(SharePref.LEGALPIC);


        additionalPic = new ArrayList();


        db = new MyDatabase(getActivity());
        additionalPic = db.getImagesList("additional", userID);




        if(additionalPic.size()==0){

          //  additionalLin.setVisibility(View.GONE);
        }
       // additionalPic.add(officePicString);
//
//        ImageAdapter imageAdapter = new ImageAdapter(getActivity(), additionalPic);

  //      gridView.setAdapter(imageAdapter);



//        ImageLoader.getInstance().displayImage(officePicString, storePic1, defaultOptions);


        upProfilePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent intent = new Intent(getActivity(),
                        ImageUploadActivity.class);

                intent.putExtra("token","profilePic");
                intent.putExtra("link",profilePicString);

                startActivity(intent);

            }
        });

        upStorePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent intent = new Intent(getActivity(),
                        ImageUploadActivity.class);

                intent.putExtra("token","officePic");
                intent.putExtra("link",officePicString);

                startActivity(intent);

            }
        });

        upLegalPic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent intent = new Intent(getActivity(),
                        ImageUploadActivity.class);

                intent.putExtra("token","legalPic");
                intent.putExtra("link",legalPicString);
                startActivity(intent);

            }
        });

        upAdditionalPic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent intent = new Intent(getActivity(),
                        ImageUploadActivity.class);

                intent.putExtra("token","additionalPic");


                startActivity(intent);

            }
        });


        setData();

        return view;
    }

//    @Override
//    public void onActivityCreated(Bundle savedInstanceState)
//    {
//        super.onActivityCreated(savedInstanceState);
//
//        FontChangeCrawler fontChanger = new FontChangeCrawler(getActivity().getAssets(), "fonts/HelveticaNeueLTStd-LtCn.otf");
//        fontChanger.replaceFonts((ViewGroup) this.getView());
//    }

    public void setData() {

        additionalPic.clear();


        additionalPic = db.getImagesList("additional", userID);

        defaultOptions = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .build();
      //  ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getActivity())
                //.defaultDisplayImageOptions(defaultOptions)
              //  .build();
   //     ImageLoader.getInstance().init(config);


        ImageLoader.getInstance().displayImage(profilePicString, profilePic, defaultOptions);
//        ImageLoader.getInstance().displayImage(officePicString, storePic1, defaultOptions);
        ImageLoader.getInstance().displayImage(legalPicString, legalPic, defaultOptions);

        ImageLoader.getInstance().displayImage(officePicString, storePic, defaultOptions);



        int sizeInDP = 10;

        int marginInDp = (int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP, sizeInDP, getResources()
                        .getDisplayMetrics());



        LinearLayout picLL = new LinearLayout(getActivity());
        picLL.layout(10, 10, 100, 10);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
//       layoutParams.setMargins(marginInDp, marginInDp, marginInDp, marginInDp);
//
//        ViewGroup.MarginLayoutParams marginParams = new ViewGroup.MarginLayoutParams(storePic.getLayoutParams());
//        marginParams.setMargins(marginInDp, marginInDp, marginInDp, marginInDp);

        layoutParams.setMargins(marginInDp, marginInDp, marginInDp, marginInDp);

        picLL.setLayoutParams(layoutParams);


        picLL.setLayoutParams(layoutParams);
        picLL.setOrientation(LinearLayout.VERTICAL);

        ((ViewGroup) view1).addView(picLL);


        for(int i=0;i<additionalPic.size();i++) {
            ImageView myImage = new ImageView(getActivity());
            LinearLayout.LayoutParams margin = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            margin.setMargins(marginInDp, marginInDp, marginInDp, marginInDp);

            myImage.setLayoutParams(margin);
            myImage.layout(0, 0, 250, 0);



            picLL.addView(myImage);
            Log.e("ns","asa");
            ImageLoader.getInstance().displayImage(additionalPic.get(i).toString(), myImage, defaultOptions);
        }
    }


    public void setImages(){


    }


    @Override
    public void onResume() {


   //     ImageLoader.getInstance().
        setData();
        super.onResume();
    }
}
