package com.bodega.owner.fragment;

import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;

import com.bodega.owner.R;
import com.bodega.owner.databasemanager.JSONParser;
import com.bodega.owner.databasemanager.Url;
import com.bodega.owner.model.DeliveryOption;
import com.bodega.owner.model.JsonModelDelivery;
import com.bodega.owner.model.JsonModelHours;
import com.bodega.owner.model.JsonModelImages;
import com.bodega.owner.utils.ConnectionDetector;
import com.bodega.owner.utils.SharePref;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by Nahid on 1/18/2016.
 */
public class OtherInfo extends Fragment {


    String errorString;
    ConnectionDetector cd;
    int success = 0;
    JSONObject json;
    String msg;
    boolean isInternetOn = false;
    ArrayList timeList, ampmlist;
    ArrayList hours, additaionalImagePath;

    JSONParser jsonParser;
    String userID;
    Button update;

    SweetAlertDialog progressSweetAlertDialog, normalDialog,doneDialog;
    boolean d0,d1, d2, d3, d4;

    String d2value, d3value, d4value;

    Spinner time11, time12, time21, time22, time31, time32, time41, time42, time51, time52, time61, time62, time71, time72;

    Spinner ampm11, ampm12, ampm21, ampm22, ampm31, ampm32, ampm41, ampm42, ampm51, ampm52, ampm61, ampm62, ampm71, ampm72;

    ArrayList<DeliveryOption> deliveryOptionArrayList;
    DeliveryOption deliveryOption;

    EditText dOptionValue2, dOptionValue3, dOptionValue4;
    ImageButton edit, cancel;
    CheckBox checkBox0,checkBox1, checkBox2, checkBox3, checkBox4;
    ArrayList<Integer> timeListInt;
    String dayString1, dayString2, dayString3, dayString4, dayString5, dayString6, dayString7;
    EditText legalName;
    SharePref sharePref;
    ArrayAdapter<String> mondayAdapter1, mondayAdapter2, mondayAdapter3, mondayAdapter4, tuesdayAdapter1, tuesdayAdapter2, wednesdayAdapter1, wednesdayAdapter2, thusdayAdapter1, thusdayAdapter2, fridayAdapter1, fridayAdapter2, saturdayAdapter1, sundayAdapter1, saturdayAdapter2, sundayAdapter2;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_store_info, container, false);
        sharePref = new SharePref(getActivity());

        edit = (ImageButton)view.findViewById(R.id.editStore);
        cancel = (ImageButton)view.findViewById(R.id.cancelStore);

        checkBox0 = (CheckBox) view.findViewById(R.id.deliveryOption0);

        checkBox1 = (CheckBox) view.findViewById(R.id.deliveryOption1);

        checkBox2 = (CheckBox) view.findViewById(R.id.deliveryOption2);

        checkBox3 = (CheckBox) view.findViewById(R.id.deliveryOption3);

        checkBox4 = (CheckBox) view.findViewById(R.id.deliveryOption4);

        dOptionValue2 = (EditText) view.findViewById(R.id.dOptionValue2);

        dOptionValue3 = (EditText) view.findViewById(R.id.dOptionValue3);
        dOptionValue4 = (EditText) view.findViewById(R.id.dOptionValue4);

        cd = new ConnectionDetector(getActivity());
        deliveryOptionArrayList = new ArrayList<>();


        isInternetOn = cd.isConnectingToInternet();

        time11 = (Spinner) view.findViewById(R.id.spinnerAM1);
        time12 = (Spinner) view.findViewById(R.id.spinnerPM1);

        time21 = (Spinner) view.findViewById(R.id.spinnerAM2);
        time22 = (Spinner) view.findViewById(R.id.spinnerPM2);

        time31 = (Spinner) view.findViewById(R.id.spinnerAM3);
        time32 = (Spinner) view.findViewById(R.id.spinnerPM3);

        time41 = (Spinner) view.findViewById(R.id.spinnerAM4);
        time42 = (Spinner) view.findViewById(R.id.spinnerPM4);

        time51 = (Spinner) view.findViewById(R.id.spinnerAM5);
        time52 = (Spinner) view.findViewById(R.id.spinnerPM5);

        time61 = (Spinner) view.findViewById(R.id.spinnerAM6);
        time62 = (Spinner) view.findViewById(R.id.spinnerPM6);

        time71 = (Spinner) view.findViewById(R.id.spinnerAM7);
        time72 = (Spinner) view.findViewById(R.id.spinnerPM7);


        userID = sharePref.getshareprefdatastring(SharePref.USERID);
        ampm11 = (Spinner) view.findViewById(R.id.ampm11);
        ampm12 = (Spinner) view.findViewById(R.id.ampm12);

        ampm21 = (Spinner) view.findViewById(R.id.ampm21);
        ampm22 = (Spinner) view.findViewById(R.id.ampm22);

        ampm31 = (Spinner) view.findViewById(R.id.ampm31);
        ampm32 = (Spinner) view.findViewById(R.id.ampm32);

        ampm41 = (Spinner) view.findViewById(R.id.ampm41);
        ampm42 = (Spinner) view.findViewById(R.id.ampm42);

        ampm51 = (Spinner) view.findViewById(R.id.ampm51);
        ampm52 = (Spinner) view.findViewById(R.id.ampm52);

        ampm61 = (Spinner) view.findViewById(R.id.ampm61);
        ampm62 = (Spinner) view.findViewById(R.id.ampm62);

        ampm71 = (Spinner) view.findViewById(R.id.ampm71);
        ampm72 = (Spinner) view.findViewById(R.id.ampm72);
        timeListInt = new ArrayList<>();

        update = (Button) view.findViewById(R.id.update);
        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(isInternetOn){
                    setHours();
                    setDeliveryOption();

                    new AsyncTaskRunnerDetails().execute();
                }

            }
        });

        ampmlist = new ArrayList();

        timeList = new ArrayList();

        hours = new ArrayList();


        timeList.add("00.00");
        timeList.add("01.00");
        timeList.add("02.00");
        timeList.add("03.00");
        timeList.add("04.00");
        timeList.add("05.00");
        timeList.add("06.00");
        timeList.add("07.00");
        timeList.add("08.00");
        timeList.add("09.00");
        timeList.add("10.00");
        timeList.add("11.00");
        timeList.add("12.00");


        progressSweetAlertDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.PROGRESS_TYPE);
        progressSweetAlertDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        progressSweetAlertDialog.setTitleText("Loading");
        progressSweetAlertDialog.setCancelable(false);


        d0 = sharePref.getshareprefdataBoolean(SharePref.D_OPTION0);
        d1 = sharePref.getshareprefdataBoolean(SharePref.D_OPTION1);
        d2 = sharePref.getshareprefdataBoolean(SharePref.D_OPTION2);
        d3 = sharePref.getshareprefdataBoolean(SharePref.D_OPTION3);
        d4 = sharePref.getshareprefdataBoolean(SharePref.D_OPTION4);

        d2value = sharePref.getshareprefdatastring(SharePref.D_OPTION2_VALUE);
        d3value = sharePref.getshareprefdatastring(SharePref.D_OPTION3_VALUE);
        d4value = sharePref.getshareprefdatastring(SharePref.D_OPTION4_VALUE);

        dayString1 = sharePref.getshareprefdatastring(SharePref.MONDAY);
        dayString2 = sharePref.getshareprefdatastring(SharePref.TUESDAY);
        dayString3 = sharePref.getshareprefdatastring(SharePref.WEDNESSDAY);
        dayString4 = sharePref.getshareprefdatastring(SharePref.THUSDAY);
        dayString5 = sharePref.getshareprefdatastring(SharePref.FRIDAY);
        dayString6 = sharePref.getshareprefdatastring(SharePref.SATURDAY);
        dayString7 = sharePref.getshareprefdatastring(SharePref.SUNDAY);
        Log.e("day", dayString1 + " & " + dayString2 + "&" + dayString3);

        String[] shopTimes1 = dayString1.split("\\ - ");
        String[] startTime1 = shopTimes1[0].split("\\|");
        String[] endTime1 = shopTimes1[1].split("\\|");

        timeListInt.add(getTime(startTime1[0]));
        timeListInt.add(getAMPM(startTime1[1]));
        timeListInt.add(getTime(endTime1[0]));
        timeListInt.add(getAMPM(endTime1[1]));



        Log.e("asd", startTime1[0] + startTime1[1]);

        String[] shopTimes2 = dayString2.split("\\ - ");
        String[] startTime2 = shopTimes2[0].split("\\|");
        String[] endTime2 = shopTimes2[1].split("\\|");

        timeListInt.add(getTime(startTime2[0]));
        timeListInt.add(getAMPM(startTime2[1]));
        timeListInt.add(getTime(endTime2[0]));
        timeListInt.add(getAMPM(endTime2[1]));


        String[] shopTimes3 = dayString3.split("\\ - ");
        String[] startTime3 = shopTimes3[0].split("\\|");
        String[] endTime3 = shopTimes3[1].split("\\|");

        timeListInt.add(getTime(startTime3[0]));
        timeListInt.add(getAMPM(startTime3[1]));
        timeListInt.add(getTime(endTime3[0]));
        timeListInt.add(getAMPM(endTime3[1]));



        String[] shopTimes4 = dayString4.split("\\ - ");
        String[] startTime4 = shopTimes4[0].split("\\|");
        String[] endTime4 = shopTimes4[1].split("\\|");

        timeListInt.add(getTime(startTime4[0]));
        timeListInt.add(getAMPM(startTime4[1]));
        timeListInt.add(getTime(endTime4[0]));
        timeListInt.add(getAMPM(endTime4[1]));



        String[] shopTimes5 = dayString5.split("\\ - ");
        String[] startTime5 = shopTimes5[0].split("\\|");
        String[] endTime5 = shopTimes5[1].split("\\|");

        timeListInt.add(getTime(startTime5[0]));
        timeListInt.add(getAMPM(startTime5[1]));
        timeListInt.add(getTime(endTime5[0]));
        timeListInt.add(getAMPM(endTime5[1]));


        String[] shopTimes6 = dayString6.split("\\ - ");
        String[] startTime6 = shopTimes6[0].split("\\|");
        String[] endTime6 = shopTimes6[1].split("\\|");

        timeListInt.add(getTime(startTime6[0]));
        timeListInt.add(getAMPM(startTime6[1]));
        timeListInt.add(getTime(endTime6[0]));
        timeListInt.add(getAMPM(endTime6[1]));


        String[] shopTimes7 = dayString7.split("\\ - ");
        String[] startTime7 = shopTimes7[0].split("\\|");
        String[] endTime7 = shopTimes7[1].split("\\|");

        timeListInt.add(getTime(startTime7[0]));
        timeListInt.add(getAMPM(startTime7[1]));
        timeListInt.add(getTime(endTime7[0]));
        timeListInt.add(getAMPM(endTime7[1]));

        ArrayAdapter<String> spinAdapter1 = new ArrayAdapter<String>(getActivity(),
                R.layout.spinner_item, timeList);


        time11.setAdapter(spinAdapter1);
        time12.setAdapter(spinAdapter1);

        time21.setAdapter(spinAdapter1);
        time22.setAdapter(spinAdapter1);

        time31.setAdapter(spinAdapter1);
        time32.setAdapter(spinAdapter1);

        time41.setAdapter(spinAdapter1);
        time42.setAdapter(spinAdapter1);

        time51.setAdapter(spinAdapter1);
        time52.setAdapter(spinAdapter1);

        time61.setAdapter(spinAdapter1);
        time62.setAdapter(spinAdapter1);

        time71.setAdapter(spinAdapter1);
        time72.setAdapter(spinAdapter1);

        update.setVisibility(View.INVISIBLE);


        ampmlist.add("am");

        ampmlist.add("pm");

        ArrayAdapter<String> spinAdapter2 = new ArrayAdapter<String>(getActivity(),
                R.layout.spinner_item, ampmlist);




        ampm11.setAdapter(spinAdapter2);
        ampm12.setAdapter(spinAdapter2);

        ampm21.setAdapter(spinAdapter2);
        ampm22.setAdapter(spinAdapter2);

        ampm31.setAdapter(spinAdapter2);
        ampm32.setAdapter(spinAdapter2);

        ampm41.setAdapter(spinAdapter2);
        ampm42.setAdapter(spinAdapter2);

        ampm51.setAdapter(spinAdapter2);
        ampm52.setAdapter(spinAdapter2);

        ampm61.setAdapter(spinAdapter2);
        ampm62.setAdapter(spinAdapter2);

        ampm71.setAdapter(spinAdapter2);
        ampm72.setAdapter(spinAdapter2);
        setData();


        setCheckBox();


        checkBox1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {

                    checkBox2.setChecked(false);
                    checkBox3.setChecked(false);
                    checkBox0.setChecked(false);

                    checkBox2.setEnabled(false);
                    checkBox3.setEnabled(false);


                } else {
                    checkBox2.setEnabled(true);
                    checkBox3.setEnabled(true);
                    checkBox4.setEnabled(true);
                }

            }
        });

        checkBox2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {

                    checkBox1.setChecked(false);

                    checkBox0.setChecked(false);
                    checkBox1.setEnabled(false);

                    checkBox3.setChecked(false);


                    checkBox3.setEnabled(false);


                } else {



                    checkBox1.setEnabled(true);
                    checkBox3.setEnabled(true);

                }

            }
        });

        checkBox0.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    checkBox1.setChecked(false);

                    checkBox1.setEnabled(false);
                    checkBox2.setChecked(false);

                    checkBox2.setEnabled(false);
                    checkBox3.setChecked(false);

                    checkBox3.setEnabled(false);
                    checkBox4.setChecked(false);

                    checkBox4.setEnabled(false);

                } else {



                    checkBox1.setEnabled(true);
                    checkBox2.setEnabled(true);
                    checkBox3.setEnabled(true);
                    checkBox4.setEnabled(true);

                }

            }
        });

        checkBox3.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {



                    checkBox1.setChecked(false);


                    checkBox1.setEnabled(false);

                    checkBox2.setChecked(false);
                    checkBox0.setChecked(false);

                    checkBox2.setEnabled(false);

                } else {

                    checkBox2.setEnabled(true);
                    checkBox1.setEnabled(true);


                }

            }
        });






        cancel.setVisibility(View.GONE);

        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                setEnableOrDisable(true);
                cancel.setVisibility(View.VISIBLE);
                update.setVisibility(View.VISIBLE);

                doneDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.NORMAL_TYPE);
                doneDialog.setTitleText("EDIT NOW");
                doneDialog.setContentText("You can now edit your information");
                doneDialog.show();
                doneDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {


                        doneDialog.dismiss();
                    }
                });

            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                setEnableOrDisable(false);
                cancel.setVisibility(View.GONE);
                update.setVisibility(View.GONE);
                setData();
            }
        });

        setData();
        return view;
    }


    public void setData() {

        checkBox0.setChecked(d0);
        checkBox1.setChecked(d1);
        checkBox2.setChecked(d2);
        checkBox3.setChecked(d3);
        checkBox4.setChecked(d4);


        if (d2) {

            dOptionValue2.setText(d2value);
        }

        if (d3) {

            dOptionValue3.setText(d3value);
        }

        if (d4) {

            dOptionValue4.setText(d4value);
        }
        setTimeList();

    }


    public void setTimeList() {


        for (int i = 0; i < timeListInt.size(); i++) {

            //  Log.e("time",timeListInt.get(i)+"");
        }


        time11.setSelection(timeListInt.get(0));
        ampm11.setSelection(timeListInt.get(1));
        time12.setSelection(timeListInt.get(2));
        ampm12.setSelection(timeListInt.get(3));

        time21.setSelection(timeListInt.get(4));
        ampm21.setSelection(timeListInt.get(5));
        time22.setSelection(timeListInt.get(6));
        ampm22.setSelection(timeListInt.get(7));

        time31.setSelection(timeListInt.get(8));
        ampm31.setSelection(timeListInt.get(9));
        time32.setSelection(timeListInt.get(10));
        ampm32.setSelection(timeListInt.get(11));

        time41.setSelection(timeListInt.get(12));
        ampm41.setSelection(timeListInt.get(13));
        time42.setSelection(timeListInt.get(14));
        ampm42.setSelection(timeListInt.get(15));

        time51.setSelection(timeListInt.get(16));
        ampm51.setSelection(timeListInt.get(17));
        time52.setSelection(timeListInt.get(18));
        ampm52.setSelection(timeListInt.get(19));

        time61.setSelection(timeListInt.get(20));
        ampm61.setSelection(timeListInt.get(21));
        time62.setSelection(timeListInt.get(22));
        ampm62.setSelection(timeListInt.get(23));

        time71.setSelection(timeListInt.get(24));
        ampm71.setSelection(timeListInt.get(25));
        time72.setSelection(timeListInt.get(26));
        ampm72.setSelection(timeListInt.get(27));

        setEnableOrDisable(false);
    }
//
//    @Override
//    public void onActivityCreated(Bundle savedInstanceState)
//    {
//        super.onActivityCreated(savedInstanceState);
//
//        FontChangeCrawler fontChanger = new FontChangeCrawler(getActivity().getAssets(), "fonts/HelveticaNeueLTStd-LtCn.otf");
//        fontChanger.replaceFonts((ViewGroup) this.getView());
//    }

    public void setCheckBox(){

        if (checkBox3.isChecked()) {


            checkBox2.setChecked(false);


            checkBox2.setEnabled(false);


        } else {

            checkBox2.setEnabled(true);

        }


        if (checkBox0.isChecked()) {

            checkBox1.setChecked(false);

            checkBox1.setEnabled(false);
            checkBox2.setChecked(false);

            checkBox2.setEnabled(false);
            checkBox3.setChecked(false);

            checkBox3.setEnabled(false);
            checkBox4.setChecked(false);

            checkBox4.setEnabled(false);

        } else {



            checkBox1.setEnabled(true);


            checkBox2.setEnabled(true);


            checkBox3.setEnabled(true);


            checkBox4.setEnabled(true);

        }



        if (checkBox1.isChecked()) {

            checkBox2.setChecked(false);
            checkBox3.setChecked(false);


            checkBox2.setEnabled(false);
            checkBox3.setEnabled(false);


        } else {
            checkBox2.setEnabled(true);
            checkBox3.setEnabled(true);
            checkBox4.setEnabled(true);
        }

        if (checkBox2.isChecked()) {

            checkBox1.setChecked(false);


            checkBox1.setEnabled(false);

            checkBox3.setChecked(false);


            checkBox3.setEnabled(false);
        }
    }

    public int getTime(String data) {

        int position = 0;

        switch (data) {
            case "00.00":

                position = 0;
                break;
            case "01.00":

                position = 1;
                break;
            case "02.00":
                position = 2;
                break;

            case "03.00":
                position = 3;

                break;
            case "04.00":
                position = 4;

                break;

            case "05.00":
                position = 5;
                break;
            case "06.00":
                position = 6;
                break;

            case "07.00":
                position = 7;

                break;
            case "08.00":
                position = 8;

                break;
            case "09.00":
                position = 9;
                break;
            case "10.00":
                position = 10;
                break;
            case "11.00":
                position = 11;
                break;

            case "12.00":
                position = 12;

                break;
            default:
                break;



        }
        return position;
    }

    public int getAMPM(String data) {

        int position = 0;

        switch (data) {
            case "am":

                position = 0;
                break;
            case "pm":

                position = 1;
                break;

            default:
                break;



        }
        return position;
    }


    public void setEnableOrDisable(boolean value){

        checkBox0.setEnabled(value);
        checkBox1.setEnabled(value);
        checkBox2.setEnabled(value);
        checkBox3.setEnabled(value);
        checkBox4.setEnabled(value);

        dOptionValue2.setEnabled(value);
        dOptionValue3.setEnabled(value);
        dOptionValue4.setEnabled(value);


        time11.setEnabled(value);
        time12 .setEnabled(value);
        time21.setEnabled(value);
        time22.setEnabled(value);

        time31.setEnabled(value);
        time32.setEnabled(value);

        time41.setEnabled(value);
        time42.setEnabled(value);

        time51.setEnabled(value);
        time52.setEnabled(value);

        time61.setEnabled(value);
        time62.setEnabled(value);

        time71.setEnabled(value);
        time72.setEnabled(value);


        ampm11.setEnabled(value);
        ampm12.setEnabled(value);

        ampm21.setEnabled(value);
        ampm22.setEnabled(value);

        ampm31.setEnabled(value);
        ampm32.setEnabled(value);

        ampm41.setEnabled(value);
        ampm42.setEnabled(value);

        ampm51.setEnabled(value);
        ampm52.setEnabled(value);

        ampm61.setEnabled(value);
        ampm62.setEnabled(value);

        ampm71.setEnabled(value);
        ampm72.setEnabled(value);

      //  setCheckBox();

    }


    class AsyncTaskRunnerDetails extends AsyncTask<String, String, String> {

        int success = 5, mailcheck = 5;

        HttpResponse response;
        JSONObject response1;
        String responseBody;
        Boolean availableProduct, availablemail;

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub
            {

                HttpClient httpClient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost(
                        Url.UPDATE);

                httpPost.setHeader("content-type", "application/json");
                httpPost.setHeader("Accept", "application/json");
                //  JSONObject jsonObject1 = new JSONObject();
                JSONObject jsonObject = new JSONObject();


                ArrayList<JsonModelDelivery> jsonDelivery = new ArrayList<>();

                ArrayList<JsonModelImages> jsonImagesList = new ArrayList<>();
                ArrayList<JsonModelHours> jsonHours = new ArrayList<>();

           //     setDeliveryOption();





                try{



                    for(int i=0;i<5;i++){
                        JsonModelDelivery delivery = new JsonModelDelivery();
                        delivery.optionName = deliveryOptionArrayList.get(i).optionName;
                        delivery.value  = deliveryOptionArrayList.get(i).value+"";

                        delivery.amount  = deliveryOptionArrayList.get(i).amount;
                        jsonDelivery.add(delivery);


                    }


                    JSONArray jsonArray = new JSONArray();
                    for (int i=0;i < jsonDelivery.size();i++){
                        jsonArray.put(jsonDelivery.get(i).getJSONObject());
                    }

                    for(int i=0;i<7;i++){
                        JsonModelHours jsonModelHours = new JsonModelHours();
                        jsonModelHours.time = hours.get(i).toString();

                        jsonHours.add(jsonModelHours);


                        //Log.e("su",json_submodel.PID[0]+"");

                    }




                    JSONArray jsonArrayHours = new JSONArray();
                    for (int i=0;i < 7;i++){
                        jsonArrayHours.put(jsonHours.get(i).getJSONObject());
                    }





                    jsonObject.accumulate("storeUpdateType", "storeInfo");

                    jsonObject.accumulate("userId", userID);


                    jsonObject.accumulate("userHours",jsonArrayHours);

                    jsonObject.accumulate("userDeliveryFees",jsonArray);
                    jsonObject.accumulate("userType","1");








                    Log.e("res",jsonObject.toString()+"");

                }
                catch (Exception e) {
//
//                    Log.d("InputStream", e.getLocalizedMessage());

                }

                StringEntity entity = null;




                try {
                    entity = new StringEntity( jsonObject.toString());
                    entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                    // Log.d("ch",entity+"");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                httpPost.setEntity(entity);

                try {
                    //  response = httpClient.execute(httpPost);
//                    resC++;
//                    Log.e("Value of response count: ", resC + "");
                    ResponseHandler<String> responseHandler=new BasicResponseHandler();
                    responseBody =httpClient.execute(httpPost, responseHandler);
                    response1=new JSONObject(responseBody);
                    Log.e("res", responseBody + "");





                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }


        @Override
        protected void onPostExecute(String string) {

            progressSweetAlertDialog.dismiss();

            int success;
            String msg = "";
            try {
                success =  response1.getInt("success");

                msg = response1.getString("message");

                if(success==1){


                    doneDialog=  new SweetAlertDialog(getActivity(),SweetAlertDialog.SUCCESS_TYPE);
                    doneDialog.setTitleText("Success");
                    doneDialog.setContentText(msg);
                    doneDialog.show();
                    doneDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {

                            doneDialog.dismiss();

                            setEnableOrDisable(false);
                            update.setVisibility(View.INVISIBLE);
                            setPref();
                        }
                    });


                }else if(success ==0){

                    getError(response1);



                    doneDialog=  new SweetAlertDialog(getActivity(),SweetAlertDialog.ERROR_TYPE);


                    doneDialog.setContentText(errorString);
                    doneDialog.show();
                    doneDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            sweetAlertDialog.dismiss();
                        }
                    });

                }



            }catch (JSONException e) {
                e.printStackTrace();
            }


            update.setEnabled(true);



        }



        @Override
        protected void onPreExecute() {

            progressSweetAlertDialog.show();
            update.setEnabled(false);

        }

    }

    public void setHours(){


        hours.add(time11.getSelectedItem().toString() + "|" + ampm11.getSelectedItem().toString()+ " - " + time12.getSelectedItem().toString() + "|" + ampm12.getSelectedItem().toString());
        hours.add(time21.getSelectedItem().toString() + "|" + ampm21.getSelectedItem().toString()+ " - " + time22.getSelectedItem().toString() + "|" + ampm22.getSelectedItem().toString());
        hours.add(time31.getSelectedItem().toString() + "|" + ampm31.getSelectedItem().toString()+ " - " + time32.getSelectedItem().toString() + "|" + ampm32.getSelectedItem().toString());
        hours.add(time41.getSelectedItem().toString() + "|" + ampm41.getSelectedItem().toString()+ " - " + time42.getSelectedItem().toString() + "|" + ampm42.getSelectedItem().toString());
        hours.add(time51.getSelectedItem().toString() + "|" + ampm51.getSelectedItem().toString()+ " - " + time52.getSelectedItem().toString() + "|" + ampm52.getSelectedItem().toString());
        hours.add(time61.getSelectedItem().toString() + "|" + ampm61.getSelectedItem().toString()+ " - " + time62.getSelectedItem().toString() + "|" + ampm62.getSelectedItem().toString());
        hours.add(time71.getSelectedItem().toString() + "|" + ampm71.getSelectedItem().toString()+ " - " + time72.getSelectedItem().toString() + "|" + ampm72.getSelectedItem().toString());

        for(int i =0;i<7;i++){


            Log.e("time"+i,hours.get(i).toString());
        }

    }


    public void getError(JSONObject json){

        String newline = System.getProperty("line.separator");

        try {
            JSONObject result = json.getJSONObject("results");


            JSONArray errors= result.getJSONArray("errors");

            for (int i=0;i<errors.length();i++){

                JSONObject temp = errors.getJSONObject(i);

                errorString = errorString + "* " +  temp.getString("errorMessage") +newline;
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void setDeliveryOption(){

        deliveryOptionArrayList.clear();

        d0 = checkBox0.isChecked();
        d1 = checkBox1.isChecked();
        d2 = checkBox2.isChecked();
        d3 = checkBox3.isChecked();
        d4 = checkBox4.isChecked();

        if(checkBox2.isChecked()){
            d2value = dOptionValue2.getText().toString();

        }else{
            d2value = "00";
            dOptionValue2.setText("");

        }
        if(checkBox3.isChecked()){
            d3value = dOptionValue3.getText().toString();
        }else{
            d3value = "00";
            dOptionValue3.setText("");

        }
        if(checkBox4.isChecked()){
            d4value = dOptionValue4.getText().toString();
        }else{
            d4value = "00";
            dOptionValue4.setText("");
        }


        deliveryOption = new DeliveryOption("d0",d0,"0");
        deliveryOptionArrayList.add(deliveryOption);

        deliveryOption = new DeliveryOption("d1",d1,"0");
        deliveryOptionArrayList.add(deliveryOption);

        deliveryOption = new DeliveryOption("d2",d2,d2value);
        deliveryOptionArrayList.add(deliveryOption);

        deliveryOption = new DeliveryOption("d3",d3,d3value);
        deliveryOptionArrayList.add(deliveryOption);
        deliveryOption = new DeliveryOption("d4",d4, d4value);
        deliveryOptionArrayList.add(deliveryOption);


    }


    public void setPref(){

        sharePref.setshareprefdataBoolean(SharePref.D_OPTION0,d0);
        sharePref.setshareprefdataBoolean(SharePref.D_OPTION1,d1);
        sharePref.setshareprefdataBoolean(SharePref.D_OPTION2,d2);

        sharePref.setshareprefdataBoolean(SharePref.D_OPTION3,d3);
        sharePref.setshareprefdataBoolean(SharePref.D_OPTION4,d4);

        sharePref.setshareprefdatastring(SharePref.D_OPTION2_VALUE, d2value);
        sharePref.setshareprefdatastring(SharePref.D_OPTION3_VALUE, d3value);
        sharePref.setshareprefdatastring(SharePref.D_OPTION4_VALUE,d4value);

        sharePref.setshareprefdatastring(SharePref.MONDAY,hours.get(0).toString());
        sharePref.setshareprefdatastring(SharePref.TUESDAY,hours.get(1).toString());
        sharePref.setshareprefdatastring(SharePref.WEDNESSDAY,hours.get(2).toString());
        sharePref.setshareprefdatastring(SharePref.THUSDAY,hours.get(3).toString());
        sharePref.setshareprefdatastring(SharePref.FRIDAY,hours.get(4).toString());
        sharePref.setshareprefdatastring(SharePref.SATURDAY,hours.get(5).toString());
        sharePref.setshareprefdatastring(SharePref.SUNDAY,hours.get(6).toString());


        Log.e("dataCheck", hours.get(0).toString());
    }

}
