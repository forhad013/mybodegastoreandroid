package com.bodega.owner.fragment;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bodega.owner.R;
import com.bodega.owner.databasemanager.JSONParser;
import com.bodega.owner.databasemanager.Url;
import com.bodega.owner.utils.ConnectionDetector;
import com.bodega.owner.utils.SharePref;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by Nahid on 1/18/2016.
 */
public class GeneralInfo extends Fragment {

    TextView firstNameTextT, lastNameTextT, emailTextT, phoneTextT, storeNameTextT, storeLegalNameTextT, storeWebAddressTextT, storeAddressTextT, ownerTaxAmountTextT, storeWebAddressText, ownerLandTextT;
    EditText firstNameText, lastNameText, emailText, phoneText, storeNameText, storeLegalNameText,  ownerTaxAmountText, ownerLandLineText;
    ScrollView mainLayout;
    SweetAlertDialog progressSweetAlertDialog, normalDialog,doneDialog;

    AutoCompleteTextView storeAddressText;
    TextView nearestTxt;
    Button update;
    ImageButton editbtn, cancelbtn;

    LinearLayout webAddressHyperLink;

    Spinner callOptionSpinner;
    String firstNameString, lastNameString,emailString,phoneString, userIdString, storeAddressString,  storeNameString, storeLegalNameString, storeWebAddressString, owmerTaxAmountString, ownerLandString ;

    public boolean editable = false;
    ConnectionDetector cd;
    boolean isInternetOn=false;


    AutoCompleteTextView shopAddress;

    SharePref sharePref;
    Typeface mytTypeface, customFont;
    JSONParser jsonParser;
    JSONObject json;
    int success;
    String message;

    EditText password1,password2;
    String passwordString1,passwordString2;

    LinearLayout passwordLin1,passwordLin2;
    private static final String LOG_TAG = "Google Places Autocomplete";

    private static final String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place";

    private static final String TYPE_AUTOCOMPLETE = "/autocomplete";

    private static final String OUT_JSON = "/json";

    String callOptionString;

    private static final String API_KEY =   "AIzaSyB_wVRhZ1qu9IVJO3JyS_fIPIfOkoLSjUA";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_general_info, container, false);

        mainLayout = (ScrollView) view.findViewById(R.id.main);
        sharePref = new SharePref(getActivity());
        jsonParser = new JSONParser();

        firstNameText = (EditText) view.findViewById(R.id.ownerFirstName);
        lastNameText = (EditText) view.findViewById(R.id.ownerLastName);
        emailText = (EditText) view.findViewById(R.id.ownerEmail);
        phoneText = (EditText) view.findViewById(R.id.ownerContact);
        ownerLandLineText = (EditText) view.findViewById(R.id.ownerLand);
        storeNameText = (EditText) view.findViewById(R.id.ownerStoreName);
        storeAddressText = (AutoCompleteTextView) view.findViewById(R.id.ownerStoreAddress);
        storeLegalNameText = (EditText) view.findViewById(R.id.ownerStoreLegalName);
        storeWebAddressText = (TextView) view.findViewById(R.id.ownerStoreWebAddress);


        ownerTaxAmountText = (EditText) view.findViewById(R.id.ownerTaxAmount);

        password1 = (EditText) view.findViewById(R.id.password1);
        password2 = (EditText) view.findViewById(R.id.password2);

        passwordLin1 = (LinearLayout) view.findViewById(R.id.passLin1);
        passwordLin2 = (LinearLayout) view.findViewById(R.id.passLin2);

        editbtn = (ImageButton)view.findViewById(R.id.editGeneral);
        cancelbtn = (ImageButton) view.findViewById(R.id.cancelGeneral);
        cancelbtn.setVisibility(View.GONE);
        update = (Button)view.findViewById(R.id.updateOwnerInfo);

        webAddressHyperLink = (LinearLayout) view.findViewById(R.id.webAddressHyperLink);

        callOptionSpinner = (Spinner) view.findViewById(R.id.callOption);


        disableAll();


        cd = new ConnectionDetector(getActivity());

        isInternetOn = cd.isConnectingToInternet();
// addressString,  storeNameString, storeLegalNameString, storeWebAddressString, owmerTaxAmountString;
        mytTypeface = Typeface.createFromAsset(getActivity().getAssets(), "fonts/RobotoCondensed-Regular.ttf");
        customFont = Typeface.createFromAsset(getActivity().getAssets(), "fonts/GothamRnd-Book.otf");
        userIdString = sharePref.getshareprefdatastring(SharePref.USERID);
        firstNameString = sharePref.getshareprefdatastring(SharePref.USERFIRSTNAME);
        lastNameString = sharePref.getshareprefdatastring(SharePref.USERLASTNAME);
        emailString = sharePref.getshareprefdatastring(SharePref.USEREMAIL);
        phoneString = sharePref.getshareprefdatastring(SharePref.USERPHONE);
        ownerLandString = sharePref.getshareprefdatastring(SharePref.USERLANDPHONE);
        storeAddressString = sharePref.getshareprefdatastring(SharePref.USERADDRESS);
        storeNameString = sharePref.getshareprefdatastring(SharePref.STORENAME);
        storeLegalNameString = sharePref.getshareprefdatastring(SharePref.STORELEGALNAME);
        storeWebAddressString = sharePref.getshareprefdatastring(SharePref.WEBNAME);
        owmerTaxAmountString = sharePref.getshareprefdatastring(SharePref.TAXAMOUNT);

        callOptionString =  sharePref.getshareprefdatastring(SharePref.CALL_OPTION);

        firstNameText.setText(firstNameString);
        lastNameText.setText(lastNameString);
        emailText.setText(emailString);
        phoneText.setText(phoneString);
        ownerLandLineText.setText(ownerLandString);
        storeAddressText.setText(storeAddressString);
        storeNameText.setText(storeNameString);
        storeLegalNameText.setText(storeLegalNameString);
        storeWebAddressText.setText(storeWebAddressString);
        ownerTaxAmountText.setText(owmerTaxAmountString);

        Log.e("callOptionString",callOptionString);

        if(callOptionString.equals("userLandLine")){
            callOptionSpinner.setSelection(0);
        }else{
            callOptionSpinner.setSelection(1);
        }

        storeAddressText.setAdapter(new GooglePlacesAutocompleteAdapter(getActivity(), R.layout.address_list_item));
//
        storeAddressText.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                // Toast.makeText(getApplicationContext(), addressString, Toast.LENGTH_SHORT).show();

            }
        });
        update.setVisibility(View.GONE);

        progressSweetAlertDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.PROGRESS_TYPE);
        progressSweetAlertDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        progressSweetAlertDialog.setTitleText("Loading");
        progressSweetAlertDialog.setCancelable(false);
        editbtn.setOnClickListener(new View.OnClickListener() {
            @TargetApi(Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(View v) {
                // if (!editable) {
                enableAll();
                editable = true;
                cancelbtn.setVisibility(View.VISIBLE);
                update.setVisibility(View.VISIBLE);


                doneDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.NORMAL_TYPE);
                doneDialog.setTitleText("EDIT NOW");
                doneDialog.setContentText("You can now edit your information");
                doneDialog.show();
                doneDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {


                        doneDialog.dismiss();
                    }
                });

//                } else {
//                    disableAll();
//                    editable = false;
//                }
            }
        });

        cancelbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                cancelbtn.setVisibility(View.GONE);
                disableAll();
                editable = false;
                update.setVisibility(View.GONE);
            }
        });


        webAddressHyperLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String link ="http://www.mybodega.online/"+storeWebAddressString;

                Intent in=new Intent(Intent.ACTION_VIEW, Uri.parse(link));
                startActivity(in);
            }
        });

        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                firstNameString = firstNameText.getText().toString();
                lastNameString = lastNameText.getText().toString();
                emailString = emailText.getText().toString();
                phoneString = phoneText.getText().toString();
                ownerLandString = ownerLandLineText.getText().toString();
                storeAddressString = storeAddressText.getText().toString();
                storeNameString = storeNameText.getText().toString();
                storeLegalNameString = storeLegalNameText.getText().toString();
                owmerTaxAmountString = ownerTaxAmountText.getText().toString();

                passwordString1 = password1.getText().toString();
                passwordString2 = password2.getText().toString();

                callOptionString = callOptionSpinner.getSelectedItem().toString();

                if(callOptionString.equals("Land Phone")){

                    callOptionString ="userLandLine";

                }else {

                    callOptionString ="userContact";

                }

                cd = new ConnectionDetector(getActivity());

                isInternetOn = cd.isConnectingToInternet();

                if(isInternetOn) {
                    if(passwordString1.equals(passwordString2)) {

                        new AsyncTaskUpdate().execute();
                    }else {

                        doneDialog = new SweetAlertDialog(getActivity(),SweetAlertDialog.ERROR_TYPE);
                        doneDialog.setTitleText("Error");
                        doneDialog.setContentText("Both password doesn't match");
                        doneDialog.show();
                        doneDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                doneDialog.dismiss();
                            }
                        });
                    }


                }else{
                    Toast.makeText(getActivity(), "Check Your Internet Connection", Toast.LENGTH_SHORT).show();


                }
            }
        });

        return view;
    }
//
//    @Override
//    public void onActivityCreated(Bundle savedInstanceState)
//    {
//        super.onActivityCreated(savedInstanceState);
//
//        FontChangeCrawler fontChanger = new FontChangeCrawler(getActivity().getAssets(), "fonts/HelveticaNeueLTStd-LtCn.otf");
//        fontChanger.replaceFonts((ViewGroup) this.getView());
//    }
    class AsyncTaskUpdate extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {

            try {

                Log.e("asd", "asdasd");
                progressSweetAlertDialog.show();
                disableAll();
                update.setEnabled(false);
                //   Toast.makeText(getApplicationContext(), rateID, Toast.LENGTH_SHORT).show();
            } catch (IndexOutOfBoundsException e) {
                Toast.makeText(getActivity(), "no data", Toast.LENGTH_SHORT).show();
            }

        }

        @Override
        protected String doInBackground(String... params) {

            List<NameValuePair> pair = new ArrayList<NameValuePair>();

            pair.add(new BasicNameValuePair("userId", userIdString));
            pair.add(new BasicNameValuePair("userType", "1"));
            pair.add(new BasicNameValuePair("storeUpdateType","generalInfo"));
            pair.add(new BasicNameValuePair("userFirstName", firstNameString));
            pair.add(new BasicNameValuePair("userLastName", lastNameString));
            pair.add(new BasicNameValuePair("userEmail", emailString));
            pair.add(new BasicNameValuePair("userContact", phoneString));
            pair.add(new BasicNameValuePair("userLandLine", ownerLandString));
            pair.add(new BasicNameValuePair("userStoreName", storeNameString));
            pair.add(new BasicNameValuePair("userShopLocation",storeAddressString));
            pair.add(new BasicNameValuePair("userLegalName",storeLegalNameString ));
            pair.add(new BasicNameValuePair("userTaxAmount",owmerTaxAmountString ));
            pair.add(new BasicNameValuePair("userPassword",passwordString1 ));
            pair.add(new BasicNameValuePair("userActiveContact",callOptionString ));

            json = jsonParser.makeHttpRequest(Url.UPDATE, "POST", pair);

            Log.e("reg", pair + "");
            Log.e("reg", json + "");



            try {

                success = json.getInt("success");
                message = json.getString("message");

                Log.e("msh", message);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            progressSweetAlertDialog.dismiss();
            if (success == 1) {
                sharePref.setshareprefdatastring(SharePref.USERFIRSTNAME, firstNameString);
                sharePref.setshareprefdatastring(SharePref.USERLASTNAME, lastNameString);
                sharePref.setshareprefdatastring(SharePref.USERPHONE, phoneString);
                sharePref.setshareprefdatastring(SharePref.USERLANDPHONE, ownerLandString);
                sharePref.setshareprefdatastring(SharePref.USEREMAIL, emailString);
                sharePref.setshareprefdatastring(SharePref.USERADDRESS, storeAddressString);
                sharePref.setshareprefdatastring(SharePref.STORENAME, storeNameString);
                sharePref.setshareprefdatastring(SharePref.STORELEGALNAME, storeLegalNameString);
                sharePref.setshareprefdatastring(SharePref.TAXAMOUNT, owmerTaxAmountString);
                sharePref.setshareprefdatastring(SharePref.CALL_OPTION, callOptionString);

                Log.e("callOption",callOptionString);
                doneDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.SUCCESS_TYPE);
                doneDialog.setTitleText("Success");
                doneDialog.setContentText(message);
                doneDialog.show();
                doneDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {

                        setData();
                        doneDialog.dismiss();
                    }
                });



            } else{


                final SweetAlertDialog errorDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE);

                errorDialog.setContentText(message);
                errorDialog.show();
                errorDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        errorDialog.dismiss();
                    }
                });
            }

            progressSweetAlertDialog.dismiss();
            update.setEnabled(true);
        }


    }


    public void disableAll(){
        firstNameText.setEnabled(false);
        lastNameText.setEnabled(false);
        emailText.setEnabled(false);
        phoneText.setEnabled(false);
        ownerLandLineText.setEnabled(false);
        storeAddressText.setEnabled(false);
        storeNameText.setEnabled(false);
        storeLegalNameText.setEnabled(false);
        storeWebAddressText.setEnabled(false);
        ownerTaxAmountText.setEnabled(false);
        callOptionSpinner.setEnabled(false);
        cancelbtn.setVisibility(View.INVISIBLE);
        update.setVisibility(View.GONE);
        passwordLin1.setVisibility(View.GONE);

        passwordLin2.setVisibility(View.GONE);
    }

    public void enableAll(){

        firstNameText.setEnabled(true);
        lastNameText.setEnabled(true);
        emailText.setEnabled(true);
        phoneText.setEnabled(true);
        ownerLandLineText.setEnabled(true);
        storeAddressText.setEnabled(true);
        storeNameText.setEnabled(true);
        storeLegalNameText.setEnabled(true);
        storeWebAddressText.setEnabled(true);
        ownerTaxAmountText.setEnabled(true);
        passwordLin1.setVisibility(View.VISIBLE);
        callOptionSpinner.setEnabled(true);
        passwordLin2.setVisibility(View.VISIBLE);
    }


    public void setData(){

        userIdString = sharePref.getshareprefdatastring(SharePref.USERID);
        firstNameString = sharePref.getshareprefdatastring(SharePref.USERFIRSTNAME);
        lastNameString = sharePref.getshareprefdatastring(SharePref.USERLASTNAME);
        emailString = sharePref.getshareprefdatastring(SharePref.USEREMAIL);
        phoneString = sharePref.getshareprefdatastring(SharePref.USERPHONE);
        ownerLandString = sharePref.getshareprefdatastring(SharePref.USERLANDPHONE);
        storeAddressString = sharePref.getshareprefdatastring(SharePref.USERADDRESS);
        storeNameString = sharePref.getshareprefdatastring(SharePref.STORENAME);
        storeLegalNameString = sharePref.getshareprefdatastring(SharePref.STORELEGALNAME);
        storeWebAddressString = sharePref.getshareprefdatastring(SharePref.WEBNAME);
        owmerTaxAmountString = sharePref.getshareprefdatastring(SharePref.TAXAMOUNT);
        callOptionString= sharePref.getshareprefdatastring(SharePref.CALL_OPTION);

        Log.e("callOptionString",callOptionString);

        if(callOptionString.equals("userLandLine")){
            callOptionSpinner.setSelection(0);
        }else{
            callOptionSpinner.setSelection(1);
        }

        firstNameText.setText(firstNameString);
        lastNameText.setText(lastNameString);
        emailText.setText(emailString);
        phoneText.setText(phoneString);
        ownerLandLineText.setText(ownerLandString);
        storeAddressText.setText(storeAddressString);
        storeNameText.setText(storeNameString);
        storeLegalNameText.setText(storeLegalNameString);
        storeWebAddressText.setText(storeWebAddressString);
        ownerTaxAmountText.setText(owmerTaxAmountString);

    }




    public static ArrayList<String> autocomplete(String input) {
        ArrayList<String> resultList = null;

        HttpURLConnection conn = null;
        StringBuilder jsonResults = new StringBuilder();
        try {
            StringBuilder sb = new StringBuilder(PLACES_API_BASE + TYPE_AUTOCOMPLETE + OUT_JSON);
            sb.append("?key=" + API_KEY);
//            sb.append("&components=country:gr");
            //  sb.append("&components=country:gr");
            sb.append("&input=" + URLEncoder.encode(input, "utf8"));

            URL url = new URL(sb.toString());

            System.out.println("URL: "+url);
            conn = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(conn.getInputStream());

            // Load the results into a StringBuilder
            int read;
            char[] buff = new char[1024];
            while ((read = in.read(buff)) != -1) {
                jsonResults.append(buff, 0, read);
            }
        } catch (MalformedURLException e) {
            Log.e("address", "Error processing Places API URL", e);
            return resultList;
        } catch (IOException e) {
            Log.e("address", "Error connecting to Places API", e);
            return resultList;
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }

        try {

            // Create a JSON object hierarchy from the results
            JSONObject jsonObj = new JSONObject(jsonResults.toString());
            JSONArray predsJsonArray = jsonObj.getJSONArray("predictions");

            Log.e("josn",jsonObj+"");
            // Extract the Place descriptions from the results
            resultList = new ArrayList<String>(predsJsonArray.length());
            for (int i = 0; i < predsJsonArray.length(); i++) {
                System.out.println(predsJsonArray.getJSONObject(i).getString("description"));
                System.out.println("============================================================");
                resultList.add(predsJsonArray.getJSONObject(i).getString("description"));
            }
        } catch (JSONException e) {
            Log.e("address", "Cannot process JSON results", e);
        }

        return resultList;
    }

    class GooglePlacesAutocompleteAdapter extends ArrayAdapter<String> implements Filterable {
        private ArrayList<String> resultList;

        public GooglePlacesAutocompleteAdapter(Context context, int textViewResourceId) {
            super(context, textViewResourceId);
        }

        @Override
        public int getCount() {
            return resultList.size();
        }

        @Override
        public String getItem(int index) {
            return resultList.get(index);
        }

        @Override
        public Filter getFilter() {
            Filter filter = new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence constraint) {
                    FilterResults filterResults = new FilterResults();
                    if (constraint != null) {
                        // Retrieve the autocomplete results.
                        resultList = autocomplete(constraint.toString());

                        // Assign the data to the FilterResults
                        filterResults.values = resultList;
                        filterResults.count = resultList.size();
                    }
                    return filterResults;
                }

                @Override
                protected void publishResults(CharSequence constraint, FilterResults results) {
                    if (results != null && results.count > 0) {
                        notifyDataSetChanged();
                    } else {
                        notifyDataSetInvalidated();
                    }
                }
            };
            return filter;
        }
    }
}
