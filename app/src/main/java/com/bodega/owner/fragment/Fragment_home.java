package com.bodega.owner.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bodega.owner.R;
import com.bodega.owner.databasemanager.JSONParser;
import com.bodega.owner.databasemanager.Url;
import com.bodega.owner.utils.SharePref;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;


public class Fragment_home extends Fragment {

	Calendar myCalendar;

	TextView to, from, paid_t, sale_t, balance_t;

	private DatePicker datePicker;
	private Calendar calendar;
	private TextView dateView;
	private int year, month, day;
	ListView list;
	Button post;
	String startTime,endTime;
	String day1, month1;
	ArrayList<String> date, trasectionid, balance, payment, subtotal;
	Boolean isInternetPresent = false;
	String total_paid, total_sale, total_receieved;

	JSONObject json;

	Button inventory;
	FragmentManager mFragmentManager;

	// Connection detector class
	ConnectionDetector cd;

	JSONParser jsonParser;
	Date currenttime;

	String storeOwnernameString,storeAddressString,storeNameString,profilePicString;

	TextView storeName,ownerName,address,open, close, paymentEditText, credit, paypal, other, deliveryOption1, deliveryOption2;
	;
	de.hdodenhof.circleimageview.CircleImageView imageView;


	boolean d1,d2,d3,d4,d0;

	String d2value,d3value,d4value;

	SharePref sharePref;

	String userID;
	final private int REQUEST_CODE_ASK_PERMISSIONS = 123;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		// Get the view from fragmenttab1.xml
		View view = inflater.inflate(R.layout.fragment_home, container,
				false);

		// getActionBar().setDisplayHomeAsUpEnabled(true);
		// getActionBar().setHomeButtonEnabled(true);

		cd = new ConnectionDetector(getActivity());

		address = (TextView) view.findViewById(R.id.address);
		storeName  = (TextView) view.findViewById(R.id.storeName);

		ownerName  = (TextView) view.findViewById(R.id.ownerName);

		imageView = (CircleImageView) view.findViewById(R.id.image);

		open = (TextView) view.findViewById(R.id.open);

		paymentEditText = (TextView) view.findViewById(R.id.payment);


		deliveryOption1 = (TextView) view.findViewById(R.id.option1);
		deliveryOption2 = (TextView) view.findViewById(R.id.option2);


		isInternetPresent = cd.isConnectingToInternet();

		sharePref = new SharePref(getActivity());



		//	inventory = (Button) view.findViewById(R.id.invenroty);
		jsonParser = new JSONParser();

		// calendar = Calendar.getInstance();

		// ReportDrawerActivity report = new ReportDrawerActivity();

		// dateFormat = "MM";


//		inventory.setOnClickListener(new OnClickListener() {
//			@Override
//			public void onClick(View v) {
//
//
//				Fragment fragment = new Fragment_products();
//
//
//
//
//				FragmentTransaction ft = ((DrawerActivity)getActivity()).mFragmentManager.beginTransaction();
//
//
//
//				if (((DrawerActivity) getActivity()).fragmentStack.size() > 0) {
//					ft.hide(((DrawerActivity) getActivity()).fragmentStack.lastElement());
//				}
//				ft.add(R.id.frame_container, fragment);
//				((DrawerActivity) getActivity()).fragmentStack.push(fragment);
//				ft.commit();
//				// fragmentMana
//
//
//
//			}
//		});

		if(isInternetPresent){

			//new AsyncTaskPending().execute();
		}


		setStoreData();
		return view;
	}

//	@Override
//	public void onCreate(Bundle savedInstanceState) {
//
//		super.onCreate(savedInstanceState);
//
//		MarketService ms = new MarketService(getActivity());
//		ms.level(MarketService.MINOR).checkVersion();
//	}


	@Override
	public void onResume() {
		setStoreData();
		super.onResume();
	}

	public void setStoreData(){


		storeNameString = sharePref.getshareprefdatastring(SharePref.STORENAME);

		storeAddressString = sharePref.getshareprefdatastring(SharePref.SHOPSTREETNAME);

		storeOwnernameString= sharePref.getshareprefdatastring(SharePref.USERFIRSTNAME);

		storeOwnernameString= storeOwnernameString+" "+sharePref.getshareprefdatastring(SharePref.USERLASTNAME);

		d0 = sharePref.getshareprefdataBoolean(SharePref.D_OPTION0);
		d1 = sharePref.getshareprefdataBoolean(SharePref.D_OPTION1);
		d2 = sharePref.getshareprefdataBoolean(SharePref.D_OPTION2);
		d3 = sharePref.getshareprefdataBoolean(SharePref.D_OPTION3);
		d4 = sharePref.getshareprefdataBoolean(SharePref.D_OPTION4);

		d2value = sharePref.getshareprefdatastring(SharePref.D_OPTION2_VALUE);
		d3value = sharePref.getshareprefdatastring(SharePref.D_OPTION3_VALUE);
		d4value = sharePref.getshareprefdatastring(SharePref.D_OPTION4_VALUE);


		profilePicString= sharePref.getshareprefdatastring(SharePref.PROFILEPIC);

		userID = sharePref.getshareprefdatastring(SharePref.USERID);

		storeName.setText(storeNameString);

		address.setText(storeAddressString);

		ownerName.setText("Manager : " + storeOwnernameString);
		checkOpenOrClose();

		if (d1) {
			deliveryOption1.setText("Free Delivery");

			if(d4){
				deliveryOption2.setText("Rush Delivery : $" + d4value);
			}

		} else if(d2) {

			deliveryOption1.setText("Minimum Purchase : $"+d2value);

			if(d4){
				deliveryOption2.setText("Rush Delivery : $" + d4value);
			}

		}else if(d3) {

			deliveryOption1.setText("Delivery Fee : $"+d3value);

			if(d4){
				deliveryOption2.setText("Rush Delivery : $" + d4value);
			}

		}else if (d0) {
			deliveryOption1.setText("No Delivery");
			deliveryOption2.setText("");
		}





		paymentEditText.setText("Payment : Cash");

		//checkOpenOrClose();

		if (isInternetPresent) {

			setData();

		} else {
			Toast.makeText(getActivity(), "No internet connection available",
					Toast.LENGTH_LONG).show();
		}


		if(isInternetPresent){
			setUpMapIfNeeded();
		}

	}



//	private void insertDummyContactWrapper() {
//		int hasWriteContactsPermission = checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION);
//		if (hasWriteContactsPermission != PackageManager.PERMISSION_GRANTED) {
//			requestPermissions(new String[] {Manifest.permission.WRITE_CONTACTS},
//					REQUEST_CODE_ASK_PERMISSIONS);
//			return;
//		}
//		insertDummyContact();
//	}


	private void setUpMapIfNeeded()
	{
		try {

			String latitude = sharePref.getshareprefdatastring(SharePref.LATITUDE);
			String longitude = sharePref.getshareprefdatastring(SharePref.LONGITUDE);

			double lat, lon;

			lat = Double.parseDouble(latitude);
			lon = Double.parseDouble(longitude);

			mFragmentManager = getChildFragmentManager();

			FragmentTransaction ft = mFragmentManager.beginTransaction();

			Fragment fragment = new Fragment_map_short();

			Bundle args = new Bundle();
			args.putDouble("lat", lat);
			args.putDouble("lon", lon);

			fragment.setArguments(args);


			ft.add(R.id.map_content, fragment);

			ft.commit();
		}catch(Exception e){

		}

	}

	public class ConnectionDetector {

		private Context _context;

		public ConnectionDetector(Context context) {
			this._context = context;
		}

		public boolean isConnectingToInternet() {
			ConnectivityManager connectivity = (ConnectivityManager) _context
					.getSystemService(Context.CONNECTIVITY_SERVICE);
			if (connectivity != null) {
				NetworkInfo[] info = connectivity.getAllNetworkInfo();
				if (info != null)
					for (int i = 0; i < info.length; i++)
						if (info[i].getState() == NetworkInfo.State.CONNECTED) {
							return true;
						}

			}
			return false;
		}
	}


	public void setData(){


		DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
				.cacheInMemory(true)
				.cacheOnDisk(true)
				.build();
		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getActivity())
				.defaultDisplayImageOptions(defaultOptions)
				.build();
		ImageLoader.getInstance().init(config);


		ImageLoader.getInstance().displayImage(profilePicString, imageView, defaultOptions);




	}

	class AsyncTaskRunnerDetails extends AsyncTask<String, String, String> {
		ProgressDialog progressDialog = new ProgressDialog(getActivity());
		int success = 5;
		JSONParser jParser = new JSONParser();

		Boolean availableProduct = false;

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub

			{

				date.clear();

				trasectionid.clear();
				payment.clear();

				subtotal.clear();
				balance.clear();

				try {

					List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

					// nameValuePairs.add(new BasicNameValuePair("fromdate",
					// from
					// .getText().toString()));

					// nameValuePairs.add(new BasicNameValuePair("todate", to
					// .getText().toString()));

					JSONObject json = jParser
							.makeHttpRequest(
									"http://altertechit.com/stock_android/due_payment.php",
									"POST", nameValuePairs);

					success = json.getInt("success");

					if (success == 1) {

						JSONArray product = json.getJSONArray("report");

						for (int i = 0; i < product.length(); i++) {
							JSONObject item = product.getJSONObject(i);

							date.add(item.getString("due"));

							payment.add(item.getString("payment"));
							trasectionid.add(item.getString("transactionid"));
							subtotal.add(item.getString("subtotal"));
							balance.add(item.getString("balance"));
						}

						availableProduct = true;

					}

				} catch (Exception e) {

					Log.d("Problemasdddddddddddddd", e + "");
				}
				return null;
			}

		}

		@Override
		protected void onPostExecute(String string) {

			progressDialog.dismiss();
			if (availableProduct = true) {



				// /calculayion();
				// Toast.makeText(getApplicationContext(),
				// date.get(0),
				// Toast.LENGTH_LONG).show();

				// ArrayAdapter<String> adapter = new
				// ArrayAdapter<String>(SalesReport.this,
				// android.R.layout.simple_list_item_1, android.R.id.text1,
				// date);
			}

			else {
				Toast.makeText(getActivity(),
						"Something wrong....Try again later", Toast.LENGTH_LONG)
						.show();

			}

		}

		@Override
		protected void onPreExecute() {

			progressDialog.setMessage("Please wait. Loading..");
			progressDialog.setCancelable(false);
			progressDialog.show();
		}

	}

	public void checkOpenOrClose() {
		boolean value = false;


		String weekDay;
		SimpleDateFormat dayFormat = new SimpleDateFormat("EEEE", Locale.US);

		Calendar calendar = Calendar.getInstance();
		weekDay = dayFormat.format(calendar.getTime());

		SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm a");
		String time = timeFormat.format(Calendar.getInstance().getTime());

		try {
			currenttime = timeFormat.parse(time);
		} catch (ParseException e) {
			e.printStackTrace();
		}


		//	Log.e("time", weekDay + " " + time);


		String data = getDayData(weekDay);


		//	Log.e("timebodega", data);



		divideTime(data);


		//return value;
	}


	public String getDayData(String weekday) {

		String data="";
		switch (weekday) {
			case "Sunday":

				data =sharePref.getshareprefdatastring(SharePref.SUNDAY);

				break;
			case "Monday":
				data =sharePref.getshareprefdatastring(SharePref.MONDAY);

				break;
			case "Wednesday":
				data =sharePref.getshareprefdatastring(SharePref.WEDNESSDAY);
				break;
			case "Tuesday":
				data =sharePref.getshareprefdatastring(SharePref.TUESDAY);

				break;

			case "Thursday":
				data =sharePref.getshareprefdatastring(SharePref.THUSDAY);

				break;

			case "Friday":
				data =sharePref.getshareprefdatastring(SharePref.FRIDAY);

				break;

			case "Saturday":
				data =sharePref.getshareprefdatastring(SharePref.SATURDAY);

				break;
			default:
				break;
		}

		return data;
	}


	public void divideTime(String data){


		//Log.e("dataCheck",data);
		String[] shopTimes1 = data.split("\\ - ");
		String[] startTime1 = shopTimes1[0].split("\\|");
		String[] endTime1 = shopTimes1[1].split("\\|");

		//Log.e("S",startTime1[0].length()+"");

//		if(startTime1[0].length()==4){
//			startTime1[0]= "0"+startTime1[0];
//		}
//		if(endTime1[0].length()==4){
//			endTime1[0]= "0"+endTime1[0];
//		}

		startTime = startTime1[0].replace(".",":")+" "+startTime1[1];
		endTime = endTime1[0].replace(".",":")+" "+endTime1[1];


		SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm a");
		//	String time = timeFormat.format(Calendar.getInstance().getTime());

		Date date1 = null,date2 = null;

		try {
			date1 = timeFormat.parse(startTime);
			date2 = timeFormat.parse(endTime);

		} catch (ParseException e) {
			e.printStackTrace();
		}

		if(currenttime.after(date1) && currenttime.before(date2)){

			open.setText("Store Open");

		}else{
			open.setText("Store Close");
		}

	}

//	@Override
//	public void onActivityCreated(Bundle savedInstanceState)
//	{
//		super.onActivityCreated(savedInstanceState);
//
//		FontChangeCrawler fontChanger = new FontChangeCrawler(getActivity().getAssets(), "fonts/HelveticaNeueLTStd-LtCn.otf");
//		fontChanger.replaceFonts((ViewGroup) this.getView());
//	}

	class AsyncTaskPending extends AsyncTask<String, String, String> {


		@Override
		protected void onPostExecute(String result) {

		}

		@Override
		protected String doInBackground(String... params) {

			List<NameValuePair> pair = new ArrayList<NameValuePair>();




			pair.add(new BasicNameValuePair("userId", userID));


			json = jsonParser.makeHttpRequest(Url.PENDINGMESSAGE, "POST", pair);

			Log.e("reg", json + "");



			return null;
		}

		@Override
		protected void onPreExecute() {

			try {

				Log.e("apiCall", "done");

				//   Toast.makeText(getApplicationContext(), rateID, Toast.LENGTH_SHORT).show();
			} catch (IndexOutOfBoundsException e) {
				//  Toast.makeText(getApplicationContext(), "no data", Toast.LENGTH_SHORT).show();
			}

		}

	}
}