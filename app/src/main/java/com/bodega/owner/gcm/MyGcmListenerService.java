/**
 * Copyright 2015 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.bodega.owner.gcm;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.bodega.owner.R;
import com.bodega.owner.activity.SoccetIOChat;
import com.bodega.owner.databasemanager.MyDatabase;
import com.bodega.owner.utils.SharePref;
import com.google.android.gms.gcm.GcmListenerService;


public class MyGcmListenerService extends GcmListenerService {

    private static final String TAG = "MyGcmListenerService";

    SharePref sharePref;


    MyDatabase db;

    boolean logedIN;


    String userID;
    /**
     * Called when message is received.
     *
     * @param from SenderID of the sender.
     * @param data Data bundle containing message data as key/value pairs.
     *             For Set of keys use data.keySet().
     */
    // [START receive_message]

    @Override
    public void onMessageReceived(String from, Bundle data) {


        Log.e("data",data+"");
        String message = data.getString("message");

        String senderID = data.getString("sender_id");

        String senderName = data.getString("sender_name");

        String receiverID = data.getString("receiver_id");




        sharePref =new SharePref(getApplicationContext());

        logedIN = sharePref.getshareprefdataBoolean(SharePref.LOGEDIN);

        userID = sharePref.getshareprefdatastring(SharePref.USERID);

        Log.e(TAG, "From: " + senderID);


//        if (from.startsWith("/topics/")) {
//            // message received from some topic.
//        } else {
//            // normal downstream message.
//        }
        sendNotification(message, senderID, senderName);

//        db = new MyDatabase(getApplicationContext());
//
//
//       db.saveMessageIncoming(receiverID, senderID, senderName, message, "incoming", "0", smsDate,smsTime);

        if(logedIN && userID.equals(receiverID)) {

//            updateMyActivity(getApplicationContext());
            //sendNotification(message, senderID, senderName);
        }
        // [END_EXCLUDE]
    }
    // [END receive_message]

    /**
     * Create and show a simple notification containing the received GCM message.
     *
     * @param message GCM message received.
     */
    private void sendNotification(String message,String senderID,String senderName) {
        Intent intent = new Intent(this, SoccetIOChat.class);
        intent.putExtra("shopId",senderID);
        intent.putExtra("shopName",senderName);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.icon);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_gcm,BIND_IMPORTANT)


                .setContentTitle("New Message from "+ senderName)
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }

    static void updateMyActivity(Context context) {

        Intent intent = new Intent("unique_name");


        context.sendBroadcast(intent);
    }
}
