package com.bodega.owner.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bodega.owner.R;
import com.bodega.owner.databasemanager.JSONParser;
import com.bodega.owner.databasemanager.Url;
import com.bodega.owner.model.DeliveryOption;
import com.bodega.owner.model.JsonModelDelivery;
import com.bodega.owner.model.JsonModelHours;
import com.bodega.owner.model.JsonModelImages;
import com.bodega.owner.utils.ConnectionDetector;
import com.bodega.owner.utils.SharePref;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class RegistrationActivityFourth extends AppCompatActivity   {

    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;



    SharePref sharePref;

    ConnectionDetector cd;
    int success = 0;

    ImageButton nextPage,previousPage;

    String errorString ="";

    Url url;
    JSONObject json;

    boolean deliveryOption0,deliveryOption1,deliveryOption2,deliveryOption3,deliveryOption4;

    String deliveryOption2Value = "",deliveryOption3Value = "",deliveryOption4Value = "";

    ImageButton backBtn;

    boolean isInternetOn=false;

    ArrayList timeList,ampmlist,additaionalImagePath;

    ArrayList hours;

    ArrayList imageList;

    String personalImagePath,firstNameString,lastNameString;

    String commercialNameString,webNameString,phoneString,storeImageString;
    ArrayList <Integer> timeListInt;
    String legalNameString,legalCertificateImage;
    String userID,gcmCode,shopNameString,addressString,passwordString1,passwordString2,contactString,emailString,message;

    String taxfeeAmountString;
    LinearLayout lin;
    JSONParser jsonParser;

    TextView taxTitle,taxInfo,deliveryTitle,deliveryInfo;

    InputMethodManager imm;

    ArrayList <DeliveryOption> deliveryOptionArrayList;

    EditText dOptionValue2,dOptionValue3,dOptionValue4;

    CheckBox checkBox0,checkBox1,checkBox2,checkBox3,checkBox4;

    boolean isExtraAvailable = false;

  //  Typeface typeface;

    String storeLandLineString;
    String msg;


    SweetAlertDialog progressSweetAlertDialog, normalDialog,doneDialog;

    private static final String LOG_TAG = "Google Places Autocomplete";

    private static final String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place";

    private static final String TYPE_AUTOCOMPLETE = "/autocomplete";

    private static final String OUT_JSON = "/json";


    private static final String API_KEY =   "AIzaSyB_wVRhZ1qu9IVJO3JyS_fIPIfOkoLSjUA";


    EditText taxAmount;

    Button signUp;

    DeliveryOption deliveryOption;

    ArrayList additionalImagePathStringList;

    ImageButton secondNxtBtn;

    String callOption;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration_fourth);


        taxAmount = (EditText) findViewById(R.id.taxFeeAmount);



        hours = new ArrayList();
        timeList = new ArrayList();
        timeListInt = new ArrayList<>();

    //    typeface = Typeface.createFromAsset(getAssets(), "fonts/HelveticaNeueLTStd-LtCn.otf");
        signUp = (Button) findViewById(R.id.register);

        additaionalImagePath = new ArrayList();
        imageList = new ArrayList();

        deliveryOptionArrayList = new ArrayList<>();

        jsonParser = new JSONParser();
        url = new Url();
        sharePref = new SharePref(getApplicationContext());

        nextPage = (ImageButton) findViewById(R.id.btnNext);

        previousPage = (ImageButton) findViewById(R.id.backBtn);

        additionalImagePathStringList = new ArrayList();

        checkBox0 = (CheckBox) findViewById(R.id.deliveryOption0);

        checkBox1 = (CheckBox) findViewById(R.id.deliveryOption1);

        checkBox2 = (CheckBox) findViewById(R.id.deliveryOption2);

        checkBox3 = (CheckBox) findViewById(R.id.deliveryOption3);

        checkBox4 = (CheckBox) findViewById(R.id.deliveryOption4);

        dOptionValue2 = (EditText) findViewById(R.id.dOptionValue2);

        dOptionValue3 = (EditText) findViewById(R.id.dOptionValue3);
        dOptionValue4 = (EditText) findViewById(R.id.dOptionValue4);

        taxTitle= (TextView) findViewById(R.id.taxTitle);
        taxInfo= (TextView) findViewById(R.id.taxInfo);
        deliveryTitle= (TextView) findViewById(R.id.delFees);
        deliveryInfo= (TextView) findViewById(R.id.deliveryInfo);

        callOption = getIntent().getStringExtra("callOption");

        firstNameString = getIntent().getStringExtra("firstName");
        lastNameString = getIntent().getStringExtra("lastName");
        emailString = getIntent().getStringExtra("email");
        passwordString1 = getIntent().getStringExtra("password");
        personalImagePath = getIntent().getStringExtra("personalPicPath");



        storeLandLineString = getIntent().getStringExtra("landLine");
        commercialNameString = getIntent().getStringExtra("commercialName");
        addressString = getIntent().getStringExtra("address");
        phoneString= getIntent().getStringExtra("phone");
        webNameString = getIntent().getStringExtra("webName");
        storeImageString = getIntent().getStringExtra("storeImage");


        legalCertificateImage = getIntent().getStringExtra("legalImage");

        legalNameString = getIntent().getStringExtra("legalName");
        timeListInt = getIntent().getIntegerArrayListExtra("hoursInt");

        hours = getIntent().getIntegerArrayListExtra("hours");

        taxfeeAmountString = getIntent().getStringExtra("tax");


        deliveryOption2Value = getIntent().getStringExtra("deliveryOp2value");
        deliveryOption3Value = getIntent().getStringExtra("deliveryOp3value");
        deliveryOption4Value = getIntent().getStringExtra("deliveryOp4value");


        deliveryOption0 = getIntent().getBooleanExtra("deliveryOp0", false);
        deliveryOption1 = getIntent().getBooleanExtra("deliveryOp1", false);

        deliveryOption2 = getIntent().getBooleanExtra("deliveryOp2", false);

        deliveryOption3 = getIntent().getBooleanExtra("deliveryOp3", false);
        deliveryOption4 = getIntent().getBooleanExtra("deliveryOp4", false);

        ArrayList Adtnllist = new ArrayList();
        additaionalImagePath =  getIntent().getStringArrayListExtra("additionalPic");


        cd = new ConnectionDetector(getApplicationContext());

        isInternetOn = cd.isConnectingToInternet();

        try{
            additaionalImagePath.size();
            isExtraAvailable = true;
        }catch (Exception e){

            isExtraAvailable=false;

            additaionalImagePath = new ArrayList();
        }

        gcmCode = sharePref.getshareprefdatastring(SharePref.GCM_REGCODE);

        progressSweetAlertDialog = new SweetAlertDialog(RegistrationActivityFourth.this, SweetAlertDialog.PROGRESS_TYPE);
        progressSweetAlertDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        progressSweetAlertDialog.setTitleText("Loading");
        progressSweetAlertDialog.setCancelable(false);
//

      // Log.e("logi$$",additaionalImagePath.get(0).toString() +" - " +deliveryOption3Value +" - "+ deliveryOption4Value );
//        if(!Adtnllist.isEmpty()){
//            additaionalImagePath = Adtnllist;
//        }

        dOptionValue2.setText(deliveryOption2Value);

        dOptionValue3.setText(deliveryOption3Value);
        dOptionValue4.setText(deliveryOption4Value);

        checkBox0.setChecked(deliveryOption0);
        checkBox1.setChecked(deliveryOption1);
        checkBox2.setChecked(deliveryOption2);
        checkBox3.setChecked(deliveryOption3);
        checkBox4.setChecked(deliveryOption4);


//        dOptionValue2.setTypeface(typeface);
//
//        dOptionValue4.setTypeface(typeface);
//        dOptionValue3.setTypeface(typeface);
//        checkBox2.setTypeface(typeface);
//        checkBox3.setTypeface(typeface);
//        checkBox4.setTypeface(typeface);
//        checkBox1.setTypeface(typeface);
//        taxAmount.setTypeface(typeface);
//        taxTitle.setTypeface(typeface);
//        taxInfo.setTypeface(typeface);
//        deliveryInfo.setTypeface(typeface);
//        deliveryTitle.setTypeface(typeface);

        dOptionValue3.setText(deliveryOption3Value);
        dOptionValue4.setText(deliveryOption4Value);

        checkBox1.setChecked(deliveryOption1);
        checkBox2.setChecked(deliveryOption2);
        checkBox3.setChecked(deliveryOption3);
        checkBox4.setChecked(deliveryOption4);

        lin = (LinearLayout) findViewById(R.id.lin);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        imm = (InputMethodManager)  getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(lin.getWindowToken(), 0);

        if (checkBox3.isChecked()) {

            checkBox1.setChecked(false);


            checkBox1.setEnabled(false);
            checkBox2.setChecked(false);


            checkBox2.setEnabled(false);


        } else {

            checkBox2.setEnabled(true);

        }


        if (checkBox0.isChecked()) {

            checkBox1.setChecked(false);

            checkBox1.setEnabled(false);
            checkBox2.setChecked(false);

            checkBox2.setEnabled(false);
            checkBox3.setChecked(false);

            checkBox3.setEnabled(false);
            checkBox4.setChecked(false);

            checkBox4.setEnabled(false);

        } else {


            checkBox1.setEnabled(true);
             checkBox2.setEnabled(true);
            checkBox3.setEnabled(true);
             checkBox4.setEnabled(true);

        }

        if(checkBox1.isChecked()) {

            checkBox2.setChecked(false);
            checkBox3.setChecked(false);


            checkBox2.setEnabled(false);
            checkBox3.setEnabled(false);


        }else{
            checkBox2.setEnabled(true);
            checkBox3.setEnabled(true);
            checkBox4.setEnabled(true);
        }

        if (checkBox2.isChecked()) {

            checkBox1.setChecked(false);


            checkBox1.setEnabled(false);

            checkBox3.setChecked(false);


            checkBox3.setEnabled(false);

        }






//        checkBox1.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//
//                if(checkBox1.isChecked()) {
//
//                    checkBox1.setChecked(false);
//
//
//                }else{
//                    checkBox1.setChecked(true);
//                }
//
//            }
//        });


        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(nullCheck()) {
                    if(isInternetOn) {
                        new AsyncTaskRunnerDetails().execute();
                    }else{
                        Toast.makeText(getApplicationContext(), "Check Your Internet Connection", Toast.LENGTH_SHORT).show();

                    }
                }

            }
});


        checkBox0.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    checkBox1.setChecked(false);

                    checkBox1.setEnabled(false);
                    checkBox2.setChecked(false);

                    checkBox2.setEnabled(false);
                    checkBox3.setChecked(false);

                    checkBox3.setEnabled(false);
                    checkBox4.setChecked(false);

                    checkBox4.setEnabled(false);

                } else {

                    checkBox1.setEnabled(true);


                    checkBox2.setEnabled(true);


                    checkBox3.setEnabled(true);


                    checkBox4.setEnabled(true);

                }

            }
        });

        

        checkBox1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked) {

                    checkBox2.setChecked(false);
                    checkBox3.setChecked(false);


                    checkBox2.setEnabled(false);
                    checkBox3.setEnabled(false);


                }else{
                    checkBox2.setEnabled(true);
                    checkBox3.setEnabled(true);
                    checkBox4.setEnabled(true);
                }

            }
        });

        checkBox2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {

                    checkBox1.setChecked(false);


                    checkBox1.setEnabled(false);

                    checkBox3.setChecked(false);


                    checkBox3.setEnabled(false);


                } else {



                    checkBox1.setEnabled(true);
                    checkBox3.setEnabled(true);

                }

            }
        });


        checkBox3.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {



                    checkBox1.setChecked(false);


                    checkBox1.setEnabled(false);

                    checkBox2.setChecked(false);


                    checkBox2.setEnabled(false);

                } else {

                    checkBox2.setEnabled(true);
                    checkBox1.setEnabled(true);

                }

            }
        });


        previousPage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getApplicationContext(), RegistrationActivityThird.class);


                setDeliveryOption();



             //   Log.e("logi", deliveryOptionArrayList + "");



             //   Log.e("logi",deliveryOption2Value +"- " +deliveryOption3Value +"- "+ deliveryOption4Value );

                intent.putExtra("firstName", firstNameString);
                intent.putExtra("lastName", lastNameString);
                intent.putExtra("email",emailString);
                intent.putExtra("password",passwordString1);
                intent.putExtra("personalPicPath", personalImagePath);

                intent.putExtra("landLine", storeLandLineString);
                intent.putExtra("commercialName", commercialNameString);
                intent.putExtra("address", addressString);
                intent.putExtra("webName", webNameString);
                intent.putExtra("phone", phoneString);
                intent.putExtra("storeImage", storeImageString);

                intent.putStringArrayListExtra("additionalPic", additaionalImagePath);
                intent.putExtra("legalName", legalNameString);
                intent.putExtra("legalImage", legalCertificateImage);

                intent.putExtra("tax", taxfeeAmountString);
                intent.putExtra("callOption", callOption);



                intent.putStringArrayListExtra("hours", hours);
                intent.putIntegerArrayListExtra("hoursInt", timeListInt);


                intent.putStringArrayListExtra("additionalPic", additaionalImagePath);

                intent.putExtra("deliveryOp1", deliveryOption1);
                intent.putExtra("deliveryOp2", deliveryOption2);
                intent.putExtra("deliveryOp3",deliveryOption3);
                intent.putExtra("deliveryOp4",deliveryOption4);


                intent.putExtra("deliveryOp2value",deliveryOption2Value);
                intent.putExtra("deliveryOp3value",deliveryOption3Value);
                intent.putExtra("deliveryOp4value",deliveryOption4Value);


                startActivity(intent);

                finish();
            }
        });

    }





    @Override
    public void onBackPressed() {

        Intent intent = new Intent(getApplicationContext(), RegistrationActivityThird.class);


        setDeliveryOption();



        //   Log.e("logi", deliveryOptionArrayList + "");



        //   Log.e("logi",deliveryOption2Value +"- " +deliveryOption3Value +"- "+ deliveryOption4Value );

        intent.putExtra("firstName", firstNameString);
        intent.putExtra("lastName", lastNameString);
        intent.putExtra("email",emailString);
        intent.putExtra("password",passwordString1);
        intent.putExtra("personalPicPath", personalImagePath);

        intent.putExtra("landLine", storeLandLineString);
        intent.putExtra("commercialName", commercialNameString);
        intent.putExtra("address", addressString);
        intent.putExtra("webName", webNameString);
        intent.putExtra("phone", phoneString);
        intent.putExtra("storeImage", storeImageString);

        intent.putStringArrayListExtra("additionalPic", additaionalImagePath);
        intent.putExtra("legalName", legalNameString);
        intent.putExtra("legalImage", legalCertificateImage);

        intent.putExtra("tax", taxfeeAmountString);
        intent.putExtra("callOption", callOption);



        intent.putStringArrayListExtra("hours", hours);
        intent.putIntegerArrayListExtra("hoursInt", timeListInt);


        intent.putExtra("deliveryOp1", deliveryOption1);
        intent.putExtra("deliveryOp2", deliveryOption2);
        intent.putExtra("deliveryOp3",deliveryOption3);
        intent.putExtra("deliveryOp4",deliveryOption4);


        intent.putExtra("deliveryOp2value",deliveryOption2Value);
        intent.putExtra("deliveryOp3value",deliveryOption3Value);
        intent.putExtra("deliveryOp4value",deliveryOption4Value);


        startActivity(intent);

        finish();


        super.onBackPressed();
    }


    public boolean nullCheck() {
        boolean flag = false;


            if (checkBox0.isChecked() ||checkBox1.isChecked() || checkBox2.isChecked()|| checkBox3.isChecked()|| checkBox4.isChecked()) {


                return true;

            } else {
                msg = "Please select your delivery options!";
            }



        if(!flag) {
            new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                    .setTitleText("Oops...")
                    .setContentText(msg)
                    .show();
        }



        return flag;


    }

    public void setDeliveryOption(){

        taxfeeAmountString = taxAmount.getText().toString();
        deliveryOption0 =  checkBox0.isChecked();
        deliveryOption1 = checkBox1.isChecked();
        deliveryOption2 = checkBox2.isChecked();
        deliveryOption3 = checkBox3.isChecked();
        deliveryOption4 = checkBox4.isChecked();

        if(checkBox2.isChecked()){
            deliveryOption2Value = dOptionValue2.getText().toString().replace("$","");

        }else{
            deliveryOption2Value = "00";

        }
        if(checkBox3.isChecked()){
            deliveryOption3Value = dOptionValue3.getText().toString().replace("$", "");
        }else{
            deliveryOption3Value = "00";

        }
        if(checkBox4.isChecked()){
            deliveryOption4Value = dOptionValue4.getText().toString().replace("$","");
        }else{
            deliveryOption4Value = "00";

        }


        deliveryOption = new DeliveryOption("d0",deliveryOption0,"0");
        deliveryOptionArrayList.add(deliveryOption);

        deliveryOption = new DeliveryOption("d1",deliveryOption1,"0");
        deliveryOptionArrayList.add(deliveryOption);

        deliveryOption = new DeliveryOption("d2",deliveryOption2,deliveryOption2Value);
        deliveryOptionArrayList.add(deliveryOption);

        deliveryOption = new DeliveryOption("d3",deliveryOption3,deliveryOption3Value);
        deliveryOptionArrayList.add(deliveryOption);
        deliveryOption = new DeliveryOption("d4",deliveryOption4, deliveryOption4Value);
        deliveryOptionArrayList.add(deliveryOption);


    }

    public void SetImageForDb(){

        Uri uri = Uri.parse(personalImagePath);

        File persoNalPicFile,storePicFile,legalPicFile;

        try {
            Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);

            File file = new File("path");
            OutputStream os = new BufferedOutputStream(new FileOutputStream(file));
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, os);
            os.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
        persoNalPicFile = new File(personalImagePath);

        storePicFile = new File(storeImageString);

        legalPicFile = new File(legalNameString);






    }

    class AsyncTaskRegValid extends AsyncTask<String, String, String> {


        @Override
        protected void onPostExecute(String result) {
            dOptionValue2.setText(imageList.get(0).toString());
            //registerBtn.setEnabled(true);
        }

        @Override
        protected String doInBackground(String... params) {

            List<NameValuePair> pair = new ArrayList<NameValuePair>();


            setDeliveryOption();
            setImages();


            pair.add(new BasicNameValuePair("files", imageList.get(0).toString()));
            pair.add(new BasicNameValuePair("fileName", "asdasd"));

            Log.e("image", imageList.get(0).toString());
            json = jsonParser.makeHttpRequest(Url.SAMPLE, "POST", pair);

            Log.e("reg", jsonParser.makeHttpRequest(Url.SAMPLE, "POST", pair) + "");




            return null;
        }

        @Override
        protected void onPreExecute() {

            try {


               // registerBtn.setEnabled(false);
                //   Toast.makeText(getApplicationContext(), rateID, Toast.LENGTH_SHORT).show();
            } catch (IndexOutOfBoundsException e) {
                Toast.makeText(getApplicationContext(), "no data", Toast.LENGTH_SHORT).show();
            }

        }

    }

    class RetrieveFeedTask extends AsyncTask<Void, Void, String> {

        private Exception exception;

        protected void onPreExecute() {
//            progressBar.setVisibility(View.VISIBLE);
//            responseView.setText("");
        }

        protected String doInBackground(Void... urls) {
           // String email = emailText.getText().toString();
            // Do some validation here

            try {
                URL url = new URL(Url.SAMPLE + "sampleData1=" + "@dhodf" + "&sampleData2=" + "@1231123");
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                try {
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                    StringBuilder stringBuilder = new StringBuilder();
                    String line;
                    while ((line = bufferedReader.readLine()) != null) {
                        stringBuilder.append(line).append("\n");
                    }
                    bufferedReader.close();
                    return stringBuilder.toString();
                }
                finally{
                    urlConnection.disconnect();
                }
            }
            catch(Exception e) {
                Log.e("ERROR", e.getMessage(), e);
                return null;
            }
        }

        protected void onPostExecute(String response) {
            if(response == null) {
                response = "THERE WAS AN ERROR";
            }
        //    progressBar.setVisibility(View.GONE);
           // Log.i("INFO", response);
           // responseView.setText(response);
            // TODO: check this.exception
            // TODO: do something with the feed

            try {
                JSONObject object =new JSONObject(response);

               // Log.e("asd",object+"");



            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    class AsyncTaskRunnerDetails extends AsyncTask<String, String, String> {

        int success = 5, mailcheck = 5;

        HttpResponse response;
        JSONObject response1;
        String responseBody;
        Boolean availableProduct, availablemail;

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub
            {

                HttpClient httpClient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost(
                        Url.REGISTRATION);

                httpPost.setHeader("content-type", "application/json");
                httpPost.setHeader("Accept", "application/json");
                //  JSONObject jsonObject1 = new JSONObject();
                JSONObject jsonObject = new JSONObject();


                ArrayList<JsonModelDelivery> jsonDelivery = new ArrayList<>();

                ArrayList<JsonModelImages> jsonImagesList = new ArrayList<>();
                ArrayList<JsonModelHours> jsonHours = new ArrayList<>();

                setDeliveryOption();

                setImages();

                JSONArray jsonArrayImages = new JSONArray();

                if(additaionalImagePath.size()!=0) {
                for(int i=0;i<additionalImagePathStringList.size();i++){
                    JsonModelImages modelImages = new JsonModelImages();
                    modelImages.image = additionalImagePathStringList.get(i).toString();

                    jsonImagesList.add(modelImages);


                    //Log.e("su",json_submodel.PID[0]+"");

                }




                    for (int i = 0; i < additaionalImagePath.size(); i++) {
                        jsonArrayImages.put(jsonImagesList.get(i).getJSONObject());
                    }
                }

                try{





                    for(int i=0;i<5;i++){
                        JsonModelDelivery delivery = new JsonModelDelivery();
                        delivery.optionName = deliveryOptionArrayList.get(i).optionName;
                        delivery.value  = deliveryOptionArrayList.get(i).value+"";

                        delivery.amount  = deliveryOptionArrayList.get(i).amount;
                        jsonDelivery.add(delivery);


                        //Log.e("su",json_submodel.PID[0]+"");

                    }


                    JSONArray jsonArray = new JSONArray();
                    for (int i=0;i < jsonDelivery.size();i++){
                        jsonArray.put(jsonDelivery.get(i).getJSONObject());
                    }

                    for(int i=0;i<7;i++){
                        JsonModelHours jsonModelHours = new JsonModelHours();
                        jsonModelHours.time = hours.get(i).toString();

                        jsonHours.add(jsonModelHours);


                        //Log.e("su",json_submodel.PID[0]+"");

                    }




                    JSONArray jsonArrayHours = new JSONArray();
                    for (int i=0;i < 7;i++){
                        jsonArrayHours.put(jsonHours.get(i).getJSONObject());
                    }



                    JSONObject jsonObjectImages = new JSONObject();

                    jsonObjectImages.accumulate("personalImage",imageList.get(0).toString());

                    jsonObjectImages.accumulate("storeImage",imageList.get(1).toString());
                    jsonObjectImages.accumulate("legalImage",imageList.get(2).toString());
                    jsonObjectImages.accumulate("additionalImage",jsonArrayImages);





                 //   String productDetails =new Gson().toJson(json_model.Products);


                    jsonObject.accumulate("userFirstName", firstNameString);
                    jsonObject.accumulate("userLastName",lastNameString);
                    jsonObject.accumulate("userEmail", emailString);
                    jsonObject.accumulate("userPassword", passwordString1);
                    jsonObject.accumulate("userStoreName", commercialNameString);
                    jsonObject.accumulate("userShopLocation",addressString);
                    jsonObject.accumulate("userContact",phoneString);

                    jsonObject.accumulate("userHours",jsonArrayHours);
                    jsonObject.accumulate("userWebAddress",webNameString);
                    jsonObject.accumulate("userLegalName",legalNameString );

                    jsonObject.accumulate("userDeliveryFees",jsonArray);
                    jsonObject.accumulate("userType","1");
                    jsonObject.accumulate("userrlMethod","0" );
                    jsonObject.accumulate("userDeviceType","0");
                    jsonObject.accumulate("userDeviceId","");
                    jsonObject.accumulate("userGcmCode",gcmCode);
                    jsonObject.accumulate("userSocialId","");
                     jsonObject.accumulate("files",jsonObjectImages);

                    jsonObject.accumulate("userLandLine",storeLandLineString);

                    if(callOption.equals("Land Phone")){

                        callOption ="userLandLine";

                    }else {

                        callOption ="userContact";

                    }
                    jsonObject.accumulate("userActiveContact",callOption);



                    Log.e("res",jsonObject.toString()+"");

                }
                catch (Exception e) {
//
//                    Log.d("InputStream", e.getLocalizedMessage());

                }

                StringEntity entity = null;




                try {
                    entity = new StringEntity( jsonObject.toString());
                    entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                   // Log.d("ch",entity+"");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                httpPost.setEntity(entity);

                try {
                    //  response = httpClient.execute(httpPost);
//                    resC++;
//                    Log.e("Value of response count: ", resC + "");
                    ResponseHandler<String> responseHandler=new BasicResponseHandler();
                     responseBody =httpClient.execute(httpPost, responseHandler);
                     response1=new JSONObject(responseBody);
                   Log.e("res", responseBody + "");



                   // invoiceNumber= response1.getString("orderno");



              } catch (IOException e) {
                 e.printStackTrace();
           } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }


        @Override
        protected void onPostExecute(String string) {

            progressSweetAlertDialog.dismiss();

            int success;
            String msg = "";
            try {
               success =  response1.getInt("success");

                msg = response1.getString("message");

                if(success==1){

                    sharePref.setshareprefdatastring(SharePref.USERID,response1.getJSONObject("result").getString("userId"));

                    sharePref.setshareprefdatastring(SharePref.USEREMAIL,emailString);
                    sharePref.setshareprefdatastring(SharePref.USERPASSWWORD, passwordString1);


                           // new AsyncTaskSocketReg().execute();

                    doneDialog=  new SweetAlertDialog(RegistrationActivityFourth.this,SweetAlertDialog.SUCCESS_TYPE);
                    doneDialog.setTitleText("Success");
                    doneDialog.setContentText(msg);
                    doneDialog.show();
                    doneDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {

                            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);


                            startActivity(intent);
                            finish();


                        }
                    });



                }else if(success ==0){

                    getError(response1);



                    doneDialog=  new SweetAlertDialog(RegistrationActivityFourth.this,SweetAlertDialog.ERROR_TYPE);


                    doneDialog.setContentText(errorString);
                    doneDialog.show();
                    doneDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            sweetAlertDialog.dismiss();
                        }
                    });

                    }



            }catch (JSONException e) {
                e.printStackTrace();
            }


            signUp.setEnabled(true);



        }



        @Override
        protected void onPreExecute() {

            progressSweetAlertDialog.show();
            signUp.setEnabled(false);

        }

    }


    public  void setImages() {

        Log.e("asd", personalImagePath);


        Log.e("asd1", legalCertificateImage);


        Log.e("asd2", storeImageString);


        imageList.add(encodeImages(personalImagePath));
       imageList.add(encodeImages(storeImageString));
       imageList.add(encodeImages(legalCertificateImage));

          if(additaionalImagePath.size()!=0){

        for(int i=0;i<additaionalImagePath.size();i++){
            additionalImagePathStringList.add(encodeImages(additaionalImagePath.get(i).toString()));


                 }
        }

    }


    public void getError(JSONObject json){

        String newline = System.getProperty("line.separator");

        try {
            JSONObject result = json.getJSONObject("results");


            JSONArray errors= result.getJSONArray("errors");

            for (int i=0;i<errors.length();i++){

                JSONObject temp = errors.getJSONObject(i);

                errorString = errorString + "* " +  temp.getString("errorMessage") +newline;
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public String encodeImages(String filePath){

        String encodedString="";

        BitmapFactory.Options options = null;
        options = new BitmapFactory.Options();
        options.inSampleSize = 3;

        Bitmap bitmap;

        bitmap = BitmapFactory.decodeFile(filePath, options);
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        // Must compress the Image to reduce image size to make
        // upload easy
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        byte[] byte_arr = stream.toByteArray();
        // Encode Image to String
        encodedString = Base64.encodeToString(byte_arr,
                Base64.DEFAULT);


        return  encodedString;

    }



    class AsyncTaskSocketReg extends AsyncTask<String, String, String> {


        @Override
        protected void onPreExecute() {

            try {



                //   Toast.makeText(getActivity(), rateID, Toast.LENGTH_SHORT).show();
            } catch (IndexOutOfBoundsException e) {

            }

        }

        @Override
        protected String doInBackground(String... params) {

            Random rand = new Random();
            int value = rand.nextInt(500);
            List<NameValuePair> pair = new ArrayList<NameValuePair>();

            pair.add(new BasicNameValuePair("username", commercialNameString));
            pair.add(new BasicNameValuePair("email", emailString));
            pair.add(new BasicNameValuePair("password", "12345678"));
            pair.add(new BasicNameValuePair("appUserID", sharePref.getshareprefdatastring(SharePref.USERID)));
            pair.add(new BasicNameValuePair("userContact", phoneString));
            JSONObject json = jsonParser.makeHttpRequest(Url.SOCKET_REG, "POST", pair);
            Log.e("pair",pair+"");
            Log.e("json",json+"");
//
//            try {
//
//
//
//
//
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }

            return null;
        }

        @Override
        protected void onPostExecute(String result) {


            doneDialog=  new SweetAlertDialog(RegistrationActivityFourth.this,SweetAlertDialog.SUCCESS_TYPE);
            doneDialog.setTitleText("Success");
            doneDialog.setContentText(msg);
            doneDialog.show();
            doneDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sweetAlertDialog) {

                    Intent intent = new Intent(getApplicationContext(), LoginActivity.class);


                    startActivity(intent);
                    finish();


                }
            });



        }

    }

}

