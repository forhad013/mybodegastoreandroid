package com.bodega.owner.activity;

import android.app.Application;


import com.bodega.owner.databasemanager.Url;


import java.net.URISyntaxException;

import io.socket.client.IO;
import io.socket.client.Socket;


public class ChatApplication extends Application {

    private Socket mSocket;
    {
        try {
            mSocket = IO.socket(Url.SOCKETCHATIP);
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }



    public Socket getSocket() {
        return mSocket;
    }


}
