package com.bodega.owner.activity;

import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bodega.owner.R;
import com.bodega.owner.databasemanager.JSONParser;
import com.bodega.owner.databasemanager.Url;
import com.bodega.owner.utils.ConnectionDetector;
import com.bodega.owner.utils.SharePref;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class AddProduct extends AppCompatActivity {

    Button saveBtn,editBtn,deleteBtn,addNewBtn,cancle;

    LinearLayout editLin;

    ConnectionDetector cd;

    String token="";
    String  status;
    String msg;
    SweetAlertDialog progressSweetAlertDialog, normalDialog, doneDialog;
    boolean isInternetPresent;

    EditText productNameEdit,productPriceEdit,passwordtxt2,categoryDescriptionTxt;
    String categoryIDString,categoryNameString,userID,product_price,product_name,product_id;

    JSONParser jsonParser;
    JSONObject json;

    int error =1;
     SharePref sharePref;

    TextView catTitle,title;



    Spinner categorySpinner;

    CheckBox statusBox;





    ArrayList categoryList;
    String message,selectedID;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_add);


        sharePref = new SharePref(getApplicationContext());
        jsonParser = new JSONParser();
        cd = new ConnectionDetector(getApplicationContext());

        isInternetPresent = cd.isConnectingToInternet();

        productNameEdit = (EditText) findViewById(R.id.name);

        productPriceEdit = (EditText) findViewById(R.id.price);

        title = (TextView) findViewById(R.id.title);
        catTitle = (TextView) findViewById(R.id.categoryLabel);
        saveBtn = (Button) findViewById(R.id.done);

        deleteBtn = (Button) findViewById(R.id.delete);

        statusBox = (CheckBox) findViewById(R.id.status);

        token = getIntent().getStringExtra("token");

        categoryNameString = getIntent().getStringExtra("categoryTitle");
        categoryIDString = getIntent().getStringExtra("categoryID");

        userID = sharePref.getshareprefdatastring(SharePref.USERID);



        progressSweetAlertDialog = new SweetAlertDialog(AddProduct.this, SweetAlertDialog.PROGRESS_TYPE);
        progressSweetAlertDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        progressSweetAlertDialog.setTitleText("Loading");
        progressSweetAlertDialog.setCancelable(false);
        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                product_price = productPriceEdit.getText().toString();

                product_name =    productNameEdit.getText().toString();

               if(statusBox.isChecked()){
                   status = "1";
               }else{
                   status = "0";
               }


                if (!categoryNameString.equals("")) {
                    if (isInternetPresent) {

                       if (token.equals("edit")) {
                           new AsyncTaskCategoryEdit().execute();
                        } else if (token.equals("add")) {

                            new AsyncTaskNewCategory().execute();
                       }
                    }

                } else {
                    new SweetAlertDialog(AddProduct.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Oops...")
                            .setContentText("Write A Category Name")
                            .show();
                }

            }
        });

        catTitle.setText("Category Name > "+categoryNameString);

        ImageButton backBtn = (ImageButton) findViewById(R.id.backBtn);



        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });
        if(isInternetPresent){

        }


//        editBtn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                token = "edit";
//
//                editLin.setVisibility(View.VISIBLE);
//
//            }
//        });

        deleteBtn.setVisibility(View.INVISIBLE);
        if(token.equals("edit")){
            deleteBtn.setVisibility(View.VISIBLE);
            product_id = getIntent().getStringExtra("product_id");
            productNameEdit.setText(getIntent().getStringExtra("name"));
            productPriceEdit.setText(getIntent().getStringExtra("product_price"));
            title.setText("Edit Product");

            if(getIntent().getStringExtra("status").equals("0")){
                statusBox.setChecked(false);
            }else{
                statusBox.setChecked(true);
            }
        }

//        addNewBtn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                productName.setText("");
//                token = "add";
//                editLin.setVisibility(View.VISIBLE);
//            }
//        });

//        cancle.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//
//                editLin.setVisibility(View.INVISIBLE);
//            }
//        });


        deleteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                doneDialog = new SweetAlertDialog(AddProduct.this, SweetAlertDialog.WARNING_TYPE);
                doneDialog.setTitleText("DELETE?");
                doneDialog.setContentText("Are You Sure To Delete This Category");
                doneDialog.setConfirmText("DELETE");
                // doneDialog.setContentText("Login unsuccessful");
                doneDialog.show();
                doneDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {

                        doneDialog.dismiss();
                        new AsyncTaskCategoryDelete().execute();

                    }
                });
                doneDialog.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {

                        doneDialog.dismiss();
                    }
                });
                doneDialog.setCancelText("CANCLE");



            }
        });


    }

    class AsyncTaskNewCategory extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {

            try {

                Log.e("asd", "asdasd");
                progressSweetAlertDialog.show();
                saveBtn.setEnabled(false);
                //   Toast.makeText(getApplicationContext(), rateID, Toast.LENGTH_SHORT).show();
            } catch (IndexOutOfBoundsException e) {
                Toast.makeText(getApplicationContext(), "no data", Toast.LENGTH_SHORT).show();
            }

        }

        @Override
        protected String doInBackground(String... params) {

            List<NameValuePair> pair = new ArrayList<NameValuePair>();


            pair.add(new BasicNameValuePair("title", product_name));
            pair.add(new BasicNameValuePair("storeid", userID));
            pair.add(new BasicNameValuePair("unitprice", product_price));
            pair.add(new BasicNameValuePair("instore", status));
            pair.add(new BasicNameValuePair("categoryid", categoryIDString));

      //  title=anikcatprod2&storeid=4&instore=1&unitprice=1.2&categoryid=21

            json = jsonParser.makeHttpRequest(Url.PRODUCT_ADD, "POST", pair);

            Log.e("reg", json + "");


            try {



                error = json.getInt("error");
                message = json.getString("message");



            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            if (error == 0) {




                try {


                    productNameEdit.setText("");
                    progressSweetAlertDialog.dismiss();

                    doneDialog = new SweetAlertDialog(AddProduct.this, SweetAlertDialog.SUCCESS_TYPE);
                    doneDialog.setTitleText("New Category Added");
                    // doneDialog.setContentText("Login unsuccessful");
                    doneDialog.show();

                    editLin.setVisibility(View.INVISIBLE);


                    progressSweetAlertDialog.dismiss();



                } catch (Exception e) {

                }


            } else {

                doneDialog = new SweetAlertDialog(AddProduct.this, SweetAlertDialog.ERROR_TYPE);
                doneDialog.setTitleText(message);
                // doneDialog.setContentText("Login unsuccessful");
                doneDialog.show();
            }


            progressSweetAlertDialog.dismiss();
            saveBtn.setEnabled(true);
        }


    }




    class AsyncTaskCategoryDelete extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {

            try {

                progressSweetAlertDialog.show();
                saveBtn.setEnabled(false);
                //   Toast.makeText(getApplicationContext(), rateID, Toast.LENGTH_SHORT).show();
            } catch (IndexOutOfBoundsException e) {
                Toast.makeText(getApplicationContext(), "no data", Toast.LENGTH_SHORT).show();
            }

        }

        @Override
        protected String doInBackground(String... params) {

            List<NameValuePair> pair = new ArrayList<NameValuePair>();


            pair.add(new BasicNameValuePair("id", product_id));
            // pair.add(new BasicNameValuePair("categoryUserId", userID));




            json = jsonParser.makeHttpRequest(Url.PRODUCT_DELETE, "POST", pair);

            Log.e("reg", json + "");


            try {



                error = json.getInt("error");
                message = json.getString("message");



            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            if (error == 0) {


                try {
                    doneDialog = new SweetAlertDialog(AddProduct.this, SweetAlertDialog.SUCCESS_TYPE);
                    doneDialog.setTitleText("Successfully Deleted");
                    // doneDialog.setContentText("Login unsuccessful");
                    doneDialog.show();


                    progressSweetAlertDialog.dismiss();


                    finish();



                } catch (Exception e) {

                }


            } else {

                doneDialog = new SweetAlertDialog(AddProduct.this, SweetAlertDialog.ERROR_TYPE);
                doneDialog.setTitleText(message);
                // doneDialog.setContentText("Login unsuccessful");
                doneDialog.show();
            }


            progressSweetAlertDialog.dismiss();
            saveBtn.setEnabled(true);
        }


    }


    class AsyncTaskCategoryEdit extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {

            try {

                Log.e("asd", "asdasd");
                progressSweetAlertDialog.show();
                saveBtn.setEnabled(false);
                //   Toast.makeText(getApplicationContext(), rateID, Toast.LENGTH_SHORT).show();
            } catch (IndexOutOfBoundsException e) {
                Toast.makeText(getApplicationContext(), "no data", Toast.LENGTH_SHORT).show();
            }

        }

        @Override
        protected String doInBackground(String... params) {

            List<NameValuePair> pair = new ArrayList<NameValuePair>();


            pair.add(new BasicNameValuePair("title", categoryNameString));
             pair.add(new BasicNameValuePair("id", selectedID));
            pair.add(new BasicNameValuePair("status", status));
            pair.add(new BasicNameValuePair("storeid", userID));

            pair.add(new BasicNameValuePair("categoryid", categoryIDString));

            json = jsonParser.makeHttpRequest(Url.CATEGORY_EDIT, "POST", pair);

            Log.e("reg", json + "");


            try {



                error = json.getInt("error");
                message = json.getString("message");



            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            if (error == 0) {


                try {
                    doneDialog = new SweetAlertDialog(AddProduct.this, SweetAlertDialog.SUCCESS_TYPE);
                    doneDialog.setTitleText("Successfully EDITED");
                    // doneDialog.setContentText("Login unsuccessful");
                    doneDialog.show();

                    editLin.setVisibility(View.INVISIBLE);


                    progressSweetAlertDialog.dismiss();







                } catch (Exception e) {

                }


            } else {

                doneDialog = new SweetAlertDialog(AddProduct.this, SweetAlertDialog.ERROR_TYPE);
                doneDialog.setTitleText(message);
                // doneDialog.setContentText("Login unsuccessful");
                doneDialog.show();
            }


            progressSweetAlertDialog.dismiss();
            saveBtn.setEnabled(true);
        }


    }


}
