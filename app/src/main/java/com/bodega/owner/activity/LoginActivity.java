package com.bodega.owner.activity;

import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.bodega.owner.R;
import com.bodega.owner.databasemanager.JSONParser;
import com.bodega.owner.databasemanager.MyDatabase;
import com.bodega.owner.databasemanager.Url;
import com.bodega.owner.gcm.RegistrationIntentService;
import com.bodega.owner.utils.ConnectionDetector;
import com.bodega.owner.utils.SharePref;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class LoginActivity extends AppCompatActivity {

    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private static final String TAG = "MainActivity";

    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private ProgressBar mRegistrationProgressBar;
    private TextView mInformationTextView;

    String msg;

    Typeface myTypeface,custom ;

    Typeface typeface;

    SharePref sharePref;
    String gcmRegCode;

    EditText email,password;

    TextView login,signUp,forget;

    TextView born;

    ConnectionDetector cd;

    MyDatabase db;

    boolean isInternetOn= false;
    
    String message,emailString,passwordString;
    boolean loginStatus=false;

    SweetAlertDialog progressSweetAlertDialog, normalDialog,doneDialog;
    
    int success=0;

    JSONParser jsonParser;

    String userId;

    TextView contact, web;
    JSONObject json;
    ArrayList <Integer> timeListInt;
    ArrayList hours;
    

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);


        contact = (TextView)findViewById(R.id.contact);
        web = (TextView)findViewById(R.id.web);

        db = new MyDatabase(getApplicationContext());
        cd = new ConnectionDetector(getApplicationContext());

        isInternetOn = cd.isConnectingToInternet();

        jsonParser = new JSONParser();
        sharePref = new SharePref(getApplicationContext());

        gcmRegCode = sharePref.getshareprefdatastring(SharePref.GCM_REGCODE);



        myTypeface = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Regular.ttf");

        hours = new ArrayList();
        emailString= sharePref.getshareprefdatastring(SharePref.USEREMAIL);;

        loginStatus =  sharePref.getshareprefdataBoolean(SharePref.LOGEDIN);;


        userId = sharePref.getshareprefdatastring(SharePref.USERID);
        Log.e("log",loginStatus+"");

         if(loginStatus){
            Intent intent1 = new Intent(getApplicationContext(), DrawerActivity.class);


            startActivity(intent1);

            finish();
         }


        passwordString = sharePref.getshareprefdatastring(SharePref.USERPASSWWORD);


        email = (EditText) findViewById(R.id.oldPass);

        password = (EditText) findViewById(R.id.newPass);


        login = (TextView) findViewById(R.id.login);
        contact = (TextView) findViewById(R.id.contact);
        web = (TextView) findViewById(R.id.web);



        signUp = (TextView) findViewById(R.id.register);

        forget = (TextView) findViewById(R.id.forget);


        password.setTypeface(myTypeface);



        timeListInt = new ArrayList<>();

        contact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                {
               ;

                    Intent i = new Intent(Intent.ACTION_SEND);
                    i.setType("message/rfc822");
                    i.putExtra(Intent.EXTRA_EMAIL  , new String[]{"owner@mybodega.online"});

                    try {
                        startActivity(Intent.createChooser(i, "Send mail..."));
                    } catch (android.content.ActivityNotFoundException ex) {
                        Toast.makeText(LoginActivity.this, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        web.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                {
                    Intent in=new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.mybodega.online/"));
                    startActivity(in);
                }
            }
        });





        progressSweetAlertDialog = new SweetAlertDialog(LoginActivity.this, SweetAlertDialog.PROGRESS_TYPE);
        progressSweetAlertDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        progressSweetAlertDialog.setTitleText("Loading");
        progressSweetAlertDialog.setCancelable(false);



        password.setText(sharePref.getshareprefdatastring(SharePref.USERPASSWWORD));

        email.setText(sharePref.getshareprefdatastring(SharePref.USEREMAIL));


        forget.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(),
                        ForgetPassword.class);

                startActivity(intent);


            }
        });




        normalDialog =   new SweetAlertDialog(LoginActivity.this, SweetAlertDialog.ERROR_TYPE);


        gcmRegCode = sharePref.getshareprefdatastring(SharePref.GCM_REGCODE);

        if(gcmRegCode.isEmpty()){

            if (checkPlayServices()) {
                Intent intent = new Intent(this, RegistrationIntentService.class);
                startService(intent);

            }

        }



        gcmRegCode = sharePref.getshareprefdatastring(SharePref.GCM_REGCODE);



        signUp.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getApplicationContext(),
                        RegistrationActivityFirst.class);

                intent.putExtra("firstName", "");
                intent.putExtra("lastName", "");
                intent.putExtra("email","");
                intent.putExtra("password","");
                intent.putExtra("personalPicPath", "");

                intent.putExtra("commercialName", "");
                intent.putExtra("address", "");
                intent.putExtra("webName", "");
                intent.putExtra("phone", "");
                intent.putExtra("storeImage", "");
                intent.putStringArrayListExtra("hours", hours);

                intent.putIntegerArrayListExtra("hoursInt", timeListInt);

                startActivity(intent);
                finish();

            }
        });
        
        
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                
                 emailString = email.getText().toString().trim();

                passwordString = password.getText().toString().trim();

                gcmRegCode = sharePref.getshareprefdatastring(SharePref.GCM_REGCODE);




                if(nullCheck()){

                    isInternetOn = cd.isConnectingToInternet();

                    if(isInternetOn) {
                        if(!gcmRegCode.isEmpty()) {
                            new AsyncTaskRegValid().execute();
                        }else{
                            doneDialog = new SweetAlertDialog(LoginActivity.this, SweetAlertDialog.NORMAL_TYPE);
                            doneDialog.setTitleText("Wait for few second");
                            // doneDialog.setContentText("Login unsuccessful");
                            doneDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {

                                    doneDialog.dismiss();
                                }
                            });
                            doneDialog.show();

                            if (checkPlayServices()) {
                                Intent intent = new Intent(getApplicationContext(), RegistrationIntentService.class);
                                startService(intent);

                            }
                        }
                    }
                    else{
                        Toast.makeText(getApplicationContext(), "Check Your Internet Connection", Toast.LENGTH_SHORT).show();

                    }
                }


//                Intent intent = new Intent(LoginActivity.this, DrawerActivity.class);
//
//
//                startActivity(intent);
//
//                finish();
                
            }
        });




    }

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(SharePref.GCM_REGCODE));
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }

    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */
    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                Log.i("TAG", "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }





    class AsyncTaskRegValid extends AsyncTask<String, String, String> {


        @Override
        protected void onPostExecute(String result) {

            progressSweetAlertDialog.dismiss();
            login.setEnabled(true);
            if (success == 1) {


                sharePref.setshareprefdatastring(SharePref.USEREMAIL, email.getText().toString());


                try {


                   setLoginData();


                            Intent intent = new Intent(LoginActivity.this, DrawerActivity.class);
                            startActivity(intent);
                            finish();




                }catch (Exception e){

                }

            } else
            {
                doneDialog = new SweetAlertDialog(LoginActivity.this,SweetAlertDialog.ERROR_TYPE);

                doneDialog.setTitleText("ERROR");
                doneDialog.setContentText("The email address or password you entered is incorrect");

                doneDialog.show();
            }



        }

        @Override
        protected String doInBackground(String... params) {

            List<NameValuePair> pair = new ArrayList<NameValuePair>();




            pair.add(new BasicNameValuePair("userEmail", emailString));

            pair.add(new BasicNameValuePair("userPassword", passwordString));
            pair.add(new BasicNameValuePair("newGcmCode", gcmRegCode));

            pair.add(new BasicNameValuePair("userType", "1"));
            pair.add(new BasicNameValuePair("userrlMethod", "0"));
            pair.add(new BasicNameValuePair("userDeviceType", "0"));

             json = jsonParser.makeHttpRequest(Url.LOGIN, "POST", pair);


            try {

                success = json.getInt("success");
                message = json.getString("message");

                Log.e("msh", json+"");





            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPreExecute() {

            try {

                Log.e("asd","asdasd");
                progressSweetAlertDialog.show();
                login.setEnabled(false);
                //   Toast.makeText(getApplicationContext(), rateID, Toast.LENGTH_SHORT).show();
            } catch (IndexOutOfBoundsException e) {
                Toast.makeText(getApplicationContext(), "no data", Toast.LENGTH_SHORT).show();
            }

        }

    }

    public boolean nullCheck() {
        boolean flag = false;

        if (!email.getText().toString().trim().equalsIgnoreCase("")) {
            if (!password.getText().toString().trim().equalsIgnoreCase("")) {

                return true;

            } else {
                msg = "Please Enter Password!";
            }

        } else {
            msg = "Please Enter Shop name!";
        }


        if(!flag) {
            new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                    .setTitleText("Oops...")
                    .setContentText(msg)
                    .show();
        }



        return flag;


    }

    public void setLoginData(){


        try {
            JSONObject results = json.getJSONObject("results");

            JSONObject userInfo = results.getJSONObject("userInformation");

            sharePref.setshareprefdataBoolean(SharePref.LOGEDIN, true);

            sharePref.setshareprefdatastring(SharePref.USERID, userInfo.getString("userId"));

            userId = userInfo.getString("userId");

            sharePref.setshareprefdatastring(SharePref.USERFIRSTNAME, userInfo.getString("userFirstName"));
            sharePref.setshareprefdatastring(SharePref.USERLASTNAME, userInfo.getString("userLastName"));

            sharePref.setshareprefdatastring(SharePref.USERPHONE, userInfo.getString("userContact"));
            sharePref.setshareprefdatastring(SharePref.USERLANDPHONE, userInfo.getString("userLandLine"));

            sharePref.setshareprefdatastring(SharePref.USERADDRESS, userInfo.getString("userShopLocation"));

            sharePref.setshareprefdatastring(SharePref.LATITUDE, userInfo.getString("userShopLatitude"));

            sharePref.setshareprefdatastring(SharePref.LONGITUDE, userInfo.getString("userShopLongitude"));

            sharePref.setshareprefdatastring(SharePref.STORELEGALNAME, userInfo.getString("userLegalName"));
            sharePref.setshareprefdatastring(SharePref.WEBNAME, userInfo.getString("userWebAddress"));
            sharePref.setshareprefdatastring(SharePref.STORENAME, userInfo.getString("userStoreName"));
            sharePref.setshareprefdatastring(SharePref.TAXAMOUNT, userInfo.getString("userTaxAmount"));
            sharePref.setshareprefdatastring(SharePref.USERPASSWWORD, password.getText().toString());

            sharePref.setshareprefdatastring(SharePref.SHOPSTREETNAME, userInfo.getString("userStoreStreet"));
            sharePref.setshareprefdatastring(SharePref.CALL_OPTION, userInfo.getString("userActiveContact"));

            JSONObject images = userInfo.getJSONObject("userImages");


            try{
                sharePref.setshareprefdatastring(SharePref.PROFILEPIC, images.getString("profilePic"));
            }catch (Exception e){

            }

            try{
                sharePref.setshareprefdatastring(SharePref.OFFICEPIC, images.getString("officePic"));
            }catch (Exception e){

            }
            try{
                sharePref.setshareprefdatastring(SharePref.LEGALPIC, images.getString("legalPic"));
            }catch (Exception e){

            }

            sharePref.setshareprefdatastring(SharePref.LEGALPIC, images.getString("legalPic"));


            try{
                JSONArray additionalImages = images.getJSONArray("additionalPic");


                for(int i=0;i<additionalImages.length();i++){
                    db.clearImages();

                    db.saveImages(userId,"additional",additionalImages.get(i).toString());

                }


            }catch (Exception e){

            }






            JSONObject deliveryOptions = userInfo.getJSONObject("userDeliveryFees");

            JSONObject deliveryOption4 = deliveryOptions.getJSONObject("dFour");

            String d4value = deliveryOption4.getString("value");

            sharePref.setshareprefdatastring(SharePref.D_OPTION4_VALUE, d4value);

            String trueOrfalse4 = deliveryOption4.getString("deliveryOption");

            if(trueOrfalse4.equals("0")){

                sharePref.setshareprefdataBoolean(SharePref.D_OPTION4, false);

            }else{
                sharePref.setshareprefdataBoolean(SharePref.D_OPTION4, true);
            }


            JSONObject deliveryOption0 = deliveryOptions.getJSONObject("dZero");


            String trueOrfalse0= deliveryOption0.getString("deliveryOption");

            if(trueOrfalse0.equals("0")){

                sharePref.setshareprefdataBoolean(SharePref.D_OPTION0, false);

            }else{
                sharePref.setshareprefdataBoolean(SharePref.D_OPTION0, true);
            }


            JSONObject deliveryOption1 = deliveryOptions.getJSONObject("dOne");


            String trueOrfalse1= deliveryOption1.getString("deliveryOption");

            if(trueOrfalse1.equals("0")){

                sharePref.setshareprefdataBoolean(SharePref.D_OPTION1, false);

            }else{
                sharePref.setshareprefdataBoolean(SharePref.D_OPTION1, true);
            }



            JSONObject deliveryOption2 = deliveryOptions.getJSONObject("dTwo");

            String d2value = deliveryOption2.getString("value");

            sharePref.setshareprefdatastring(SharePref.D_OPTION2_VALUE, d2value);

            String trueOrfalse2 = deliveryOption2.getString("deliveryOption");

            if(trueOrfalse2.equals("0")){

                sharePref.setshareprefdataBoolean(SharePref.D_OPTION2, false);

            }else{
                sharePref.setshareprefdataBoolean(SharePref.D_OPTION2, true);
            }

            JSONObject deliveryOption3 = deliveryOptions.getJSONObject("dThree");

            String d3value = deliveryOption4.getString("value");

            sharePref.setshareprefdatastring(SharePref.D_OPTION3_VALUE, d3value);

            String trueOrfalse3 = deliveryOption3.getString("deliveryOption");

            if(trueOrfalse3.equals("0")){

                sharePref.setshareprefdataBoolean(SharePref.D_OPTION3, false);

            }else{
                sharePref.setshareprefdataBoolean(SharePref.D_OPTION3, true);
            }


            JSONObject hours = userInfo.getJSONObject("userHours");



            String sunday = getHours(hours.getJSONObject("SunDay"));

            sharePref.setshareprefdatastring(SharePref.SUNDAY, sunday);

            String monday = getHours(hours.getJSONObject("MonDay"));

            sharePref.setshareprefdatastring(SharePref.MONDAY, monday);

            String tuesDay = getHours(hours.getJSONObject("TuesDay"));

            sharePref.setshareprefdatastring(SharePref.TUESDAY, tuesDay);

            String wednesDay = getHours(hours.getJSONObject("WednesDay"));

            sharePref.setshareprefdatastring(SharePref.WEDNESSDAY, wednesDay);

            String thursday = getHours(hours.getJSONObject("ThursDay"));

            sharePref.setshareprefdatastring(SharePref.THUSDAY, thursday);

            String friday = getHours(hours.getJSONObject("FriDay"));

            sharePref.setshareprefdatastring(SharePref.FRIDAY, friday);

            String saturDay = getHours(hours.getJSONObject("SaturDay"));

            sharePref.setshareprefdatastring(SharePref.SATURDAY, saturDay);

         //   new AsyncTaskSocketLogin().execute();

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String getHours(JSONObject days){
        String endTimeString="",startTimeString="";
        String hours="";

        try {
            JSONObject endTime = days.getJSONObject("endTime");

            endTimeString = endTime.get("time").toString().replace(" ","")+ "|" +endTime.get("ind").toString().replace(" ", "");

            JSONObject startTime = days.getJSONObject("startTime");

            startTimeString = startTime.get("time").toString().replace(" ","")+ "|" +startTime.get("ind").toString().replace(" ","");



        } catch (JSONException e) {
            e.printStackTrace();
        }

        hours = startTimeString + " - "+endTimeString;

        return hours;
    }


    class AsyncTaskSocketLogin extends AsyncTask<String, String, String> {


        @Override
        protected void onPreExecute() {

            try {



                //   Toast.makeText(getActivity(), rateID, Toast.LENGTH_SHORT).show();
            } catch (IndexOutOfBoundsException e) {

            }

        }

        @Override
        protected String doInBackground(String... params) {


            List<NameValuePair> pair = new ArrayList<NameValuePair>();


            pair.add(new BasicNameValuePair("email", emailString));
            pair.add(new BasicNameValuePair("password", "12345678"));


            JSONObject json = jsonParser.makeHttpRequest(Url.SOCKET_LOGIN, "POST", pair);

            Log.e("json",json+"");


            return null;
        }

        @Override
        protected void onPostExecute(String result) {

            progressSweetAlertDialog.dismiss();

            login.setEnabled(true);

                    finish();



        }

    }


}
