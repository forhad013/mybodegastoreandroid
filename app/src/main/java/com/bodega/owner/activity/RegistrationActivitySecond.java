package com.bodega.owner.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bodega.owner.R;
import com.bodega.owner.databasemanager.JSONParser;
import com.bodega.owner.databasemanager.Url;
import com.bodega.owner.utils.ConnectionDetector;
import com.bodega.owner.utils.SharePref;
import com.luminous.pick.Action;
import com.sw926.imagefileselector.ImageCropper;
import com.sw926.imagefileselector.ImageFileSelector;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class RegistrationActivitySecond extends AppCompatActivity {

    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;



    String [] addressContent;
    SharePref sharePref;

    ConnectionDetector cd;
    String msg;
    int success = 0;
    LinearLayout lin;

    AutoCompleteTextView shopAddress;


    ImageFileSelector mImageFileSelector;
    ImageCropper mImageCropper;
    Url url;
    JSONObject json;

    ImageButton backBtn;

    boolean isInternetOn=false;
    String single_path;

    ArrayList additaionalImagePath;
    ImageButton secondNxtBtn;

    ImageButton nextPage,previousPage;

    ImageButton storePicBtn,aditionalStorePicBtn;
    String userID,gcmCode,shopNameString,addressString,passwordString1,passwordString2,contactString,emailString,message,personalImagePath,firstNameString,lastNameString;

    String commercialNameString,webNameString,phoneString,storeImageString="";

    String legalNameString,legalCertificateImage;

    JSONParser jsonParser;

    String storeLandLineString;

    EditText storelandLine;

    String callOption;
    Spinner calloptionSpinner;



    InputMethodManager imm;
    SweetAlertDialog progressSweetAlertDialog, normalDialog,doneDialog;

    private static final String LOG_TAG = "Google Places Autocomplete";

    private static final String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place";

    private static final String TYPE_AUTOCOMPLETE = "/autocomplete";

    private static final String OUT_JSON = "/json";


    private static final String API_KEY =   "AIzaSyB_wVRhZ1qu9IVJO3JyS_fIPIfOkoLSjUA";

    boolean deliveryOption0,deliveryOption1,deliveryOption2,deliveryOption3,deliveryOption4;

    String deliveryOption2Value,deliveryOption3Value,deliveryOption4Value;
    ArrayList hours;

    ArrayList <Integer> timeListInt;

    EditText commercialName,phone,webName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration_second);

        timeListInt = new ArrayList<>();

cd = new ConnectionDetector(getApplicationContext());
        secondNxtBtn = (ImageButton) findViewById(R.id.btnNext2);
        commercialName = (EditText) findViewById(R.id.comercialName);

        shopAddress = (AutoCompleteTextView) findViewById(R.id.address);

        phone = (EditText) findViewById(R.id.phoneCell);

        webName = (EditText) findViewById(R.id.webName);

        additaionalImagePath = new ArrayList();

        storePicBtn = (ImageButton) findViewById(R.id.storePic);

        storelandLine = (EditText) findViewById(R.id.phoneLand);

        calloptionSpinner= (Spinner) findViewById(R.id.callOption);
        callOption = getIntent().getStringExtra("callOption");

        calloptionSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                callOption = calloptionSpinner.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

       try {
           if (callOption.equals("Land Phone")) {
               calloptionSpinner.setSelection(0);
           } else if (callOption.equals("Cell Phone")) {
               calloptionSpinner.setSelection(1);
           }
       }catch (Exception e){
           e.printStackTrace();
       }



        aditionalStorePicBtn = (ImageButton) findViewById(R.id.storePicOptional);

        isInternetOn = cd.isConnectingToInternet();
      //  getData();

        hours =  new ArrayList();
        firstNameString = getIntent().getStringExtra("firstName");
        lastNameString = getIntent().getStringExtra("lastName");
        emailString = getIntent().getStringExtra("email");
        passwordString1 = getIntent().getStringExtra("password");
        personalImagePath = getIntent().getStringExtra("personalPicPath");


        storeLandLineString = getIntent().getStringExtra("landLine");
        commercialNameString = getIntent().getStringExtra("commercialName");
        addressString = getIntent().getStringExtra("address");
        phoneString= getIntent().getStringExtra("phone");
        webNameString = getIntent().getStringExtra("webName");
        storeImageString = getIntent().getStringExtra("storeImage");

        legalCertificateImage = getIntent().getStringExtra("legalImage");

        legalNameString = getIntent().getStringExtra("legalName");

        hours = new ArrayList();

        hours = getIntent().getStringArrayListExtra("hours");

        timeListInt = getIntent().getIntegerArrayListExtra("hoursInt");


        deliveryOption2Value = getIntent().getStringExtra("deliveryOp2value");
        deliveryOption3Value = getIntent().getStringExtra("deliveryOp3value");
        deliveryOption4Value = getIntent().getStringExtra("deliveryOp4value");


        mImageFileSelector.setDebug(true);
        deliveryOption1 = getIntent().getBooleanExtra("deliveryOp1", false);
        deliveryOption0 = getIntent().getBooleanExtra("deliveryOp0", false);
        deliveryOption2 = getIntent().getBooleanExtra("deliveryOp2", false);

        deliveryOption3 = getIntent().getBooleanExtra("deliveryOp3", false);
        deliveryOption4 = getIntent().getBooleanExtra("deliveryOp4", false);

        additaionalImagePath = getIntent().getStringArrayListExtra("additionalPic");

        storeImageString=storeImageString+"";

        lin = (LinearLayout) findViewById(R.id.lin);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        imm = (InputMethodManager)  getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(lin.getWindowToken(), 0);
      //  Log.e("pic", addressString);

        aditionalStorePicBtn = (ImageButton) findViewById(R.id.storePicOptional);


        jsonParser = new JSONParser();
        url = new Url();
        sharePref = new SharePref(getApplicationContext());

        nextPage = (ImageButton) findViewById(R.id.btnNext);

        previousPage = (ImageButton) findViewById(R.id.btnPrev);




        Log.e("hours",hours+"");

        commercialName.setText(commercialNameString);

        progressSweetAlertDialog = new SweetAlertDialog(RegistrationActivitySecond.this, SweetAlertDialog.PROGRESS_TYPE);
        progressSweetAlertDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        progressSweetAlertDialog.setTitleText("Loading");
        progressSweetAlertDialog.setCancelable(false);

        shopAddress.setText(addressString);
        phone.setText(phoneString);
        webName.setText(webNameString);
        storelandLine.setText(storeLandLineString);


        shopAddress.setAdapter(new GooglePlacesAutocompleteAdapter(this, R.layout.address_list_item));
//
        shopAddress.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                  addressString = (String) parent.getItemAtPosition(position);

              //  checkLocation();



                if(!checkLocation()){
                    addressString= "";

                 //   Toast.makeText(getApplicationContext(), "MyBodega is not open for service outside of  New Jersey and New York. Come back later as we expand to new areas.", Toast.LENGTH_SHORT).show();

                    String msg ="MyBodega is not open for service outside of  New Jersey and New York. Come back later as we expand to new areas.";
                    new SweetAlertDialog(RegistrationActivitySecond.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Oops...")
                            .setContentText(msg)
                            .show();
                }

              // Toast.makeText(getApplicationContext(), addressString, Toast.LENGTH_SHORT).show();

            }
        });

        secondNxtBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                commercialNameString = commercialName.getText().toString().trim();
                // addressString = shopAddress.getText().toString().trim();
                phoneString= phone.getText().toString().trim();
                webNameString = webName.getText().toString().trim();

                storeLandLineString = storelandLine.getText().toString();



                if(nullCheck()) {

                    new AsyncTaskWeNameValid().execute();

                }
            }
        });


        if(!isInternetOn){

            Toast.makeText(getApplicationContext(), "Please turn on your internet connection", Toast.LENGTH_SHORT).show();


        }
//


        storePicBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

          //     imagePickerDialoge();

                Intent intent = new Intent(RegistrationActivitySecond.this,
                        ImageSelectActivity.class);

                intent.putExtra("token","store" +
                        "" +
                        "" +
                        "" +
                        "");


                startActivity(intent);

            }
        });

        aditionalStorePicBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(Action.ACTION_MULTIPLE_PICK);
                startActivityForResult(i, 200);
            }
        });




        nextPage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                commercialNameString = commercialName.getText().toString().trim();
               // addressString = shopAddress.getText().toString().trim();
                phoneString= phone.getText().toString().trim();
                webNameString = webName.getText().toString().trim();

                storeLandLineString = storelandLine.getText().toString();



                if(nullCheck()) {

                    new AsyncTaskWeNameValid().execute();

                 }

            }
        });


        previousPage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                commercialNameString = commercialName.getText().toString().trim();
                addressString = shopAddress.getText().toString().trim();
                phoneString = phone.getText().toString().trim();
                webNameString = webName.getText().toString().trim();
                storeLandLineString = storelandLine.getText().toString();

                Intent intent = new Intent(getApplicationContext(), RegistrationActivityFirst.class);


                intent.putExtra("firstName", firstNameString);
                intent.putExtra("lastName", lastNameString);
                intent.putExtra("email", emailString);
                intent.putExtra("password", passwordString1);
                intent.putExtra("personalPicPath", personalImagePath);

                intent.putExtra("commercialName", commercialNameString);
                intent.putExtra("address", addressString);
                intent.putExtra("webName", webNameString);
                intent.putExtra("phone", phoneString);
                intent.putExtra("landLine", storeLandLineString);
                intent.putExtra("storeImage", storeImageString);
                intent.putExtra("callOption", callOption);


                intent.putExtra("legalName", legalNameString);
                intent.putExtra("legalImage", legalCertificateImage);
                intent.putStringArrayListExtra("hours", hours);
                intent.putIntegerArrayListExtra("hoursInt", timeListInt);
                intent.putStringArrayListExtra("additionalPic", additaionalImagePath);


                intent.putExtra("deliveryOp0", deliveryOption0);
                intent.putExtra("deliveryOp1", deliveryOption1);
                intent.putExtra("deliveryOp2", deliveryOption2);
                intent.putExtra("deliveryOp3", deliveryOption3);
                intent.putExtra("deliveryOp4", deliveryOption4);


                intent.putExtra("deliveryOp2value", deliveryOption2Value);
                intent.putExtra("deliveryOp3value", deliveryOption3Value);
                intent.putExtra("deliveryOp4value", deliveryOption4Value);

                startActivity(intent);
                finish();
            }
        });



        if(phoneString.isEmpty()){
            getData();
        }


    }


    public boolean checkLocation(){

        boolean status=false;

        addressContent = addressString.split("\\, ");

        if(addressContent.length!=0){

            for(int i=0;i<addressContent.length;i++) {
            String temp = addressContent[i];

                Log.e("temp",temp);
                if(temp.equals("NY")||temp.contains("NJ")||temp.contains("new york")||temp.contains("new jersy")||temp.contains("New York")||temp.contains("New Jersy")){

                    status = true;
                    break;
                }
            }
        }

        return status;
    }

    @Override
    protected void onResume() {

        storeImageString = sharePref.getshareprefdatastring(SharePref.STORE_IMAGE_PATH);
        super.onResume();
    }

    class AsyncTaskWeNameValid extends AsyncTask<String, String, String> {


        @Override
        protected void onPostExecute(String result) {

            progressSweetAlertDialog.dismiss();
            if (success == 1) {


                try {





                    Intent intent = new Intent(getApplicationContext(), RegistrationActivityThird.class);

                    intent.putExtra("firstName", firstNameString);
                    intent.putExtra("lastName", lastNameString);
                    intent.putExtra("email", emailString);
                    intent.putExtra("password", passwordString1);
                    intent.putExtra("personalPicPath", personalImagePath);


                    intent.putExtra("landLine", storeLandLineString);
                    intent.putExtra("commercialName", commercialNameString);
                    intent.putExtra("address", addressString);
                    intent.putExtra("webName", webNameString);
                    intent.putExtra("phone", phoneString);
                    intent.putExtra("storeImage", storeImageString);
                    intent.putExtra("callOption", callOption);

                    intent.putExtra("legalName", legalNameString);
                    intent.putExtra("legalImage", legalCertificateImage);
                    intent.putStringArrayListExtra("hours", hours);
                    intent.putIntegerArrayListExtra("hoursInt", timeListInt);

                    intent.putExtra("deliveryOp0", deliveryOption0);
                    intent.putExtra("deliveryOp1", deliveryOption1);
                    intent.putExtra("deliveryOp2", deliveryOption2);
                    intent.putExtra("deliveryOp3",deliveryOption3);
                    intent.putExtra("deliveryOp4",deliveryOption4);

                    intent.putStringArrayListExtra("additionalPic", additaionalImagePath);
                    intent.putExtra("deliveryOp2value",deliveryOption2Value);
                    intent.putExtra("deliveryOp3value", deliveryOption3Value);
                    intent.putExtra("deliveryOp4value", deliveryOption4Value);

                    //  intent.putExtra("personalPicPath", personalImagePath);


                    startActivity(intent);

                    finish();


                }catch (Exception e){

                }

            } else {

                doneDialog = new SweetAlertDialog(RegistrationActivitySecond.this,SweetAlertDialog.ERROR_TYPE);
                doneDialog.setTitleText("ERROR");
                doneDialog.setContentText("This web address is already in use");

                doneDialog.show();
            }




        }

        @Override
        protected String doInBackground(String... params) {

            List<NameValuePair> pair = new ArrayList<NameValuePair>();




            pair.add(new BasicNameValuePair("userType", "1"));

            pair.add(new BasicNameValuePair("storeWebAddress", webNameString));


            json = jsonParser.makeHttpRequest(Url.WEBNAMECHECK, "POST", pair);




            try {

                success = json.getInt("success");
                message = json.getString("message");






            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPreExecute() {

            try {

                progressSweetAlertDialog.show();

                //   Toast.makeText(getApplicationContext(), rateID, Toast.LENGTH_SHORT).show();
            } catch (IndexOutOfBoundsException e) {
                Toast.makeText(getApplicationContext(), "no data", Toast.LENGTH_SHORT).show();
            }

        }

    }


    public void imagePickerDialoge(){
        final Dialog dialog = new Dialog(this);
        // Include dialog.xml file
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.choose_image_dialog);
        // Set dialog title
        //dialog.setTitle("Category Add");

        dialog.show();

        Button done = (Button) dialog.findViewById(R.id.done);
        final Button crop = (Button) dialog.findViewById(R.id.crop);

        Button exit = (Button) dialog.findViewById(R.id.cancel);
        TextView gallery = (TextView) dialog.findViewById(R.id.gallery);

        TextView camera = (TextView) dialog.findViewById(R.id.camera);

        final ImageView imageView = (ImageView) dialog.findViewById(R.id.image);

        // save = (Button) dialog.findViewById(R.id.submit);


        crop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                File file = new File(storeImageString);

                try {
                    mImageCropper.cropImage(file);
                   }catch (Exception e){
                Log.d("error",e+"");
            }

            }
        });

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();



            }
        });

        mImageFileSelector = new ImageFileSelector(this);
        mImageFileSelector.setCallback(new ImageFileSelector.Callback() {
            @Override
            public void onSuccess(final String file) {
                if (!TextUtils.isEmpty(file)) {


                    final Bitmap bitmap = BitmapFactory.decodeFile(file);
                    File imageFile = new File(file);



                    storeImageString =   imageFile.getPath();

                    imageView.setImageBitmap(bitmap);
                    imageView.setVisibility(View.VISIBLE);
                    crop.setVisibility(View.VISIBLE);


                } else {
                    Toast.makeText(getApplicationContext(), "select image file error", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onError() {
                Toast.makeText(getApplicationContext(), "select image file error", Toast.LENGTH_LONG).show();
            }
        });

        mImageCropper = new ImageCropper(this);
        mImageCropper.setCallback(new ImageCropper.ImageCropperCallback() {
            @Override
            public void onCropperCallback(ImageCropper.CropperResult result, File srcFile, File outFile) {

                storeImageString = "";
                //  mBtnCrop.setVisibility(View.GONE);
               try {
                   if (result == ImageCropper.CropperResult.success) {
                       storeImageString = outFile.getPath();

                       final Bitmap bitmap = BitmapFactory.decodeFile(String.valueOf(outFile));
                       File imageFile = new File(String.valueOf(outFile));


                       storeImageString = imageFile.getPath();

                       imageView.setImageBitmap(bitmap);
                       crop.setVisibility(View.VISIBLE);

                   } else if (result == ImageCropper.CropperResult.error_illegal_input_file) {
                       Toast.makeText(getApplicationContext(), "input file error", Toast.LENGTH_LONG).show();
                   } else if (result == ImageCropper.CropperResult.error_illegal_out_file) {
                       Toast.makeText(getApplicationContext(), "output file error", Toast.LENGTH_LONG).show();
                   }
              }catch (Exception e){
                Log.d("error",e+"");
            }
            }
        });





        gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             try{
                mImageFileSelector.selectImage(RegistrationActivitySecond.this);
            }catch (Exception e){
                Log.d("error",e+"");
            }
            }
        });


        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{
                    mImageFileSelector.takePhoto(RegistrationActivitySecond.this);
            }catch (Exception e){
                Log.d("error",e+"");
            }

            }
        });

        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                storeImageString="";
            }
        });



    }





//    @Override
//    protected void onSaveInstanceState(Bundle outState) {
//        super.onSaveInstanceState(outState);
//    //    mImageFileSelector.onSaveInstanceState(outState);
//      //  mImageCropper.onSaveInstanceState(outState);
//    }
//
//    @Override
//    protected void onRestoreInstanceState(Bundle savedInstanceState) {
//        super.onRestoreInstanceState(savedInstanceState);
//      //  mImageFileSelector.onRestoreInstanceState(savedInstanceState);
////        mImageCropper.onRestoreInstanceState(savedInstanceState);
//    }
//
//    @SuppressWarnings("NullableProblems")
//    @Override
//    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
//        try{
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
//        mImageFileSelector.onRequestPermissionsResult(requestCode, permissions, grantResults);
//    }catch (Exception e){
//        Log.d("error",e+"");
//    }
//    }


    @Override
    public void onBackPressed() {

        commercialNameString = commercialName.getText().toString().trim();
        addressString = shopAddress.getText().toString().trim();
        phoneString= phone.getText().toString().trim();
        webNameString = webName.getText().toString().trim();
        storeLandLineString = storelandLine.getText().toString();

        Intent intent = new Intent(getApplicationContext(), RegistrationActivityFirst.class);


        intent.putExtra("firstName", firstNameString);
        intent.putExtra("lastName", lastNameString);
        intent.putExtra("email",emailString);
        intent.putExtra("password",passwordString1);
        intent.putExtra("personalPicPath", personalImagePath);

        intent.putExtra("commercialName", commercialNameString);
        intent.putExtra("address", addressString);
        intent.putExtra("webName", webNameString);
        intent.putExtra("phone", phoneString);
        intent.putExtra("landLine", storeLandLineString);
        intent.putExtra("storeImage", storeImageString);
        intent.putExtra("callOption", callOption);


        intent.putExtra("legalName", legalNameString);
        intent.putExtra("legalImage", legalCertificateImage);
        intent.putStringArrayListExtra("hours", hours);
        intent.putIntegerArrayListExtra("hoursInt", timeListInt);
        intent.putStringArrayListExtra("additionalPic", additaionalImagePath);

        intent.putExtra("deliveryOp0", deliveryOption0);
        intent.putExtra("deliveryOp1", deliveryOption1);
        intent.putExtra("deliveryOp2", deliveryOption2);
        intent.putExtra("deliveryOp3",deliveryOption3);
        intent.putExtra("deliveryOp4",deliveryOption4);


        intent.putExtra("deliveryOp2value",deliveryOption2Value);
        intent.putExtra("deliveryOp3value",deliveryOption3Value);
        intent.putExtra("deliveryOp4value",deliveryOption4Value);

        startActivity(intent);
        finish();

        super.onBackPressed();
    }


//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//
//        try {
//            super.onActivityResult(requestCode, resultCode, data);
//            mImageFileSelector.onActivityResult(requestCode, resultCode, data);
//            mImageCropper.onActivityResult(requestCode, resultCode, data);
//            if (requestCode == 100 && resultCode == Activity.RESULT_OK) {
////            adapter.clear();
////
////            viewSwitcher.setDisplayedChild(1);
//                single_path = data.getStringExtra("single_path");
//                //  imageLoader.displayImage("file://" + single_path, imgSinglePick);
//
//
//                storeImageString = single_path;
//
//                Log.e("pic", storeImageString);
//                // Uri uri = data.getData();
//
//                Uri uri = Uri.parse(storeImageString);
//
//
//            } else if (requestCode == 200 && resultCode == Activity.RESULT_OK) {
//                String[] all_path = data.getStringArrayExtra("all_path");
//
//                additaionalImagePath = new ArrayList();
//
//                additaionalImagePath.clear();
//
//                ArrayList<CustomGallery> dataT = new ArrayList<CustomGallery>();
//
//                for (String string : all_path) {
//                    CustomGallery item = new CustomGallery();
//                    item.sdcardPath = string;
//
//                    dataT.add(item);
//                }
//
//                if (dataT.size() != 0) {
//
//                    for (int i = 0; i < dataT.size(); i++) {
//
//
//                        additaionalImagePath.add(dataT.get(i).sdcardPath);
//
//                    }
//                }
//
//                // Log.e("sdCard",additaionalImagePath.size()+"");
//
//
////            viewSwitcher.setDisplayedChild(0);
////            adapter.addAll(dataT);
//            }
//        }catch (Exception e){
//
//        }
//    }




    public boolean nullCheck() {
        boolean flag = false;

        if (!commercialName.getText().toString().trim().equalsIgnoreCase("")) {
            if (!phone.getText().toString().trim().equalsIgnoreCase("")) {
                if (!addressString.equalsIgnoreCase("")) {
                    if (!storeImageString.equalsIgnoreCase("")) {
                        if (!webName.getText().toString().trim().equalsIgnoreCase("")) {

                            if (!storelandLine.getText().toString().trim().equalsIgnoreCase("")) {
                                return true;
                            }else{
                                msg = "Please Enter your Land Phone Number!";
                            }

                        }else {

                            msg = "Please Enter a Weblink Name!";
                        }
                    } else {
                        msg = "Please Enter Your Store Image!";
                    }
                } else {
                    msg = "Please Enter Your Shop Address!";
                }

            } else {
                msg = "Please Enter Your Phone Number!";
            }

        } else {
            msg = "Please Enter Commercial Name Of Your Store!";
        }


        if(!flag) {
            new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                    .setTitleText("Oops...")
                    .setContentText(msg)
                    .show();
        }



        return flag;


    }


    public static ArrayList<String> autocomplete(String input) {
        ArrayList<String> resultList = null;

        HttpURLConnection conn = null;
        StringBuilder jsonResults = new StringBuilder();
        try {
            StringBuilder sb = new StringBuilder(PLACES_API_BASE + TYPE_AUTOCOMPLETE + OUT_JSON);
            sb.append("?key=" + API_KEY);
//            sb.append("&components=country:gr");
            //  sb.append("&components=country:gr");
            sb.append("&input=" + URLEncoder.encode(input, "utf8"));

            URL url = new URL(sb.toString());

            System.out.println("URL: "+url);
            conn = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(conn.getInputStream());

            // Load the results into a StringBuilder
            int read;
            char[] buff = new char[1024];
            while ((read = in.read(buff)) != -1) {
                jsonResults.append(buff, 0, read);
            }
        } catch (MalformedURLException e) {
            Log.e("address", "Error processing Places API URL", e);
            return resultList;
        } catch (IOException e) {
            Log.e("address", "Error connecting to Places API", e);
            return resultList;
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }

        try {

            // Create a JSON object hierarchy from the results
            JSONObject jsonObj = new JSONObject(jsonResults.toString());
            JSONArray predsJsonArray = jsonObj.getJSONArray("predictions");

            Log.e("josn",jsonObj+"");
            // Extract the Place descriptions from the results
            resultList = new ArrayList<String>(predsJsonArray.length());
            for (int i = 0; i < predsJsonArray.length(); i++) {
                System.out.println(predsJsonArray.getJSONObject(i).getString("description"));
                System.out.println("============================================================");
                resultList.add(predsJsonArray.getJSONObject(i).getString("description"));
            }
        } catch (JSONException e) {
            Log.e("address", "Cannot process JSON results", e);
        }

        return resultList;
    }

    class GooglePlacesAutocompleteAdapter extends ArrayAdapter<String> implements Filterable {


        private ArrayList<String> resultList;

        public GooglePlacesAutocompleteAdapter(Context context, int textViewResourceId) {
            super(context, textViewResourceId);
        }

        @Override
        public int getCount() {
            return resultList.size();
        }

        @Override
        public String getItem(int index) {
            return resultList.get(index);
        }

        @Override
        public Filter getFilter() {
            Filter filter = new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence constraint) {


                    FilterResults filterResults = new FilterResults();
                       try{
                    if (constraint != null) {
                        // Retrieve the autocomplete results.
                        resultList = autocomplete(constraint.toString());

                        // Assign the data to the FilterResults
                        filterResults.values = resultList;
                        filterResults.count = resultList.size();
                    }
                       }catch (Exception e){
                        Log.d("error",e+"");
                    }
                    return filterResults;

                }

                @Override
                protected void publishResults(CharSequence constraint, FilterResults results) {
                    try{
                    if (results != null && results.count > 0) {
                        notifyDataSetChanged();
                    } else {
                        notifyDataSetInvalidated();
                    }
                    }catch (Exception e){
                        Log.d("error",e+"");
                    }
                }
            };
            return filter;
        }
    }

    public void getData() {

        String possibleEmail = "";
        //  String data = emailFetcher.getName(getApplicationContext());

        try {

            TelephonyManager tManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);

            phone.setText(tManager.getLine1Number());
        }catch (Exception e){

        }


    }



}
