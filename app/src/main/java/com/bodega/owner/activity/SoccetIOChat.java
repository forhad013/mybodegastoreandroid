package com.bodega.owner.activity;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bodega.owner.R;
import com.bodega.owner.adapter.NewMessageListAdapter;
import com.bodega.owner.databasemanager.JSONParser;
import com.bodega.owner.databasemanager.MyDatabase;
import com.bodega.owner.model.NewMessageItem;
import com.bodega.owner.twillio.TwillioCall;
import com.bodega.owner.utils.ConnectionDetector;
import com.bodega.owner.utils.SharePref;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;

import cn.pedant.SweetAlert.SweetAlertDialog;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;



public class SoccetIOChat extends AppCompatActivity {

    String shopID,shopName;

    TextView shopNameView;

    EditText messageEditText;

    int success;

    int error=0;

    ImageButton backBtn;

    String message;
    SweetAlertDialog progressSweetAlertDialog, normalDialog, doneDialog;

    String conversationID;

    String storeContact,storeImage,manager_name;


    MyDatabase db;

    LinearLayout callBtn;
    ImageButton sendBtn;

    ListView messageList;

    String sendingMsg,userID;

    ConnectionDetector cd;
    String userName;

    boolean intStatus;

    SharePref sharePref;

    JSONParser jsonParser;
    String senderID,senderName,shopEmail;

    NewMessageListAdapter singleMessageListAdapter;

    HashMap<String,String> roomMap;

    public ArrayList<NewMessageItem> newMessageItemArrayList,MessageItemList;

    RelativeLayout relativeLayout;


    SimpleDateFormat targetFormat = new SimpleDateFormat("yyyy-MM-dd");
    SimpleDateFormat newtargetFormat = new SimpleDateFormat("MM-dd-yyyy");
    SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm-a");

    SimpleDateFormat databaseDateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    SimpleDateFormat chatDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm-a");

  //  2016-05-17 21:29:36
    Socket mSocket;
//    {
//        try {
//            mSocket = IO.socket(Url.SOCKETCHATIP);
//        } catch (URISyntaxException e) {}
//    }


    private Emitter.Listener handleIncomingMessages = new Emitter.Listener(){
        @Override
        public void call(final Object... args){
              runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    Log.e("data","run");
                     JSONObject data = (JSONObject) args[0];
//
                   Log.e("data",data+"");
                    try {
                        Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                        Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
                        r.play();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    try{

                        String message = data.getString("message");
                        String dateNdTime  = data.getString("date");


                        String senderId = data.getString("sender_id");

                        String senderName = data.getString("sender_name");

                        String receiver_id = data.getString("receiver_id");

                        String receiver_name = data.getString("receiver_name");

                        int id = 0;
                        Date dateFromDb = databaseDateformat.parse(dateNdTime);
                        Calendar c = Calendar.getInstance();

                        c.setTime(dateFromDb);
                        c.setTimeZone(TimeZone.getDefault());

                        String s=  getCurrentTimezoneOffset();
                        s= s.replace("G","");
                        s= s.replace("M","");
                        s= s.replace("T","");


                        boolean hasPlusSign = s.contains("+");

                        // Log.e("asd",hasPlusSign+"");
                        if(hasPlusSign){
                            s= s.replace("+","");
                            String[] parts = s.split(":");
                            c.add(Calendar.HOUR,Integer.parseInt(parts[0]));
                            c.add(Calendar.MINUTE,Integer.parseInt(parts[1]));
                        }else{
                            s=   s.replace("-","");
                            String[] parts = s.split(":");
                            int k = - Integer.parseInt(parts[0]);
                            int j = - Integer.parseInt(parts[1]);

                            c.add(Calendar.HOUR,k);
                            c.add(Calendar.MINUTE,j);
                        }


                        Date dbTime=c.getTime();

                        // Log.e("parse",timeFormat.format(dbTime));

                        dateNdTime = chatDateFormat.format(dbTime);


                        String[] msgTime = dateNdTime.split("\\ ");

                        String date = msgTime[0];
                        String time = msgTime[1];

                        if(receiver_id.equals(userID)) {

                            NewMessageItem newMessageItem = new NewMessageItem(id + "", senderId + "", senderName, message, time, date, 1, receiver_id + "", receiver_name);

                            //    newMessageItemArrayList.add(newMessageItem);

                            MessageItemList.add(newMessageItem);

                            singleMessageListAdapter.notifyDataSetChanged();
                            messageList.setSelection(singleMessageListAdapter.getCount() - 1);

                            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(relativeLayout.getWindowToken(), 0);
                        }

                     //   sendNotification(message,senderId,senderName,receiver_id,date,time);

                    }catch (Exception e){
                        e.printStackTrace();
                    }
 

                }
            });
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_message_acitivity);


        //SocketLogin socketLogin =  new SocketLogin(getApplicationContext());

        callBtn = (LinearLayout) findViewById(R.id.call);

      //  mSocket.on("message", handleIncomingMessages);


        shopID = getIntent().getStringExtra("shopId");

//        Log.e("shop",shopID);

        shopName = getIntent().getStringExtra("shopName");

        shopEmail = getIntent().getStringExtra("shopEmail");



        roomMap = new HashMap<>();

        sharePref = new SharePref(getApplicationContext());

        backBtn = (ImageButton) findViewById(R.id.backBtn);

        jsonParser = new JSONParser();
        shopNameView = (TextView) findViewById(R.id.title);

        messageEditText = (EditText) findViewById(R.id.message_text);

        messageList = (ListView) findViewById(R.id.msgList);

        relativeLayout = (RelativeLayout) findViewById(R.id.rel);

        sendBtn = (ImageButton) findViewById(R.id.send);

        db = new MyDatabase(getApplicationContext());

        MessageItemList = new ArrayList<>();
        userID = sharePref.getshareprefdatastring(SharePref.USERID);

              shopNameView.setText(shopName);

        newMessageItemArrayList = new ArrayList<>();
        senderID = sharePref.getshareprefdatastring(SharePref.USERID);

        senderName = sharePref.getshareprefdatastring(SharePref.USERFIRSTNAME + SharePref.USERLASTNAME);

        MessageItemList = new ArrayList<>();

        cd = new ConnectionDetector(getApplicationContext());


        intStatus = cd.isConnectingToInternet();
        TimeZone tz = TimeZone.getDefault();


        Log.e("tz",getCurrentTimezoneOffset()+"");

        new AsyncGetAllMessage().execute();


        progressSweetAlertDialog = new SweetAlertDialog(SoccetIOChat.this, SweetAlertDialog.PROGRESS_TYPE);
        progressSweetAlertDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        progressSweetAlertDialog.setTitleText("Loading");
        progressSweetAlertDialog.setCancelable(false);

  //      db.updateRead(shopID);


        connectSocket();

        if(intStatus){
          //  new AsyncTaskLogin().execute();
        }else{

            Toast.makeText(getApplicationContext(), "Check Your Internet Connection", Toast.LENGTH_SHORT).show();


        }



        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onBackPressed();
            }
        });


        callBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {




                    Intent in = new Intent(getApplicationContext(), TwillioCall.class);

                    in.putExtra("image", storeImage);
                    in.putExtra("ownerName",shopName );



                    in.putExtra("storeContact", storeContact);



                    startActivity(in);

            }
        });


        sendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                intStatus = cd.isConnectingToInternet();

                if(intStatus) {

                    sendingMsg = messageEditText.getText().toString();



                    JSONObject jsonObject = new JSONObject();

                    try {
                        jsonObject.accumulate("sender",userID);
                        jsonObject.accumulate("receiver",shopID);
                        jsonObject.accumulate("message",sendingMsg);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                    if(!sendingMsg.isEmpty()) {

                        mSocket.emit("client_message", jsonObject);



                        Calendar c = Calendar.getInstance();
                        Date current_time = c.getTime();

                       String dateString = targetFormat.format(current_time);

                        String timeString = timeFormat.format(current_time);


                        NewMessageItem newMessageItem = new NewMessageItem(0+"",  userID+"",  userName,  sendingMsg,    timeString, dateString, 1,  shopID+"",  shopName);

                        MessageItemList.add(newMessageItem);

                        singleMessageListAdapter.notifyDataSetChanged();
                        messageList.setSelection(MessageItemList.size()-1);
                        messageEditText.setText("");

                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(relativeLayout.getWindowToken(), 0);
                    }


                }else{
                    Toast.makeText(getApplicationContext(), "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
                }

            }
        });


        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(relativeLayout.getWindowToken(), 0);

    }


    public String getCurrentTimezoneOffset() {

        TimeZone tz = TimeZone.getDefault();
        Calendar cal = GregorianCalendar.getInstance(tz);
        int offsetInMillis = tz.getOffset(cal.getTimeInMillis());

        String offset = String.format("%02d:%02d", Math.abs(offsetInMillis / 3600000), Math.abs((offsetInMillis / 60000) % 60));
        offset = "GMT"+(offsetInMillis >= 0 ? "+" : "-") + offset;

        return offset;
    }

    public void connectSocket(){
        ChatApplication app = (ChatApplication) getApplication();
        mSocket = app.getSocket();

        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.accumulate("userid",userID);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        mSocket.emit("register_online", jsonObject);//
     //   mSocket.on("joined", onNewMessage);
        mSocket.on("server_message", handleIncomingMessages);

        mSocket.on(Socket.EVENT_CONNECT_ERROR, onConnectError);
        mSocket.on(Socket.EVENT_CONNECT_TIMEOUT, onConnectError);



        mSocket.connect();

        // checkNewMsg();

    }

    @Override
    protected void onStop() {
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.accumulate("userid",userID);
        } catch (JSONException e) {
            e.printStackTrace();
        }




        mSocket.emit("register_offline", jsonObject);

        super.onStop();
    }

    @Override
    protected void onDestroy() {

        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.accumulate("userid",userID);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.e("Stop","Stop");
        mSocket.emit("register_offline", jsonObject);
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {

        finish();

        super.onBackPressed();
    }

    public void getAllMessage(){

        messageList.setAdapter(singleMessageListAdapter);
        messageList.setSelection(singleMessageListAdapter.getCount()-1);
    }



    @Override
    protected void onNewIntent(Intent intent) {

       // getAllMessage();
        super.onNewIntent(intent);
    }
    private Emitter.Listener onConnectError = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
             runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    for(int i=0;i<args.length;i++) {
                        Log.e("soccet", args[i] + "");
                    }
                }
            });
        }
    };

    @Override
    public void onResume() {
        new AsyncGetAllMessage().execute();
        mSocket.connect();
        super.onResume();
     //   getApplicationContext().registerReceiver(mMessageReceiver, new IntentFilter("unique_name"));
    }

    @Override
    protected void onPause() {

        super.onPause();
       // getApplicationContext().unregisterReceiver(mMessageReceiver);
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            // Extract data included in the Intent
        //    getAllMessage();

            //do other stuff here
        }
    };

//    @Override
//    protected void onDestroy() {
//        mSocket.off("chat.messages", handleIncomingMessages);
//        mSocket.off(Socket.EVENT_CONNECT_ERROR, onConnectError);
//        mSocket.off(Socket.EVENT_CONNECT_TIMEOUT, onConnectError);
//        mSocket.disconnect();
//        super.onDestroy();
//    }




    class AsyncGetAllMessage extends AsyncTask<String, String, String> {


        @Override
        protected void onPreExecute() {

            try {

                newMessageItemArrayList.clear();

                //   Toast.makeText(getActivity(), rateID, Toast.LENGTH_SHORT).show();
            } catch (IndexOutOfBoundsException e) {

            }

        }

        @Override
        protected String doInBackground(String... params) {


            List<NameValuePair> pair = new ArrayList<NameValuePair>();


        //    pair.add(new BasicNameValuePair("conversation", conversationID));


            String url ="http://192.169.227.95:3000/api/chat/"+senderID+"/"+shopID;

            JSONObject json = jsonParser.makeHttpRequest(url, "GET", pair);

            Log.e("url", url+ "");
            Log.e("json", json+ "");

            try {



                error = json.getInt("error");

                if(error==0){

                    storeContact = json.getString("sender_contact");

                    storeImage= json.getString("sender_image");


                    manager_name= json.getString("sender_name");

                   JSONArray jsonArray = json.getJSONArray("records");

                    int l = jsonArray.length();

                    for(int i=0;i<jsonArray.length();i++){

                        try{


                            JSONObject data = jsonArray.getJSONObject(i);

                            String message = data.getString("message");
                            String dateNdTime  = data.getString("date");

                            Date dateFromDb = databaseDateformat.parse(dateNdTime);

                           // dateFromDb.setT

                            Calendar c = Calendar.getInstance();

                            c.setTime(dateFromDb);
                            c.setTimeZone(TimeZone.getDefault());

                          String s=  getCurrentTimezoneOffset();
                            s= s.replace("G","");
                            s= s.replace("M","");
                            s= s.replace("T","");


                            boolean hasPlusSign = s.contains("+");

                           // Log.e("asd",hasPlusSign+"");
                            if(hasPlusSign){
                                s= s.replace("+","");
                                String[] parts = s.split(":");
                                c.add(Calendar.HOUR, Integer.parseInt(parts[0]));
                                c.add(Calendar.MINUTE, Integer.parseInt(parts[1]));
                            }else{
                                s=   s.replace("-","");
                                String[] parts = s.split(":");
                                int k = - Integer.parseInt(parts[0]);
                                int j = - Integer.parseInt(parts[1]);

                                c.add(Calendar.HOUR,k);
                                c.add(Calendar.MINUTE,j);
                            }


                            Date dbTime=c.getTime();

                           // Log.e("parse",timeFormat.format(dbTime));

                            dateNdTime = chatDateFormat.format(dbTime);

                        //    JSONObject sender = data.getJSONObject("sender_id");

                            int senderID= data.getInt("sender_id");
                            String senderName= data.getString("sender_name");


                            //JSONObject receiver = data.getJSONObject("receiver");

                            int receiverID= data.getInt("receiver_id");
                            String receiverName= data .getString("receiver_name");

                            int id = data.getInt("id");

                            String[] msgTime = dateNdTime.split("\\ ");

                            String date = msgTime[0];
                            String time = msgTime[1];

                            NewMessageItem newMessageItem = new NewMessageItem(id+"",  senderID+"",  senderName,  message,  time,  date,  1,  receiverID+"",  receiverName);

                            newMessageItemArrayList.add(newMessageItem);

                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }

                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String result) {

//           Log.e("asd", newMessageItemArrayList.size()+"");


            MessageItemList.clear();
            int l=newMessageItemArrayList.size();

            for(int i=0;i<newMessageItemArrayList.size();i++){

                MessageItemList.add(newMessageItemArrayList.get(l-i-1));

            }


            singleMessageListAdapter = new NewMessageListAdapter(getApplicationContext(), MessageItemList);



            messageList.setAdapter(singleMessageListAdapter);
            messageList.setSelection(singleMessageListAdapter.getCount() - 1);



        }

    }

    private void sendNotification(String message,String senderID,String senderName,String receiverID,String smsDate,String smsTime) {
        Intent intent = new Intent(this, SoccetIOChat.class);
        intent.putExtra("shopId",senderID);
        intent.putExtra("shopName",senderName);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_gcm,BIND_IMPORTANT)


                .setContentTitle("New Message from "+ senderName)
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }



}


