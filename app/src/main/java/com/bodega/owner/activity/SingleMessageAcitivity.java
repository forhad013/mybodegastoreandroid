package com.bodega.owner.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


import com.bodega.owner.R;
import com.bodega.owner.adapter.SingleMessageListAdapter;
import com.bodega.owner.databasemanager.JSONParser;
import com.bodega.owner.databasemanager.MessageItem;
import com.bodega.owner.databasemanager.MyDatabase;
import com.bodega.owner.databasemanager.Url;
import com.bodega.owner.utils.ConnectionDetector;
import com.bodega.owner.utils.SharePref;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class SingleMessageAcitivity extends AppCompatActivity {

    String shopID,shopName;

    TextView shopNameView;

    EditText messageEditText;

    int success;

    ImageButton backBtn;

    String message;
    SweetAlertDialog progressSweetAlertDialog, normalDialog, doneDialog;


    ArrayList<MessageItem> messageItemArrayList;


    MyDatabase db;

    ImageButton sendBtn;

    ListView messageList;

    String sendingMsg;

    ConnectionDetector cd;
    String userName;

    boolean intStatus;

    SharePref sharePref;

    JSONParser jsonParser;
    String senderID,senderName;

    SingleMessageListAdapter singleMessageListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_message_acitivity);

        shopID = getIntent().getStringExtra("shopID");

        shopName = getIntent().getStringExtra("shopName");

        sharePref = new SharePref(getApplicationContext());

        backBtn = (ImageButton) findViewById(R.id.backBtn);

        jsonParser = new JSONParser();
        shopNameView = (TextView) findViewById(R.id.title);

        messageEditText = (EditText) findViewById(R.id.message_text);

        messageList = (ListView) findViewById(R.id.msgList);

        sendBtn = (ImageButton) findViewById(R.id.send);

        db = new MyDatabase(getApplicationContext());

        messageItemArrayList = new ArrayList<>();


        shopNameView.setText(shopName);


        senderID = sharePref.getshareprefdatastring(SharePref.USERID);

        senderName = sharePref.getshareprefdatastring(SharePref.USERFIRSTNAME);


        cd = new ConnectionDetector(getApplicationContext());


        intStatus = cd.isConnectingToInternet();


        progressSweetAlertDialog = new SweetAlertDialog(SingleMessageAcitivity.this, SweetAlertDialog.PROGRESS_TYPE);
        progressSweetAlertDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        progressSweetAlertDialog.setTitleText("Loading");
        progressSweetAlertDialog.setCancelable(false);

        //db.updateRead(shopID);


        getAllMessage();



        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onBackPressed();
            }
        });

        sendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                intStatus = cd.isConnectingToInternet();

                if(intStatus) {

                    sendingMsg = messageEditText.getText().toString();
                    sendingMsg = messageEditText.getText().toString();

                    if(!sendingMsg.isEmpty()) {

                        sendBtn.setEnabled(false);
                        new AsyncTaskMessage().execute();

                    }


                }else{
                    Toast.makeText(getApplicationContext(), "Check Your Internet Connection", Toast.LENGTH_SHORT).show();
                }

            }
        });



    }


    @Override
    public void onBackPressed() {

        finish();

        super.onBackPressed();
    }

    public void getAllMessage(){

        messageItemArrayList.clear();


        messageItemArrayList = db.getList(shopID);

        singleMessageListAdapter = new SingleMessageListAdapter(getApplicationContext(),messageItemArrayList);

        messageList.setAdapter(singleMessageListAdapter);
        messageList.setSelection(singleMessageListAdapter.getCount()-1);
    }


    class AsyncTaskMessage extends AsyncTask<String, String, String> {


        @Override
        protected void onPostExecute(String result) {
            if (success == 1) {



                db.saveMessage(senderID, shopID, shopName, sendingMsg, "outgoing", "1");

              messageEditText.setText("");

                getAllMessage();




            } else
                Toast.makeText(getApplicationContext(), "Message sending failed", Toast.LENGTH_SHORT).show();

            progressSweetAlertDialog.dismiss();
            sendBtn.setEnabled(true);
        }

        @Override
        protected String doInBackground(String... params) {



            List<NameValuePair> pair = new ArrayList<NameValuePair>();



            pair.add(new BasicNameValuePair("smsContent", sendingMsg));
            pair.add(new BasicNameValuePair("senderID", senderID));

            pair.add(new BasicNameValuePair("senderName",senderName));
            pair.add(new BasicNameValuePair("receiverID", shopID));

            pair.add(new BasicNameValuePair("receiverName", shopName));
            pair.add(new BasicNameValuePair("userType", "1"));

            JSONObject json = jsonParser.makeHttpRequest(Url.SEND_MESSAGE, "POST", pair);

            Log.e("pair", pair + "");

            Log.e("reg1111", json + "");



            try {

                success = json.getInt("success");
                message = json.getString("message");







            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPreExecute() {

            try {

                Log.e("asd", "asdasd");
             //   progressSweetAlertDialog.show();

                //   Toast.makeText(getApplicationContext(), rateID, Toast.LENGTH_SHORT).show();
            } catch (IndexOutOfBoundsException e) {
                Toast.makeText(getApplicationContext(), "no data", Toast.LENGTH_SHORT).show();
            }

        }

    }

    @Override
    protected void onNewIntent(Intent intent) {

        getAllMessage();
        super.onNewIntent(intent);
    }


    @Override
    public void onResume() {
        super.onResume();
        getApplicationContext() .registerReceiver(mMessageReceiver, new IntentFilter("unique_name"));
    }

    @Override
    protected void onPause() {
        super.onPause();
        getApplicationContext().unregisterReceiver(mMessageReceiver);
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            // Extract data included in the Intent
            getAllMessage();

            //do other stuff here
        }
    };
}


