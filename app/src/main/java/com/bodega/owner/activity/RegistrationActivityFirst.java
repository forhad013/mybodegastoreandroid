package com.bodega.owner.activity;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Typeface;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bodega.owner.R;
import com.bodega.owner.databasemanager.JSONParser;
import com.bodega.owner.databasemanager.Url;
import com.bodega.owner.utils.ConnectionDetector;
import com.bodega.owner.utils.SharePref;
import com.sw926.imagefileselector.ImageCropper;
import com.sw926.imagefileselector.ImageFileSelector;

import org.json.JSONObject;

import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class RegistrationActivityFirst extends AppCompatActivity {

    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;


    String storeLandLineString;

    SharePref sharePref;

    String id,phone;

    ConnectionDetector cd;

    Context context;

    String msg;
    int success = 0;

    private int PICK_IMAGE_REQUEST = 1;

    String personalIamgePath;

   // ImageView imageView;

    String commercialNameString,webNameString,phoneString,storeImageString,addressString;
    Url url;
    JSONObject json;
    private static int IMAGE_PICKER_SELECT = 1;
    ImageButton backBtn;

    boolean isInternetOn=false;
    ImageButton secondNxtBtn;
    ArrayList additaionalImagePath;

    ImageButton nextPage,previousPage;

    boolean deliveryOption0,deliveryOption1,deliveryOption2,deliveryOption3,deliveryOption4;

    String deliveryOption2Value,deliveryOption3Value,deliveryOption4Value;


    LinearLayout lin;

  //  String userID,gcmCode,shopNameString,addressString,passwordString1,passwordString2,contactString,emailString,message;

    JSONParser jsonParser;

    ImageCropper mImageCropper;

    SweetAlertDialog progressSweetAlertDialog, normalDialog,doneDialog;

    private static final String LOG_TAG = "Google Places Autocomplete";

    private static final String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place";

    private static final String TYPE_AUTOCOMPLETE = "/autocomplete";

    private static final String OUT_JSON = "/json";
    String legalNameString,legalCertificateImage;

    private static final String API_KEY =   "AIzaSyB_wVRhZ1qu9IVJO3JyS_fIPIfOkoLSjUA";

    InputMethodManager imm;
    EditText firstName,lastName,email,password1,password2;

    ImageButton personalPicBtn;

    Typeface mytTypeface;
    ArrayList <Integer> timeListInt;

    ArrayList hours;

    ImageFileSelector mImageFileSelector;


    TextView regInfo,about_you,picTitle;

    String callOption;

    String firstNameString,lastNameString,emailString,password1String,password2String;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration_first);



cd = new ConnectionDetector(getApplicationContext());


        additaionalImagePath = new ArrayList();
        jsonParser = new JSONParser();
        url = new Url();
        sharePref = new SharePref(getApplicationContext());

        mytTypeface = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Regular.ttf");
        nextPage = (ImageButton) findViewById(R.id.btnNext);

        previousPage = (ImageButton) findViewById(R.id.btnPrev);

        mImageFileSelector.setDebug(true);
        timeListInt = new ArrayList<>();
        personalPicBtn = (ImageButton) findViewById(R.id.personalPic);

        regInfo = (TextView) findViewById(R.id.reg_info);

        about_you = (TextView) findViewById(R.id.about_you);

        picTitle = (TextView) findViewById(R.id.picTitle);

        context = getApplicationContext();
        secondNxtBtn = (ImageButton) findViewById(R.id.btnNext2);

        firstName = (EditText) findViewById(R.id.firstName);
        lastName= (EditText) findViewById(R.id.lastName);
        email =  (EditText) findViewById(R.id.email);
        password1= (EditText) findViewById(R.id.password1);





        hours = new ArrayList();

        hours = getIntent().getStringArrayListExtra("hours");


        password1.setTypeface(mytTypeface);


        callOption = getIntent().getStringExtra("callOption");
        firstNameString = getIntent().getStringExtra("firstName");
        lastNameString = getIntent().getStringExtra("lastName");
        emailString = getIntent().getStringExtra("email");
        password1String = getIntent().getStringExtra("password");
        personalIamgePath = getIntent().getStringExtra("personalPicPath");



        storeLandLineString = getIntent().getStringExtra("landLine");
        commercialNameString = getIntent().getStringExtra("commercialName");
        addressString = getIntent().getStringExtra("address");
        phoneString= getIntent().getStringExtra("phone");
        webNameString = getIntent().getStringExtra("webName");
        storeImageString = getIntent().getStringExtra("storeImage");

        legalCertificateImage = getIntent().getStringExtra("legalImage");

        legalNameString = getIntent().getStringExtra("legalName");

        timeListInt = getIntent().getIntegerArrayListExtra("hoursInt");

        hours = getIntent().getIntegerArrayListExtra("hours");

        deliveryOption2Value = getIntent().getStringExtra("deliveryOp2value");
        deliveryOption3Value = getIntent().getStringExtra("deliveryOp3value");
        deliveryOption4Value = getIntent().getStringExtra("deliveryOp4value");

        deliveryOption1 = getIntent().getBooleanExtra("deliveryOp1", false);
        deliveryOption0 = getIntent().getBooleanExtra("deliveryOp0", false);
        deliveryOption2 = getIntent().getBooleanExtra("deliveryOp2", false);

        deliveryOption3 = getIntent().getBooleanExtra("deliveryOp3", false);
        deliveryOption4 = getIntent().getBooleanExtra("deliveryOp4", false);

        additaionalImagePath = getIntent().getStringArrayListExtra("additionalPic");



//        isInternetOn = cd.isConnectingToInternet();
//
//        if(isInternetOn){
//
//            SocketToken socketToken = new SocketToken(getApplicationContext());
//        }else{
//
//            doneDialog=  new SweetAlertDialog(RegistrationActivityFirst.this,SweetAlertDialog.ERROR_TYPE);
//            doneDialog.setTitleText("Error");
//            doneDialog.setContentText("Please turn on the internet connection and Restart the application");
//            doneDialog.show();
//            doneDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
//                @Override
//                public void onClick(SweetAlertDialog sweetAlertDialog) {
//
//
//                    finish();
//
//
//                }
//            });
//
//
//        }

        storeImageString=storeImageString+"";

        lin = (LinearLayout) findViewById(R.id.lin);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        imm = (InputMethodManager)  getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(lin.getWindowToken(), 0);


        legalCertificateImage=legalCertificateImage + "";
        Log.e("pic", addressString + "");


        firstName.setText(firstNameString);
        lastName.setText(lastNameString);

        email.setText(emailString);

        password1.setText(password1String);


        if(emailString.isEmpty()){

            email.setText(getData());
        }





        if(!personalIamgePath.equals("")){


//            Uri uri = Uri.parse(personalIamgePath);
//
//            try {
//                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
//
//             imageView.setImageBitmap(bitmap);
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
        }



        previousPage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);

                startActivity(intent);


                finish();

            }
        });


        secondNxtBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                firstNameString = firstName.getText().toString().trim();
                lastNameString = lastName.getText().toString().trim();
                emailString = email.getText().toString().trim();
                password1String = password1.getText().toString().trim();


                if (nullCheck()) {
                    Intent intent = new Intent(getApplicationContext(), RegistrationActivitySecond.class);


                    Log.e("pic", firstNameString);

                    intent.putExtra("firstName", firstNameString);
                    intent.putExtra("lastName", lastNameString);
                    intent.putExtra("email", emailString);
                    intent.putExtra("password", password1String);
                    intent.putExtra("personalPicPath", personalIamgePath);

                    intent.putExtra("landLine", storeLandLineString);
                    intent.putExtra("commercialName", commercialNameString);
                    intent.putExtra("address", addressString);
                    intent.putExtra("webName", webNameString);
                    intent.putExtra("phone", phoneString);
                    intent.putExtra("storeImage", storeImageString);
                    intent.putExtra("callOption", callOption);


                    intent.putExtra("legalImage", legalCertificateImage);
                    intent.putStringArrayListExtra("hours", hours);
                    intent.putIntegerArrayListExtra("hoursInt", timeListInt);
                    intent.putStringArrayListExtra("additionalPic", additaionalImagePath);

                    intent.putExtra("deliveryOp0", deliveryOption0);
                    intent.putExtra("deliveryOp1", deliveryOption1);
                    intent.putExtra("deliveryOp2", deliveryOption2);
                    intent.putExtra("deliveryOp3", deliveryOption3);
                    intent.putExtra("deliveryOp4", deliveryOption4);


                    intent.putExtra("deliveryOp2value", deliveryOption2Value);
                    intent.putExtra("deliveryOp3Value", deliveryOption3Value);
                    intent.putExtra("deliveryOp4Value", deliveryOption4Value);
                    startActivity(intent);


                    finish();

                }

            }
        });

        nextPage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                firstNameString = firstName.getText().toString().trim();
                lastNameString = lastName.getText().toString().trim();
                emailString = email.getText().toString().trim();
                password1String = password1.getText().toString().trim();



                  if(nullCheck()){
                        Intent intent = new Intent(getApplicationContext(), RegistrationActivitySecond.class);


                        Log.e("pic", firstNameString);

                      intent.putExtra("firstName", firstNameString);
                        intent.putExtra("lastName", lastNameString);
                        intent.putExtra("email",emailString);
                        intent.putExtra("password",password1String);
                        intent.putExtra("personalPicPath", personalIamgePath);

                      intent.putExtra("landLine", storeLandLineString);
                        intent.putExtra("commercialName", commercialNameString);
                        intent.putExtra("address", addressString);
                        intent.putExtra("webName", webNameString);
                        intent.putExtra("phone",phoneString);
                      intent.putExtra("callOption", callOption);


                      intent.putExtra("legalImage", legalCertificateImage);
                        intent.putExtra("storeImage", storeImageString);
                        intent.putStringArrayListExtra("hours", hours);
                    intent.putIntegerArrayListExtra("hoursInt", timeListInt);
                    intent.putStringArrayListExtra("additionalPic", additaionalImagePath);

                      intent.putExtra("deliveryOp0", deliveryOption0);
                    intent.putExtra("deliveryOp1", deliveryOption1);
                    intent.putExtra("deliveryOp2", deliveryOption2);
                    intent.putExtra("deliveryOp3",deliveryOption3);
                    intent.putExtra("deliveryOp4",deliveryOption4);


                    intent.putExtra("deliveryOp2value",deliveryOption2Value);
                    intent.putExtra("deliveryOp3Value",deliveryOption3Value);
                    intent.putExtra("deliveryOp4Value",deliveryOption4Value);
                        startActivity(intent);


                        finish();

                 }




            }
        });


        personalPicBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

              //  pickImages();

//
//                Intent intent = new Intent();
//// Show only images, no videos or anything else
//                intent.setType("image/*");
//                intent.setAction(Intent.ACTION_GET_CONTENT);
//// Always show the chooser (if there are multiple options available)
//                startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
//
//                Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//                startActivityForResult(i, IMAGE_PICKER_SELECT);

          // imagePickerDialoge();

                Intent intent = new Intent(RegistrationActivityFirst.this,
                        ImageSelectActivity.class);

                intent.putExtra("token","personal");


                startActivity(intent);

            }
        });





    }


//
//    public void imagePickerDialoge(){
//        final Dialog dialog = new Dialog(this);
//        // Include dialog.xml file
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.setContentView(R.layout.choose_image_dialog);
//        // Set dialog title
//        //dialog.setTitle("Category Add");
//
//        dialog.show();
//
//        Button done = (Button) dialog.findViewById(R.id.done);
//        final Button crop = (Button) dialog.findViewById(R.id.crop);
//
//        Button exit = (Button) dialog.findViewById(R.id.cancel);
//        TextView gallery = (TextView) dialog.findViewById(R.id.gallery);
//
//        TextView camera = (TextView) dialog.findViewById(R.id.camera);
//
//        final ImageView imageView = (ImageView) dialog.findViewById(R.id.image);
//
//        // save = (Button) dialog.findViewById(R.id.submit);
//
//
//        crop.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//              try{
//                  File file = new File(personalIamgePath);
//
//                mImageCropper.cropImage(file);
//              }catch (Exception e){
//                Log.d("error",e+"");
//            }
//
//            }
//        });
//
//        done.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                dialog.dismiss();
//               // showImage();
//
//
//            }
//        });
//
//        mImageFileSelector = new ImageFileSelector(this);
//        mImageFileSelector.setCallback(new ImageFileSelector.Callback() {
//            @Override
//            public void onSuccess(String file) {
//                if (!TextUtils.isEmpty(file)) {
//
//                    Log.e("g",file);
//                    final Bitmap bitmap = BitmapFactory.decodeFile(file);
//                    File imageFile = new File(file);
//
//
//
//                    personalIamgePath =   imageFile.getPath();
//
//
//
//                    imageView.setImageBitmap(bitmap);
//                    imageView.setVisibility(View.VISIBLE);
//                    crop.setVisibility(View.VISIBLE);
//
//
//                } else {
//                    Toast.makeText(getApplicationContext(), "select image file error", Toast.LENGTH_LONG).show();
//                }
//            }
//
//            @Override
//            public void onError() {
//                Toast.makeText(getApplicationContext(), "select image file error", Toast.LENGTH_LONG).show();
//            }
//        });
//
//        mImageCropper = new ImageCropper(this);
//        mImageCropper.setCallback(new ImageCropper.ImageCropperCallback() {
//            @Override
//            public void onCropperCallback(ImageCropper.CropperResult result, File srcFile, File outFile) {
//                personalIamgePath = "";
//              //  mBtnCrop.setVisibility(View.GONE);
//                if (result == ImageCropper.CropperResult.success) {
//                    personalIamgePath = outFile.getPath();
//
//                    final Bitmap bitmap = BitmapFactory.decodeFile(String.valueOf(outFile));
//                    File imageFile = new File(String.valueOf(outFile));
//
//
//
//                    personalIamgePath =   imageFile.getPath();
//
//                    imageView.setImageBitmap(bitmap);
//                    crop.setVisibility(View.VISIBLE);
//
//                } else if (result == ImageCropper.CropperResult.error_illegal_input_file) {
//                    Toast.makeText(getApplicationContext(), "input file error", Toast.LENGTH_LONG).show();
//                } else if (result == ImageCropper.CropperResult.error_illegal_out_file) {
//                    Toast.makeText(getApplicationContext(), "output file error", Toast.LENGTH_LONG).show();
//                }
//            }
//        });
//
//
//
//
//
//       gallery.setOnClickListener(new View.OnClickListener() {
//           @Override
//           public void onClick(View v) {
//               mImageFileSelector.selectImage(RegistrationActivityFirst.this);
//
//           }
//       });
//
//
//        camera.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                mImageFileSelector.takePhoto(RegistrationActivityFirst.this);
//
//            }
//        });
//
//        exit.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                dialog.dismiss();
//                personalIamgePath="";
//            }
//        });
//
//
//
//    }
//
//
//
//    @Override
//    protected void onSaveInstanceState(Bundle outState) {
//        super.onSaveInstanceState(outState);
////        mImageFileSelector.onSaveInstanceState(outState);
//     //   mImageCropper.onSaveInstanceState(outState);
//    }
//
//    @Override
//    protected void onRestoreInstanceState(Bundle savedInstanceState) {
//        super.onRestoreInstanceState(savedInstanceState);
//      //  mImageFileSelector.onRestoreInstanceState(savedInstanceState);
//      //  mImageCropper.onRestoreInstanceState(savedInstanceState);
//    }
//
//    @SuppressWarnings("NullableProblems")
//    @Override
//    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
//       try{
//           super.onRequestPermissionsResult(requestCode, permissions, grantResults);
//        mImageFileSelector.onRequestPermissionsResult(requestCode, permissions, grantResults); }catch (Exception e){
//        Log.d("error",e+"");
//    }
//    }

    @Override
    protected void onResume() {

        personalIamgePath = sharePref.getshareprefdatastring(SharePref.PERSONAL_IMAGE_PATH);
        super.onResume();
    }

    @Override
    public void onBackPressed() {

        Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
        startActivity(intent);

        finish();

        super.onBackPressed();
    }

//
//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        try {
//            super.onActivityResult(requestCode, resultCode, data);
//            mImageFileSelector.onActivityResult(requestCode, resultCode, data);
//            mImageCropper.onActivityResult(requestCode, resultCode, data);
//            if (requestCode == 100 && resultCode == Activity.RESULT_OK) {
////            adapter.clear();
////
////            viewSwitcher.setDisplayedChild(1);
//                String single_path = data.getStringExtra("single_path");
//                //  imageLoader.displayImage("file://" + single_path, imgSinglePick);
//
//
//                personalIamgePath = single_path;
//
//                Log.e("pic", personalIamgePath);
//                // Uri uri = data.getData();
//
//                Uri uri = Uri.parse("file://" + personalIamgePath);
//
//                try {
//                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
//
//
//                    // Log.d(TAG, String.valueOf(bitmap));
//
////                ImageView imageView = (ImageView) findViewById(R.id.imageView);
//                    //  imageView.setImageBitmap(bitmap);
//
//
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//
//            } else if (requestCode == 200 && resultCode == Activity.RESULT_OK) {
//                String[] all_path = data.getStringArrayExtra("all_path");
//
//                ArrayList<CustomGallery> dataT = new ArrayList<CustomGallery>();
//
//                for (String string : all_path) {
//                    CustomGallery item = new CustomGallery();
//                    item.sdcardPath = string;
//
//                    dataT.add(item);
//                }
//
////            viewSwitcher.setDisplayedChild(0);
////            adapter.addAll(dataT);
//            }
//        }catch (Exception e){
//            Log.d("error",e+"");
//        }
//    }


    public boolean nullCheck() {
        boolean flag = false;

        if (!firstName.getText().toString().trim().equalsIgnoreCase("")) {
            if (!lastName.getText().toString().trim().equalsIgnoreCase("")) {
                if (!password1.getText().toString().trim().equalsIgnoreCase("")) {
                    if (isEmailValid(email.getText().toString())&& !email.getText().toString().trim().equalsIgnoreCase("")) {
                        if (!personalIamgePath.equalsIgnoreCase("")) {

                                return true;

                        }else {

                            msg = "Please Select an image!";
                        }
                    } else {
                        msg = "Please Enter valid Email!";
                    }
                } else {
                    msg = "Please Enter Password!";
                }

            } else {
                msg = "Please Enter Your Last Name!";
            }

        } else {
            msg = "Please Enter Your First Name!";
        }


        if(!flag) {
            new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                    .setTitleText("Oops...")
                    .setContentText(msg)
                    .show();
        }



        return flag;


    }
    private boolean isEmailValid(String email) {

        return email.contains("@");
    }

    public String getData(){

        String possibleEmail="";



//        TelephonyManager tManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
//
//        contact.setText(tManager.getLine1Number());


        try{

            Account[] accounts = AccountManager.get(this).getAccountsByType("com.google");

            for (Account account : accounts) {

                possibleEmail = account.name;


            }
        }
        catch(Exception e)
        {
            Log.i("Exception", "Exception:"+e) ;
        }


        return possibleEmail;
    }

    public class EmailFetcher{

        String getName(Context context) {
            Cursor CR = null;
            CR = getOwner(context);
            String id = "", name = "";
            while (CR.moveToNext())
            {
                name = CR.getString(CR.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
            }
            return name;
        }





        Cursor getOwner(Context context) {
            String accountName = null;
            Cursor emailCur = null;
            AccountManager accountManager = AccountManager.get(context);
            Account[] accounts = accountManager.getAccountsByType("com.google");
            if (accounts[0].name != null) {
                accountName = accounts[0].name;
                String where = ContactsContract.CommonDataKinds.Email.DATA + " = ?";
                ArrayList<String> what = new ArrayList<String>();
                what.add(accountName);
                Log.v("Got account", "Account " + accountName);
                for (int i = 1; i < accounts.length; i++) {
                    where += " or " + ContactsContract.CommonDataKinds.Email.DATA + " = ?";
                    what.add(accounts[i].name);
                    Log.v("Got account", "Account " + accounts[i].name);
                }
                String[] whatarr = (String[])what.toArray(new String[what.size()]);
                ContentResolver cr = context.getContentResolver();
                emailCur = cr.query(ContactsContract.CommonDataKinds.Email.CONTENT_URI, null, where, whatarr, null);
                if (id != null) {
                    // get the phone number
                    Cursor pCur = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?", new String[]{id}, null);
                    while (pCur.moveToNext())
                    {
                        phone = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                        Log.v("Got contacts", "phone" + phone);
                    }
                    pCur.close();
                }
            }
            return emailCur;
        }
    }
}
