package com.bodega.owner.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.bodega.owner.R;
import com.bodega.owner.databasemanager.JSONParser;
import com.bodega.owner.databasemanager.Url;
import com.bodega.owner.utils.ConnectionDetector;
import com.bodega.owner.utils.SharePref;
import com.sw926.imagefileselector.ImageCropper;
import com.sw926.imagefileselector.ImageFileSelector;

import org.json.JSONObject;

import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class RegistrationActivityThird extends AppCompatActivity {

    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;



    SharePref sharePref;

    ConnectionDetector cd;
    int success = 0;
    ImageButton secondNxtBtn;

    ImageButton nextPage,previousPage;

    Url url;
    JSONObject json;

    String msg;

    ImageButton backBtn;

    boolean isInternetOn=false;

    String storeLandLineString;

    ImageButton legalPermitButton;
    boolean deliveryOption0,deliveryOption1,deliveryOption2,deliveryOption3,deliveryOption4;

    String deliveryOption2Value,deliveryOption3Value,deliveryOption4Value;

    ImageFileSelector mImageFileSelector;
    ImageCropper mImageCropper;
    ArrayList timeList,ampmlist;

    ArrayList hours,additaionalImagePath;

    String personalImagePath,firstNameString,lastNameString;

    String commercialNameString,webNameString,phoneString,storeImageString;
    String userID,gcmCode,shopNameString,addressString,passwordString1,passwordString2,contactString,emailString,message;

    String legalNameString,legalCertificateImage="";
    JSONParser jsonParser;

    String single_path="";

    InputMethodManager imm;

    String callOption;

    SweetAlertDialog progressSweetAlertDialog, normalDialog,doneDialog;

    private static final String LOG_TAG = "Google Places Autocomplete";

    private static final String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place";

    private static final String TYPE_AUTOCOMPLETE = "/autocomplete";

    private static final String OUT_JSON = "/json";
    LinearLayout lin;

    private static final String API_KEY =   "AIzaSyB_wVRhZ1qu9IVJO3JyS_fIPIfOkoLSjUA";


    Spinner time11,time12,time21,time22,time31,time32,time41,time42,time51,time52,time61,time62,time71,time72;

    Spinner ampm11,ampm12,ampm21,ampm22,ampm31,ampm32,ampm41,ampm42,ampm51,ampm52,ampm61,ampm62,ampm71,ampm72;


    ArrayList <Integer> timeListInt;

    EditText legalName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration_third);


        additaionalImagePath = new ArrayList();

        hours = new ArrayList();

        time11 = (Spinner) findViewById(R.id.spinnerAM1);
        time12 = (Spinner) findViewById(R.id.spinnerPM1);

        time21 = (Spinner) findViewById(R.id.spinnerAM2);
        time22 = (Spinner) findViewById(R.id.spinnerPM2);

        time31 = (Spinner) findViewById(R.id.spinnerAM3);
        time32 = (Spinner) findViewById(R.id.spinnerPM3);

        time41 = (Spinner) findViewById(R.id.spinnerAM4);
        time42 = (Spinner) findViewById(R.id.spinnerPM4);

        time51 = (Spinner) findViewById(R.id.spinnerAM5);
        time52 = (Spinner) findViewById(R.id.spinnerPM5);

        time61 = (Spinner) findViewById(R.id.spinnerAM6);
        time62 = (Spinner) findViewById(R.id.spinnerPM6);

        time71 = (Spinner) findViewById(R.id.spinnerAM7);
        time72 = (Spinner) findViewById(R.id.spinnerPM7);


        ampm11 =  (Spinner) findViewById(R.id.ampm11);
        ampm12 =  (Spinner) findViewById(R.id.ampm12);

        ampm21 =  (Spinner) findViewById(R.id.ampm21);
        ampm22 =  (Spinner) findViewById(R.id.ampm22);

        ampm31 =  (Spinner) findViewById(R.id.ampm31);
        ampm32 =  (Spinner) findViewById(R.id.ampm32);

        ampm41 =  (Spinner) findViewById(R.id.ampm41);
        ampm42 =  (Spinner) findViewById(R.id.ampm42);

        ampm51 =  (Spinner) findViewById(R.id.ampm51);
        ampm52 =  (Spinner) findViewById(R.id.ampm52);

        ampm61 =  (Spinner) findViewById(R.id.ampm61);
        ampm62 =  (Spinner) findViewById(R.id.ampm62);

        ampm71 =  (Spinner) findViewById(R.id.ampm71);
        ampm72 =  (Spinner) findViewById(R.id.ampm72);

        lin = (LinearLayout) findViewById(R.id.lin);

         getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        imm = (InputMethodManager)  getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(lin.getWindowToken(), 0);

        timeListInt = new ArrayList<>();

        ampmlist = new ArrayList();

        timeList = new ArrayList();

        ImageFileSelector.setDebug(true);
        timeList.add("00.00");
        timeList.add("01.00");
        timeList.add("02.00");
        timeList.add("03.00");
        timeList.add("04.00");
        timeList.add("05.00");
        timeList.add("06.00");
        timeList.add("07.00");
        timeList.add("08.00");
        timeList.add("09.00");
        timeList.add("10.00");
        timeList.add("11.00");
        timeList.add("12.00");

     mImageFileSelector.setDebug(true);
        ArrayAdapter<String> spinAdapter1 = new ArrayAdapter<String>(getApplicationContext(),
                R.layout.spinner_item, timeList);


        time11.setAdapter(spinAdapter1);
        time12.setAdapter(spinAdapter1);

        time21.setAdapter(spinAdapter1);
        time22.setAdapter(spinAdapter1);

        time31.setAdapter(spinAdapter1);
        time32.setAdapter(spinAdapter1);

        time41.setAdapter(spinAdapter1);
        time42.setAdapter(spinAdapter1);

        time51.setAdapter(spinAdapter1);
        time52.setAdapter(spinAdapter1);

        time61.setAdapter(spinAdapter1);
        time62.setAdapter(spinAdapter1);

        time71.setAdapter(spinAdapter1);
        time72.setAdapter(spinAdapter1);



        ampmlist.add("am");

        ampmlist.add("pm");

        ArrayAdapter<String> spinAdapter2 = new ArrayAdapter<String>(getApplicationContext(),
                R.layout.spinner_item, ampmlist);




        ampm11.setAdapter(spinAdapter2);
        ampm12.setAdapter(spinAdapter2);

        ampm21.setAdapter(spinAdapter2);
        ampm22.setAdapter(spinAdapter2);

        ampm31.setAdapter(spinAdapter2);
        ampm32.setAdapter(spinAdapter2);

        ampm41.setAdapter(spinAdapter2);
        ampm42.setAdapter(spinAdapter2);

        ampm51.setAdapter(spinAdapter2);
        ampm52.setAdapter(spinAdapter2);

        ampm61.setAdapter(spinAdapter2);
        ampm62.setAdapter(spinAdapter2);

        ampm71.setAdapter(spinAdapter2);
        ampm72.setAdapter(spinAdapter2);




cd = new ConnectionDetector(getApplicationContext());


        callOption = getIntent().getStringExtra("callOption");
        firstNameString = getIntent().getStringExtra("firstName");
        lastNameString = getIntent().getStringExtra("lastName");
        emailString = getIntent().getStringExtra("email");
        passwordString1 = getIntent().getStringExtra("password");
        personalImagePath = getIntent().getStringExtra("personalPicPath");


        commercialNameString = getIntent().getStringExtra("commercialName");
        addressString = getIntent().getStringExtra("address");
        phoneString= getIntent().getStringExtra("phone");
        webNameString = getIntent().getStringExtra("webName");
        storeImageString = getIntent().getStringExtra("storeImage");

        storeLandLineString = getIntent().getStringExtra("landLine");

        legalCertificateImage = getIntent().getStringExtra("legalImage");

        legalNameString = getIntent().getStringExtra("legalName");
        timeListInt = getIntent().getIntegerArrayListExtra("hoursInt");

        hours = getIntent().getIntegerArrayListExtra("hours");

        deliveryOption2Value = getIntent().getStringExtra("deliveryOp2value");
        deliveryOption3Value = getIntent().getStringExtra("deliveryOp3value");
        deliveryOption4Value = getIntent().getStringExtra("deliveryOp4value");

        deliveryOption1 = getIntent().getBooleanExtra("deliveryOp1", false);

        deliveryOption0 = getIntent().getBooleanExtra("deliveryOp0", false);

        deliveryOption2 = getIntent().getBooleanExtra("deliveryOp2", false);

        deliveryOption3 = getIntent().getBooleanExtra("deliveryOp3", false);
        deliveryOption4 = getIntent().getBooleanExtra("deliveryOp4", false);

        additaionalImagePath = getIntent().getStringArrayListExtra("additionalPic");




        jsonParser = new JSONParser();
        url = new Url();
        sharePref = new SharePref(getApplicationContext());

        nextPage = (ImageButton) findViewById(R.id.btnNext);


        legalName = (EditText) findViewById(R.id.legalName);

        secondNxtBtn = (ImageButton) findViewById(R.id.btnNext2);



        legalPermitButton = (ImageButton) findViewById(R.id.legalPermitBtn);

        previousPage = (ImageButton) findViewById(R.id.btnPrev);

        legalName.setText(legalNameString);

        legalCertificateImage = legalCertificateImage+"";


        if (timeListInt.size() != 0) {


            setTimeList();
        }else {
            ampm12.setSelection(1);
            ampm22.setSelection(1);
            ampm32.setSelection(1);
            ampm42.setSelection(1);
            ampm52.setSelection(1);
            ampm62.setSelection(1);
            ampm72.setSelection(1);

            time11.setSelection(6);
            time12.setSelection(11);
            time21.setSelection(6);
            time22.setSelection(11);
            time31.setSelection(6);
            time32.setSelection(11);
            time41.setSelection(6);
            time42.setSelection(11);
            time51.setSelection(6);
            time52.setSelection(11);
            time61.setSelection(6);
            time62.setSelection(11);
            time71.setSelection(6);
            time72.setSelection(11);

        }


        legalPermitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                Intent i = new Intent(Action.ACTION_PICK);
//                startActivityForResult(i, 100);

            //    imagePickerDialoge();

                Intent intent = new Intent(RegistrationActivityThird.this,
                        ImageSelectActivity.class);

                intent.putExtra("token","legal");


                startActivity(intent);

            }
        });

        secondNxtBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(nullCheck()) {


                    hours.add(time11.getSelectedItem().toString() + "|" + ampm11.getSelectedItem().toString() + "-" + time12.getSelectedItem().toString() + "|" + ampm12.getSelectedItem().toString());
                    hours.add(time21.getSelectedItem().toString() + "|" + ampm21.getSelectedItem().toString() + "-" + time22.getSelectedItem().toString() + "|" + ampm22.getSelectedItem().toString());
                    hours.add(time31.getSelectedItem().toString() + "|" + ampm31.getSelectedItem().toString() + "-" + time32.getSelectedItem().toString() + "|" + ampm32.getSelectedItem().toString());
                    hours.add(time41.getSelectedItem().toString() + "|" + ampm41.getSelectedItem().toString() + "-" + time42.getSelectedItem().toString() + "|" + ampm42.getSelectedItem().toString());
                    hours.add(time51.getSelectedItem().toString() + "|" +ampm51.getSelectedItem().toString()+ "-" +time52.getSelectedItem().toString() + "|" +ampm52.getSelectedItem().toString());
                    hours.add(time61.getSelectedItem().toString() + "|" +ampm61.getSelectedItem().toString()+ "-" +time62.getSelectedItem().toString() + "|" +ampm62.getSelectedItem().toString());
                    hours.add(time71.getSelectedItem().toString() + "|" +ampm71.getSelectedItem().toString()+ "-" +time72.getSelectedItem().toString() + "|" +ampm72.getSelectedItem().toString());

                    getTimeList();

                    Intent intent = new Intent(getApplicationContext(), RegistrationActivityFourth.class);

                    legalNameString = legalName.getText().toString();

                    intent.putExtra("firstName", firstNameString);
                    intent.putExtra("lastName", lastNameString);
                    intent.putExtra("email", emailString);
                    intent.putExtra("password", passwordString1);
                    intent.putExtra("personalPicPath", personalImagePath);
                     intent.putExtra("landLine", storeLandLineString);
                    intent.putExtra("commercialName", commercialNameString);
                    intent.putExtra("address", addressString);
                    intent.putExtra("webName", webNameString);
                    intent.putExtra("phone", phoneString);
                    intent.putExtra("storeImage", storeImageString);

                    intent.putExtra("legalName", legalNameString);
                    intent.putExtra("legalImage", legalCertificateImage);
                    intent.putStringArrayListExtra("hours", hours);
                    intent.putIntegerArrayListExtra("hoursInt", timeListInt);
                    intent.putStringArrayListExtra("additionalPic", additaionalImagePath);


                    intent.putExtra("deliveryOp0", deliveryOption0);
                    intent.putExtra("deliveryOp1", deliveryOption1);
                    intent.putExtra("deliveryOp2", deliveryOption2);
                    intent.putExtra("deliveryOp3",deliveryOption3);
                    intent.putExtra("deliveryOp4",deliveryOption4);


                    intent.putExtra("deliveryOp2value",deliveryOption2Value);
                    intent.putExtra("deliveryOp3value", deliveryOption3Value);
                    intent.putExtra("deliveryOp4value", deliveryOption4Value);




                    startActivity(intent);

                    finish();
                }

            }
        });


        secondNxtBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(nullCheck()) {


                    hours.add(time11.getSelectedItem().toString() + "|" + ampm11.getSelectedItem().toString() + "-" + time12.getSelectedItem().toString() + "|" + ampm12.getSelectedItem().toString());
                    hours.add(time21.getSelectedItem().toString() + "|" + ampm21.getSelectedItem().toString() + "-" + time22.getSelectedItem().toString() + "|" + ampm22.getSelectedItem().toString());
                    hours.add(time31.getSelectedItem().toString() + "|" + ampm31.getSelectedItem().toString() + "-" + time32.getSelectedItem().toString() + "|" + ampm32.getSelectedItem().toString());
                    hours.add(time41.getSelectedItem().toString() + "|" + ampm41.getSelectedItem().toString() + "-" + time42.getSelectedItem().toString() + "|" + ampm42.getSelectedItem().toString());
                    hours.add(time51.getSelectedItem().toString() + "|" +ampm51.getSelectedItem().toString()+ "-" +time52.getSelectedItem().toString() + "|" +ampm52.getSelectedItem().toString());
                    hours.add(time61.getSelectedItem().toString() + "|" +ampm61.getSelectedItem().toString()+ "-" +time62.getSelectedItem().toString() + "|" +ampm62.getSelectedItem().toString());
                    hours.add(time71.getSelectedItem().toString() + "|" +ampm71.getSelectedItem().toString()+ "-" +time72.getSelectedItem().toString() + "|" +ampm72.getSelectedItem().toString());

                    getTimeList();

                    Intent intent = new Intent(getApplicationContext(), RegistrationActivityFourth.class);

                    legalNameString = legalName.getText().toString();

                    intent.putExtra("firstName", firstNameString);
                    intent.putExtra("lastName", lastNameString);
                    intent.putExtra("email", emailString);
                    intent.putExtra("password", passwordString1);
                    intent.putExtra("personalPicPath", personalImagePath);
                    intent.putExtra("landLine", storeLandLineString);
                    intent.putExtra("commercialName", commercialNameString);
                    intent.putExtra("address", addressString);
                    intent.putExtra("webName", webNameString);
                    intent.putExtra("phone", phoneString);
                    intent.putExtra("storeImage", storeImageString);
                    intent.putExtra("callOption", callOption);


                    intent.putExtra("legalName", legalNameString);
                    intent.putExtra("legalImage", legalCertificateImage);
                    intent.putStringArrayListExtra("hours", hours);
                    intent.putIntegerArrayListExtra("hoursInt", timeListInt);
                    intent.putStringArrayListExtra("additionalPic", additaionalImagePath);

                    intent.putExtra("deliveryOp0", deliveryOption0);
                    intent.putExtra("deliveryOp1", deliveryOption1);
                    intent.putExtra("deliveryOp2", deliveryOption2);
                    intent.putExtra("deliveryOp3",deliveryOption3);
                    intent.putExtra("deliveryOp4",deliveryOption4);


                    intent.putExtra("deliveryOp2value",deliveryOption2Value);
                    intent.putExtra("deliveryOp3value", deliveryOption3Value);
                    intent.putExtra("deliveryOp4value", deliveryOption4Value);




                    startActivity(intent);

                    finish();
                }


            }
        });
        nextPage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                    if(nullCheck()) {


                hours.add(time11.getSelectedItem().toString() + "|" + ampm11.getSelectedItem().toString() + "-" + time12.getSelectedItem().toString() + "|" + ampm12.getSelectedItem().toString());
                hours.add(time21.getSelectedItem().toString() + "|" + ampm21.getSelectedItem().toString() + "-" + time22.getSelectedItem().toString() + "|" + ampm22.getSelectedItem().toString());
                hours.add(time31.getSelectedItem().toString() + "|" + ampm31.getSelectedItem().toString() + "-" + time32.getSelectedItem().toString() + "|" + ampm32.getSelectedItem().toString());
                hours.add(time41.getSelectedItem().toString() + "|" + ampm41.getSelectedItem().toString() + "-" + time42.getSelectedItem().toString() + "|" + ampm42.getSelectedItem().toString());
                hours.add(time51.getSelectedItem().toString() + "|" +ampm51.getSelectedItem().toString()+ "-" +time52.getSelectedItem().toString() + "|" +ampm52.getSelectedItem().toString());
                hours.add(time61.getSelectedItem().toString() + "|" +ampm61.getSelectedItem().toString()+ "-" +time62.getSelectedItem().toString() + "|" +ampm62.getSelectedItem().toString());
                    hours.add(time71.getSelectedItem().toString() + "|" +ampm71.getSelectedItem().toString()+ "-" +time72.getSelectedItem().toString() + "|" +ampm72.getSelectedItem().toString());

                    getTimeList();

                Intent intent = new Intent(getApplicationContext(), RegistrationActivityFourth.class);

                    legalNameString = legalName.getText().toString();

                    intent.putExtra("firstName", firstNameString);
                    intent.putExtra("lastName", lastNameString);
                    intent.putExtra("email", emailString);
                    intent.putExtra("password", passwordString1);
                    intent.putExtra("personalPicPath", personalImagePath);
                        intent.putExtra("landLine", storeLandLineString);
                    intent.putExtra("commercialName", commercialNameString);
                    intent.putExtra("address", addressString);
                    intent.putExtra("webName", webNameString);
                    intent.putExtra("phone", phoneString);
                    intent.putExtra("storeImage", storeImageString);   intent.putExtra("callOption", callOption);



                        intent.putExtra("legalName", legalNameString);
                    intent.putExtra("legalImage", legalCertificateImage);
                    intent.putStringArrayListExtra("hours", hours);
                    intent.putIntegerArrayListExtra("hoursInt", timeListInt);
                intent.putStringArrayListExtra("additionalPic", additaionalImagePath);

                        intent.putExtra("deliveryOp0", deliveryOption0);
                intent.putExtra("deliveryOp1", deliveryOption1);
                intent.putExtra("deliveryOp2", deliveryOption2);
                intent.putExtra("deliveryOp3",deliveryOption3);
                intent.putExtra("deliveryOp4",deliveryOption4);


                intent.putExtra("deliveryOp2value",deliveryOption2Value);
                intent.putExtra("deliveryOp3value", deliveryOption3Value);
                intent.putExtra("deliveryOp4value", deliveryOption4Value);




                startActivity(intent);

                    finish();
               }

            }
        });


        previousPage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                legalNameString = legalName.getText().toString();



                hours.add(time11.getSelectedItem().toString() + "|" + ampm11.getSelectedItem().toString()+ " - " + time12.getSelectedItem().toString() + "|" + ampm12.getSelectedItem().toString());
                hours.add(time21.getSelectedItem().toString() + "|" + ampm21.getSelectedItem().toString()+ " - " + time22.getSelectedItem().toString() + "|" + ampm22.getSelectedItem().toString());
                hours.add(time31.getSelectedItem().toString() + "|" + ampm31.getSelectedItem().toString()+ " - " + time32.getSelectedItem().toString() + "|" + ampm32.getSelectedItem().toString());
                hours.add(time41.getSelectedItem().toString() + "|" + ampm41.getSelectedItem().toString()+ " - " + time42.getSelectedItem().toString() + "|" + ampm42.getSelectedItem().toString());
                hours.add(time51.getSelectedItem().toString() + "|" + ampm51.getSelectedItem().toString()+ " - " + time52.getSelectedItem().toString() + "|" + ampm52.getSelectedItem().toString());
                hours.add(time61.getSelectedItem().toString() + "|" + ampm61.getSelectedItem().toString()+ " - " + time62.getSelectedItem().toString() + "|" + ampm62.getSelectedItem().toString());
                hours.add(time71.getSelectedItem().toString() + "|" + ampm71.getSelectedItem().toString()+ " - " + time72.getSelectedItem().toString() + "|" + ampm72.getSelectedItem().toString());



                getTimeList();



               // Log.e("hours", hours + "");

                Intent intent = new Intent(getApplicationContext(), RegistrationActivitySecond.class);


                intent.putExtra("firstName", firstNameString);
                intent.putExtra("lastName", lastNameString);
                intent.putExtra("email", emailString);
                intent.putExtra("password", passwordString1);
                intent.putExtra("personalPicPath", personalImagePath);


                intent.putExtra("landLine", storeLandLineString);
                intent.putExtra("commercialName", commercialNameString);
                intent.putExtra("address", addressString);
                intent.putExtra("webName", webNameString);
                intent.putExtra("phone", phoneString);
                intent.putExtra("storeImage", storeImageString);
                intent.putExtra("callOption", callOption);



                intent.putExtra("legalName", legalNameString);
                intent.putExtra("legalImage", legalCertificateImage);
                intent.putStringArrayListExtra("hours", hours);
                intent.putIntegerArrayListExtra("hoursInt", timeListInt);
                intent.putStringArrayListExtra("additionalPic", additaionalImagePath);


                intent.putExtra("deliveryOp0", deliveryOption0);
                intent.putExtra("deliveryOp1", deliveryOption1);
                intent.putExtra("deliveryOp2", deliveryOption2);
                intent.putExtra("deliveryOp3",deliveryOption3);
                intent.putExtra("deliveryOp4",deliveryOption4);


                intent.putExtra("deliveryOp2value",deliveryOption2Value);
                intent.putExtra("deliveryOp3value", deliveryOption3Value);
                intent.putExtra("deliveryOp4value", deliveryOption4Value);
                startActivity(intent);

                finish();

            }
        });



    }

//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        mImageFileSelector.onActivityResult(requestCode, resultCode, data);
//        mImageCropper.onActivityResult(requestCode, resultCode, data);
//        if (requestCode == 100 && resultCode == Activity.RESULT_OK) {
////            adapter.clear();
////
////            viewSwitcher.setDisplayedChild(1);
//              single_path = data.getStringExtra("single_path");
//            //  imageLoader.displayImage("file://" + single_path, imgSinglePick);
//
//
//            legalCertificateImage = single_path;
//
//         //   Log.e("pic", legalCertificateImage);
//            // Uri uri = data.getData();
//
//
//
//
//        } else if (requestCode == 200 && resultCode == Activity.RESULT_OK) {
//            String[] all_path = data.getStringArrayExtra("all_path");
//
//            ArrayList<CustomGallery> dataT = new ArrayList<CustomGallery>();
//
//            for (String string : all_path) {
//                CustomGallery item = new CustomGallery();
//                item.sdcardPath = string;
//
//                dataT.add(item);
//            }
//
////            viewSwitcher.setDisplayedChild(0);
////            adapter.addAll(dataT);
//        }
//    }

    @Override
    protected void onResume() {

        legalCertificateImage = sharePref.getshareprefdatastring(SharePref.LEGAL_IMAGE_PATH);
        super.onResume();
    }
    public static boolean isEmptyString(String text) {
        return (text == null || text.trim().equals("null") || text.trim()
                .length() <= 0);
    }
    public boolean nullCheck() {
        boolean flag = false;





        Log.e("asd",legalCertificateImage.isEmpty()+"");
        Log.e("asd",legalCertificateImage);
       // legalCertificateImage.isEmpty();

        if (!legalName.getText().toString().trim().equalsIgnoreCase("")) {
              if (!isEmptyString(legalCertificateImage))  {
                if(time11.getSelectedItemPosition()!=0 ||time12.getSelectedItemPosition()!=0 ||
                        time21.getSelectedItemPosition()!=0 ||time22.getSelectedItemPosition()!=0 ||
                        time31.getSelectedItemPosition()!=0 ||time32.getSelectedItemPosition()!=0 ||
                        time41.getSelectedItemPosition()!=0 ||time42.getSelectedItemPosition()!=0 ||
                        time51.getSelectedItemPosition()!=0 ||time52.getSelectedItemPosition()!=0 ||
                        time61.getSelectedItemPosition()!=0 ||time62.getSelectedItemPosition()!=0 ||
                        time71.getSelectedItemPosition()!=0 ||time72.getSelectedItemPosition()!=0
                        ) {


                    return true;
                }else{
                    msg = "Please select appropriate time of store!";
                }

            } else {
                msg = "Please select legal certificate image of your store!";
            }

        } else {

            msg = "Please Enter Legal Name Of Your Store!";
        }


        if(!flag) {
            new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                    .setTitleText("Oops...")
                    .setContentText(msg)
                    .show();
        }



        return flag;


    }

//    public void imagePickerDialoge(){
//        final Dialog dialog = new Dialog(RegistrationActivityThird.this);
//        // Include dialog.xml file
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.setContentView(R.layout.choose_image_dialog);
//        // Set dialog title
//        //dialog.setTitle("Category Add");
//
//        dialog.show();
//
//        Button done = (Button) dialog.findViewById(R.id.done);
//        final Button crop = (Button) dialog.findViewById(R.id.crop);
//
//        Button exit = (Button) dialog.findViewById(R.id.cancel);
//        TextView gallery = (TextView) dialog.findViewById(R.id.gallery);
//
//        TextView camera = (TextView) dialog.findViewById(R.id.camera);
//
//        final ImageView imageView = (ImageView) dialog.findViewById(R.id.image);
//
//        // save = (Button) dialog.findViewById(R.id.submit);
//
//
//        crop.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                File file = new File(legalCertificateImage);
//
//                try{
//                mImageCropper.cropImage(file);
//                }catch (Exception e){
//                    Log.d("error",e+"");
//                }
//            }
//        });
//
//        done.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                dialog.dismiss();
//                //    showImage();
//
//
//            }
//        });
//
//        mImageFileSelector = new ImageFileSelector(this);
//        mImageFileSelector.setCallback(new ImageFileSelector.Callback() {
//            @Override
//            public void onSuccess(final String file) {
//                if (!TextUtils.isEmpty(file)) {
//
//
//                    final Bitmap bitmap = BitmapFactory.decodeFile(file);
//                    File imageFile = new File(file);
//
//
//
//                    legalCertificateImage =   imageFile.getPath();
//
//                    imageView.setImageBitmap(bitmap);
//                    imageView.setVisibility(View.VISIBLE);
//                    crop.setVisibility(View.VISIBLE);
//
//
//                } else {
//                    Toast.makeText(getApplicationContext(), "select image file error", Toast.LENGTH_LONG).show();
//                }
//            }
//
//            @Override
//            public void onError() {
//                Toast.makeText(getApplicationContext(), "select image file error", Toast.LENGTH_LONG).show();
//            }
//        });
//
//        mImageCropper = new ImageCropper(this);
//        mImageCropper.setCallback(new ImageCropper.ImageCropperCallback() {
//            @Override
//            public void onCropperCallback(ImageCropper.CropperResult result, File srcFile, File outFile) {
//                legalCertificateImage = "";
//                //  mBtnCrop.setVisibility(View.GONE);
//
//                try {
//                    if (result == ImageCropper.CropperResult.success) {
//                        legalCertificateImage = outFile.getPath();
//
//                        final Bitmap bitmap = BitmapFactory.decodeFile(String.valueOf(outFile));
//                        File imageFile = new File(String.valueOf(outFile));
//
//
//                        legalCertificateImage = imageFile.getPath();
//
//                        imageView.setImageBitmap(bitmap);
//                        crop.setVisibility(View.VISIBLE);
//
//                    } else if (result == ImageCropper.CropperResult.error_illegal_input_file) {
//                        Toast.makeText(getApplicationContext(), "input file error", Toast.LENGTH_LONG).show();
//                    } else if (result == ImageCropper.CropperResult.error_illegal_out_file) {
//                        Toast.makeText(getApplicationContext(), "output file error", Toast.LENGTH_LONG).show();
//                    }
//                } catch (Exception e) {
//                    Log.d("error", e + "");
//                }
//            }
//        });
//
//
//
//
//
//        gallery.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                try {
//                    mImageFileSelector.selectImage(RegistrationActivityThird.this);
//                }catch (Exception e){
//                Log.d("error",e+"");
//            }
//
//            }
//        });
//
//
//        camera.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                try{
//                mImageFileSelector.takePhoto(RegistrationActivityThird.this);
//                }catch (Exception e){
//                    Log.d("error",e+"");
//                }
//
//            }
//        });
//
//        exit.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                dialog.dismiss();
//                legalCertificateImage="";
//            }
//        });
//
//
//
//    }
//
//
//
//
//
//
//    @Override
//    protected void onSaveInstanceState(Bundle outState) {
//        super.onSaveInstanceState(outState);
//       // mImageFileSelector.onSaveInstanceState(outState);
//      //  mImageCropper.onSaveInstanceState(outState);
//    }
//
//    @Override
//    protected void onRestoreInstanceState(Bundle savedInstanceState) {
//        super.onRestoreInstanceState(savedInstanceState);
//      //  mImageFileSelector.onRestoreInstanceState(savedInstanceState);
//      //  mImageCropper.onRestoreInstanceState(savedInstanceState);
//    }
//
//    @SuppressWarnings("NullableProblems")
//    @Override
//    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
//        mImageFileSelector.onRequestPermissionsResult(requestCode, permissions, grantResults);
//    }


    public void getTimeList(){


        timeListInt.clear();

        timeListInt.add(time11.getSelectedItemPosition());
        timeListInt.add(ampm11.getSelectedItemPosition());
        timeListInt.add(time12.getSelectedItemPosition());
        timeListInt.add(ampm12.getSelectedItemPosition());

        timeListInt.add(time21.getSelectedItemPosition());
        timeListInt.add(ampm21.getSelectedItemPosition());
        timeListInt.add(time22.getSelectedItemPosition());
        timeListInt.add(ampm22.getSelectedItemPosition());

        timeListInt.add(time31.getSelectedItemPosition());
        timeListInt.add(ampm31.getSelectedItemPosition());
        timeListInt.add(time32.getSelectedItemPosition());
        timeListInt.add(ampm32.getSelectedItemPosition());

        timeListInt.add(time41.getSelectedItemPosition());
        timeListInt.add(ampm41.getSelectedItemPosition());
        timeListInt.add(time42.getSelectedItemPosition());
        timeListInt.add(ampm42.getSelectedItemPosition());

        timeListInt.add(time51.getSelectedItemPosition());
        timeListInt.add(ampm51.getSelectedItemPosition());
        timeListInt.add(time52.getSelectedItemPosition());
        timeListInt.add(ampm52.getSelectedItemPosition());

        timeListInt.add(time61.getSelectedItemPosition());
        timeListInt.add(ampm61.getSelectedItemPosition());
        timeListInt.add(time62.getSelectedItemPosition());
        timeListInt.add(ampm62.getSelectedItemPosition());

        timeListInt.add(time71.getSelectedItemPosition());
        timeListInt.add(ampm71.getSelectedItemPosition());
        timeListInt.add(time72.getSelectedItemPosition());
        timeListInt.add(ampm72.getSelectedItemPosition());


     //   Log.e("timelistInt",timeListInt.size()+"");
    }

    @Override
    public void onBackPressed() {

        legalNameString = legalName.getText().toString();



        hours.add(time11.getSelectedItem().toString() + " " +ampm11.getSelectedItem().toString()+ "-" +time12.getSelectedItem().toString() + " " +ampm12.getSelectedItem().toString());
        hours.add(time21.getSelectedItem().toString() + " " +ampm21.getSelectedItem().toString()+ "-" +time22.getSelectedItem().toString() + " " +ampm22.getSelectedItem().toString());
        hours.add(time31.getSelectedItem().toString() + " " +ampm31.getSelectedItem().toString()+ "-" +time32.getSelectedItem().toString() + " " +ampm32.getSelectedItem().toString());
        hours.add(time41.getSelectedItem().toString() + " " +ampm41.getSelectedItem().toString()+ "-" +time42.getSelectedItem().toString() + " " +ampm42.getSelectedItem().toString());
        hours.add(time51.getSelectedItem().toString() + " " +ampm51.getSelectedItem().toString()+ "-" +time52.getSelectedItem().toString() + " " +ampm52.getSelectedItem().toString());
        hours.add(time61.getSelectedItem().toString() + " " +ampm61.getSelectedItem().toString()+ "-" +time62.getSelectedItem().toString() + " " +ampm62.getSelectedItem().toString());
        hours.add(time71.getSelectedItem().toString() + " " +ampm71.getSelectedItem().toString()+ "-" +time72.getSelectedItem().toString() + " " +ampm72.getSelectedItem().toString());



        getTimeList();



        // Log.e("hours", hours + "");

        Intent intent = new Intent(getApplicationContext(), RegistrationActivitySecond.class);


        intent.putExtra("firstName", firstNameString);
        intent.putExtra("lastName", lastNameString);
        intent.putExtra("email", emailString);
        intent.putExtra("password", passwordString1);
        intent.putExtra("personalPicPath", personalImagePath);


        intent.putExtra("landLine", storeLandLineString);
        intent.putExtra("commercialName", commercialNameString);
        intent.putExtra("address", addressString);
        intent.putExtra("webName", webNameString);
        intent.putExtra("phone", phoneString);
        intent.putExtra("storeImage", storeImageString);
        intent.putExtra("callOption", callOption);



        intent.putExtra("legalName", legalNameString);
        intent.putExtra("legalImage", legalCertificateImage);
        intent.putStringArrayListExtra("hours", hours);
        intent.putIntegerArrayListExtra("hoursInt", timeListInt);
        intent.putStringArrayListExtra("additionalPic", additaionalImagePath);

        intent.putExtra("deliveryOp0", deliveryOption0);
        intent.putExtra("deliveryOp1", deliveryOption1);
        intent.putExtra("deliveryOp2", deliveryOption2);
        intent.putExtra("deliveryOp3",deliveryOption3);
        intent.putExtra("deliveryOp4",deliveryOption4);


        intent.putExtra("deliveryOp2value",deliveryOption2Value);
        intent.putExtra("deliveryOp3value", deliveryOption3Value);
        intent.putExtra("deliveryOp4value", deliveryOption4Value);
        startActivity(intent);

        finish();


        super.onBackPressed();
    }

//    @Override
//    public void setContentView(View view)
//    {
//        super.setContentView(view);
//
//        FontChangeCrawler fontChanger = new FontChangeCrawler(getAssets(), "fonts/HelveticaNeueLTStd-LtCn.otf");
//        fontChanger.replaceFonts((ViewGroup) this.findViewById(android.R.id.content));
//    }

    public void setTimeList(){




        for(int i=0;i<timeListInt.size();i++){

           //  Log.e("time",timeListInt.get(i)+"");
        }


        time11.setSelection(timeListInt.get(0));
        ampm11.setSelection(timeListInt.get(1));
        time12.setSelection(timeListInt.get(2));
        ampm12.setSelection(timeListInt.get(3));

        time21.setSelection(timeListInt.get(4));
        ampm21.setSelection(timeListInt.get(5));
        time22.setSelection(timeListInt.get(6));
        ampm22.setSelection(timeListInt.get(7));

        time31.setSelection(timeListInt.get(8));
        ampm31.setSelection(timeListInt.get(9));
        time32.setSelection(timeListInt.get(10));
        ampm32.setSelection(timeListInt.get(11));

        time41.setSelection(timeListInt.get(12));
        ampm41.setSelection(timeListInt.get(13));
        time42.setSelection(timeListInt.get(14));
        ampm42.setSelection(timeListInt.get(15));

        time51.setSelection(timeListInt.get(16));
        ampm51.setSelection(timeListInt.get(17));
        time52.setSelection(timeListInt.get(18));
        ampm52.setSelection(timeListInt.get(19));

        time61.setSelection(timeListInt.get(20));
        ampm61.setSelection(timeListInt.get(21));
        time62.setSelection(timeListInt.get(22));
        ampm62.setSelection(timeListInt.get(23));

        time71.setSelection(timeListInt.get(24));
        ampm71.setSelection(timeListInt.get(25));
        time72.setSelection(timeListInt.get(26));
        ampm72.setSelection(timeListInt.get(27));
    }


}
