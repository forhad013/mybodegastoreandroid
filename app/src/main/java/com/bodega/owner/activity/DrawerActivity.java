package com.bodega.owner.activity;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NotificationCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.bodega.owner.R;
import com.bodega.owner.adapter.ConversationItem;
import com.bodega.owner.adapter.NavDrawerListAdapter;
import com.bodega.owner.databasemanager.JSONParser;
import com.bodega.owner.databasemanager.MyDatabase;
import com.bodega.owner.fragment.Fragment_category;
import com.bodega.owner.fragment.Fragment_home;
import com.bodega.owner.fragment.Fragment_profile_test;
import com.bodega.owner.fragment.MessageList;
import com.bodega.owner.model.NewMessageItem;
import com.bodega.owner.utils.ConnectionDetector;
import com.bodega.owner.utils.SharePref;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Stack;

import io.socket.client.Socket;
import io.socket.emitter.Emitter;

public class DrawerActivity extends AppCompatActivity {

    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ImageView mDrawerfooter;
    private ImageButton mDrawerToggle;

    public int newMessageCounter=0;
    ArrayList<ConversationItem> conversationItemArrayList;

    private NavDrawerListAdapter adapter;
    private ArrayList<String> navDrawerItems;

    private ArrayList<Integer> navDrawerItemsImages;
    Fragment mFragment;
    public Stack<Fragment> fragmentStack;
    // nav drawer title
    private CharSequence mDrawerTitle;
    ConnectionDetector cd;
    boolean isInternetPresent;

    String currentFragment;

    MyDatabase db;
   // SimpleFacebook mSimpleFacebook;

    public FragmentManager mFragmentManager;
    SharePref sharePref;
    String type="normal";
    ImageView toolbar_title;
    String loginMethod,userID;
    ImageButton submit;

    public static List<String> categoryname;
    public static List<String> categoryid;

    ImageButton messageBtn;

    TextView textcounter;

    boolean logedIN;

    String token;

    int success;
    HashMap<String,String> roomMap;

    public ArrayList<NewMessageItem> newMessageItemArrayList;

    Socket mSocket;
  public  JSONParser jsonParser = new JSONParser();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drawer);

      //  toolbar_title = (ImageView) findViewById(R.id.toolbar_title);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);


        mDrawerList = (ListView) findViewById(R.id.left_drawer);

       mDrawerToggle = (ImageButton) findViewById(R.id.ham);

        messageBtn = (ImageButton) findViewById(R.id.text);
        navDrawerItems = new ArrayList<>();
        navDrawerItemsImages = new ArrayList<>();

        fragmentStack = new Stack<>();
       // submit = (ImageButton) findViewById(R.id.submit);

        conversationItemArrayList = new ArrayList<>();
        sharePref = new SharePref(this);
        cd = new ConnectionDetector(getApplicationContext());


        userID = sharePref.getshareprefdatastring(SharePref.USERID);

        isInternetPresent = cd.isConnectingToInternet();


       // SocketLogin socketLogin = new SocketLogin(getApplicationContext());



        db = new MyDatabase(getApplicationContext());

        textcounter = (TextView) findViewById(R.id.textCounter);
        logedIN = sharePref.getshareprefdataBoolean(SharePref.LOGEDIN);
        mDrawerList.setDividerHeight(2);

        navDrawerItems.add("Home");
        navDrawerItems.add("Profile");
         navDrawerItems.add("My Inventory");
        navDrawerItems.add("Log Out");


        navDrawerItemsImages.add(R.drawable.home);
        navDrawerItemsImages.add(R.drawable.ic_profile1);
         navDrawerItemsImages.add(R.drawable.ic_inventory);
        navDrawerItemsImages.add(R.drawable.ic_logout);

        if(isInternetPresent){


            connectSocket();
          //  new AsyncTaskLogin().execute();

        }


        currentFragment = "";
      displayView(0);



        adapter = new NavDrawerListAdapter(getApplicationContext(),navDrawerItems,navDrawerItemsImages);

        mDrawerList.setAdapter(adapter);



        mDrawerToggle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mDrawerLayout.isDrawerOpen(mDrawerList)) {
                    mDrawerLayout.closeDrawer(mDrawerList);
                } else {
                    mDrawerLayout.openDrawer(mDrawerList);
                }
            }
        });

        messageBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (logedIN) {
                    displayView(7);
                } else {
                    Intent intent = new Intent(getApplicationContext(), LoginActivity.class);


                    startActivity(intent);

                    finish();
                }


            }
        });


        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {


               displayView(i);

            }
        });




    }


//
//    @Override
//    protected void onNewIntent(Intent intent) {
//
//       // setCounter();
//        super.onNewIntent(intent);
//    }


    @Override
    public void onResume() {
       // setCounter();
        mSocket.connect();
        super.onResume();
       // getApplicationContext() .registerReceiver(mMessageReceiver, new IntentFilter("unique_name"));
    }

    @Override
    protected void onPause() {
        super.onPause();
     //   getApplicationContext().unregisterReceiver(mMessageReceiver);
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            // Extract data included in the Intent
          //  setCounter();

            //do other stuff here
        }
    };
    public void setCounter(){

        textcounter.setText(newMessageCounter+"");
    }

    public void setCounter(int i){

        textcounter.setText(i+"");
    }

    public void connectSocket(){
        ChatApplication app = (ChatApplication) getApplication();
        mSocket = app.getSocket();

        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.accumulate("userid",userID);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.e("object",jsonObject+"");


        mSocket.emit("register_online", jsonObject);//
        mSocket.on("joined", onNewMessage);
        mSocket.on("server_message", handleIncomingMessages);

        mSocket.on(Socket.EVENT_CONNECT_ERROR, onConnectError);
        mSocket.on(Socket.EVENT_CONNECT_TIMEOUT, onConnectError);
        //  roomMap.put("room",conversationID);
        // mSocket.emit("join", roomMap);


        mSocket.connect();

        // checkNewMsg();

    }

    @Override
    protected void onStop() {
//        JSONObject jsonObject = new JSONObject();
//
//        try {
//            jsonObject.accumulate("userid",userID);
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//
//
//
//
//        mSocket.emit("register_offline", jsonObject);

        super.onStop();
    }

    @Override
    protected void onDestroy() {

        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.accumulate("userid",userID);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.e("Stop","Stop");
        mSocket.emit("register_offline", jsonObject);
        super.onDestroy();
    }



    private Emitter.Listener onNewMessage = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.e("test", args[0] + "");
                }
            });
        }
    };

    private Emitter.Listener handleIncomingMessages = new Emitter.Listener(){
        @Override
        public void call(final Object... args){
            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    Log.e("data","run");
                    JSONObject data = (JSONObject) args[0];

                    Log.e("data",data+"");

                    try {
                        Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                        Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
                        r.play();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    try{



                        String message = data.getString("message");
                        String dateNdTime  = data.getString("date");


                        String senderId = data.getString("sender_id");

                        String senderName = data.getString("sender_name");

                        String receiver_id = data.getString("receiver_id");

                        String receiver_name = data.getString("receiver_name");

                        int id = 0;

                        String[] msgTime = dateNdTime.split("\\ ");

                        String date = msgTime[0];
                        String time = msgTime[1];

                        sendNotification(message,senderId,senderName,receiver_id,date,time);


                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    newMessageCounter++;

                    setCounter();




                }
            });
        }
    };
    private Emitter.Listener onConnectError = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    for (int i = 0; i < args.length; i++) {
                        Log.e("soccet", args[i] + "");
                    }
                }
            });
        }
    };

    private void displayView(int position) {
        // update the main content by replacing fragments

        Fragment fragment = null;

        switch (position) {
            case 0:
             //   if(!currentFragment.equals("home")) {
                    fragment = new Fragment_home();


                    currentFragment = "home";
             //   }

                break;
            case 1:

                if(logedIN){
              //      if(!currentFragment.equals("profile")) {
                        fragment = new Fragment_profile_test();
                        currentFragment="profile";
                   // }

                }else{
                    Intent intent = new Intent(getApplicationContext(), LoginActivity.class);


                    startActivity(intent);

                    finish();
                }
                break;

            case 2:
              //  if(!currentFragment.equals("product")) {
                    fragment = new Fragment_category();
                    currentFragment="category";
              //  }

                break;
            case 3:
                sharePref.setshareprefdataBoolean(SharePref.LOGEDIN, false);

                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);


                startActivity(intent);

                finish();

                break;

            case 7:
             //   if(!currentFragment.equals("messagelist")) {
                    fragment = new MessageList();
                    currentFragment="messagelist";
             //   }
                break;


            default:
                break;
        }

        if (fragment != null) {

            mFragmentManager = getSupportFragmentManager();

            FragmentTransaction ft = mFragmentManager.beginTransaction();

            Bundle args = new Bundle();
            args.putString("token", token);
            args.putString("type", type);

            fragment.setArguments(args);

            if (fragmentStack.size() > 0) {
                ft.hide(fragmentStack.lastElement());
            }
            ft.add(R.id.frame_container, fragment);
            fragmentStack.push(fragment);
            ft.commit();
//            if(fragmentStack.size()==4){
//                fragmentStack.remove(0);
//            }
            // fragmentManager.beginTransaction()
            // .replace(R.id.frame_container, fragment).commit();
            // update selected item and title, then close the drawer
            mDrawerList.setItemChecked(position, true);
            mDrawerList.setSelection(position);

            mDrawerLayout.closeDrawer(mDrawerList);
        } else {
            // error in creating fragment
            Log.e("MainActivity", "Error in creating fragment");
        }
    }





    public void checkNewMsg(){

        for(int i=0;i<conversationItemArrayList.size();i++) {
            if(!db.CheckConversation(conversationItemArrayList.get(i).getSenderID())){

                newMessageCounter++;
            }

           setCounter();
        }
    }



    @Override
    public void onBackPressed() {

        Log.i("aa", fragmentStack.size() + "");


        if(mDrawerLayout.isDrawerOpen(mDrawerList)){

            mDrawerLayout.closeDrawer(mDrawerList);
        }else {

            if (fragmentStack.size() > 1) {

                Log.e("si:", fragmentStack.size() + "");
                FragmentTransaction ft = mFragmentManager.beginTransaction();

                fragmentStack.lastElement().onPause();
                ft.remove(fragmentStack.pop());

                fragmentStack.lastElement().onResume();
                ft.show(fragmentStack.lastElement());
//                Log.e("bbb", fragmentStack.lastElement() + "");
                ft.commit();

            } else {


                finish();


            }
        }
    }




    private void sendNotification(String message,String senderID,String senderName,String receiverID,String smsDate,String smsTime) {
        Intent intent = new Intent(this, SoccetIOChat.class);
        intent.putExtra("shopId",senderID);
        intent.putExtra("shopName",senderName);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_gcm,BIND_IMPORTANT)


                .setContentTitle("New Message from "+ senderName)
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }


}

