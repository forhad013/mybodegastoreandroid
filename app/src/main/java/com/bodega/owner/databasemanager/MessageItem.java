package com.bodega.owner.databasemanager;

/**
 * Created by Smartnjazzy on 8/31/2015.
 */
public class MessageItem {

    public String ID;

    public String myID;
    public String fromID;
    public String fromName;
    public String type;

    public String message;

    public String time;
    public String date;

    public int status;



    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }


    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }


    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getFromName() {
        return fromName;
    }

    public void setFromName(String fromName) {
        this.fromName = fromName;
    }


    public String getFromID() {
        return fromID;
    }

    public void setFromID(String fromID) {
        this.fromID = fromID;
    }


    public String getMyID() {
        return myID;
    }

    public void setMyID(String myID) {
        this.myID = myID;
    }

    public String getDate (String date){
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }



    public MessageItem(String ID, String myID, String fromID, String fromName, String message,String type, String date, String time, int status) {
        this.myID = myID;
        this.fromID = fromID;
        this.fromName = fromName;

        this.message = message;

        this.time = time;
        this.type = type;

        this.date = date;

        this.status = status;

        this.ID = ID;


    }



}
