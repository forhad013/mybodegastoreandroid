package com.bodega.owner.databasemanager;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;


import com.bodega.owner.adapter.NewMessageListItem;
import com.bodega.owner.utils.SharePref;
import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Mac on 11/12/15.
 */
public class MyDatabase extends SQLiteAssetHelper {

    private static final String DATABASE_NAME = "bodega.sqlite";
    private static final int DATABASE_VERSION = 1;

    SharePref sharePref;

    SimpleDateFormat showDateFormate, fromCalender;

    Calendar calendar;

    MessageItem messageItem;

    MessageListItem messageListItem;
    String myID;

    ArrayList<MessageListItem> messageListItemArrayList;

    ArrayList<MessageItem> messageItemArrayList;
    SimpleDateFormat formatter, timeFormat;


    public MyDatabase(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);

        formatter = new SimpleDateFormat("yyyy-MM-dd");
        timeFormat = new SimpleDateFormat("hh:mm a");

        sharePref = new SharePref(context);

        messageItemArrayList = new ArrayList<>();

        messageListItemArrayList = new ArrayList<>();

        myID = sharePref.getshareprefdatastring(SharePref.USERID);


    }



    public void delete(String id) {
        SQLiteDatabase db = this.getReadableDatabase();

        String ID = id;
        //	 String retrieve = "INSERT INTO myorder(restaurant,food_item,item_number,status) values (reset,'Rangamati,Chittagong','Kaptai Lake is a man made lake in south-eastern Bangladesh. It is located in the Kaptai Upazila under Rangamati District of Chittagong Division. The lake was created as a result of building the Kaptai Dam on the Karnaphuli River, as part of the Karnaphuli Hydro-electric project. The Kaptai Lake average depth is 100 feet  and maximum depth is 490 feet.','22.5009397','92.2136081');" ;
        // Cursor cursor = db.rawQuery(retrieve, null);

        db.delete("accounts", "ID" + "=" + ID, null);

        //db.update("STATUS", cv, ID + "=?", new String[] { "1" });
        db.close();
    }

    public void saveMessage(String fromID, String shopID, String shopName, String type, String message, String status) {
        SQLiteDatabase db = this.getReadableDatabase();

        calendar = Calendar.getInstance();
        Date date = calendar.getTime();

        String dateForDb = formatter.format(date);

        String timeForDb = timeFormat.format(date);


        ContentValues cv = new ContentValues();
        cv.put("myID", fromID);
        cv.put("shopID", shopID);
        cv.put("shopName", shopName);

        cv.put("type", type);
        cv.put("message", message);

        cv.put("date", dateForDb);
        cv.put("time", timeForDb);
        cv.put("status", status);


        String ID = "ID";
        //	 String retrieve = "INSERT INTO myorder(restaurant,food_item,item_number,status) values (reset,'Rangamati,Chittagong','Kaptai Lake is a man made lake in south-eastern Bangladesh. It is located in the Kaptai Upazila under Rangamati District of Chittagong Division. The lake was created as a result of building the Kaptai Dam on the Karnaphuli River, as part of the Karnaphuli Hydro-electric project. The Kaptai Lake average depth is 100 feet  and maximum depth is 490 feet.','22.5009397','92.2136081');" ;
        // Cursor cursor = db.rawQuery(retrieve, null);

        db.insert("inbox", null, cv);

        //db.update("STATUS", cv, ID + "=?", new String[] { "1" });
        db.close();

    }


    public void clear() {

        SQLiteDatabase db = this.getReadableDatabase();


        db.execSQL("delete from  accounts");

        db.close();
    }



    public void clearImages() {

        SQLiteDatabase db = this.getReadableDatabase();


        db.execSQL("delete from  images");

        db.close();
    }

    public ArrayList getList(String token) {
        Cursor c;
        String searchDate = "";



        SQLiteDatabase db = this.getReadableDatabase();
        String retrieve = "select * from inbox where shopID= "+token+" and myID='" +myID+"'";

        Log.e("check", retrieve);


        c = db.rawQuery(retrieve, null);
        if (c.getCount() != 0) {

            if (c != null) {
                if (c.moveToFirst()) {
                    do {


                        String ID = c.getString(c.getColumnIndex("ID"));
                        String myID = c.getString(c.getColumnIndex("myID"));
                        String shopID = c.getString(c.getColumnIndex("shopID"));
                        String shopName = c.getString(c.getColumnIndex("shopName"));
                        String type = c.getString(c.getColumnIndex("type"));
                        String message = c.getString(c.getColumnIndex("message"));

                        String date = c.getString(c.getColumnIndex("date"));

                        String time = c.getString(c.getColumnIndex("time"));

                        String status = c.getString(c.getColumnIndex("status"));

                        messageItem = new MessageItem(ID, myID, shopID, shopName, type, message, date, time, Integer.parseInt(status));


                        messageItemArrayList.add(messageItem);

                    } while (c.moveToNext());

                }

            }

        }

        db.close();


        return messageItemArrayList;

    }


    public void saveMessageIncoming(String forID, String shopID, String shopName, String type, String message, String status,String smsDate, String smsTime) {
        SQLiteDatabase db = this.getReadableDatabase();
        calendar = Calendar.getInstance();

       Date date = calendar.getTime();

        String dateForDb = formatter.format(date);

        String timeForDb = timeFormat.format(date);


        ContentValues cv = new ContentValues();
        cv.put("myID", forID);
        cv.put("shopID", shopID);
        cv.put("shopName", shopName);

        cv.put("type", type);
        cv.put("message", message);

        cv.put("date", dateForDb);
        cv.put("time", timeForDb);
        cv.put("status", status);


        String ID = "ID";
        //	 String retrieve = "INSERT INTO myorder(restaurant,food_item,item_number,status) values (reset,'Rangamati,Chittagong','Kaptai Lake is a man made lake in south-eastern Bangladesh. It is located in the Kaptai Upazila under Rangamati District of Chittagong Division. The lake was created as a result of building the Kaptai Dam on the Karnaphuli River, as part of the Karnaphuli Hydro-electric project. The Kaptai Lake average depth is 100 feet  and maximum depth is 490 feet.','22.5009397','92.2136081');" ;
        // Cursor cursor = db.rawQuery(retrieve, null);

        db.insert("inbox", null, cv);

        //db.update("STATUS", cv, ID + "=?", new String[] { "1" });
        db.close();

    }

    public void saveImages(String userID, String type, String link) {
        SQLiteDatabase db = this.getReadableDatabase();


        //   Date date = calendar.getTime();

        //String dateForDb = formatter.format(date);

        //String timeForDb = timeFormat.format(date);


        ContentValues cv = new ContentValues();
        cv.put("userID", userID);
        cv.put("link", link);
        cv.put("additional",type);



        String ID = "ID";
        //	 String retrieve = "INSERT INTO myorder(restaurant,food_item,item_number,status) values (reset,'Rangamati,Chittagong','Kaptai Lake is a man made lake in south-eastern Bangladesh. It is located in the Kaptai Upazila under Rangamati District of Chittagong Division. The lake was created as a result of building the Kaptai Dam on the Karnaphuli River, as part of the Karnaphuli Hydro-electric project. The Kaptai Lake average depth is 100 feet  and maximum depth is 490 feet.','22.5009397','92.2136081');" ;
        // Cursor cursor = db.rawQuery(retrieve, null);

        db.insert("images", null, cv);

        //db.update("STATUS", cv, ID + "=?", new String[] { "1" });
        db.close();

    }


    public ArrayList getImagesList(String type,String userID) {
        Cursor c;
        String searchDate = "";


        ArrayList linkLIst = new ArrayList();

        SQLiteDatabase db = this.getReadableDatabase();
        String retrieve = "select * from images where userID= "+userID+" and additional='" +type+"'";

        Log.e("check", retrieve);


        c = db.rawQuery(retrieve, null);
        if (c.getCount() != 0) {

            if (c != null) {
                if (c.moveToFirst()) {
                    do {





                        String link = c.getString(c.getColumnIndex("link"));



                        linkLIst.add(link);


                    } while (c.moveToNext());

                }

            }

        }

        db.close();


        return linkLIst;

    }


    public boolean CheckConversation(String userID) {
        Cursor c;
        String searchDate = "";


        boolean status = false;

        SQLiteDatabase db = this.getReadableDatabase();
        String retrieve = "select * from conversation where userID = "+userID;




        c = db.rawQuery(retrieve, null);
        if (c.getCount() != 0) {


            status = true;

        }

        db.close();


        return status;

    }


    public boolean getReadStatus(String userID) {
        Cursor c;
        String searchDate = "";


        boolean status = false;

        SQLiteDatabase db = this.getReadableDatabase();
        String retrieve = "select * from conversation where userID = "+userID;




        c = db.rawQuery(retrieve, null);
        if (c.getCount() != 0) {

            String read = c.getString(c.getColumnIndex("status"));

            if(read.equals("1")) {
                status = true;
            }

        }

        db.close();


        return status;

    }

    public String getConversation(String userID) {
        Cursor c;
        String searchDate = "";


        String conID = "";

        SQLiteDatabase db = this.getReadableDatabase();
        String retrieve = "select * from conversation where userID= "+userID;


        c = db.rawQuery(retrieve, null);

        Log.e("c",c.getCount()+"");

        if (c.getCount() != 0) {

            if (c != null) {

                if (c.moveToFirst()) {
                    do {


                        conID = c.getString(c.getColumnIndex("conID"));


                    } while (c.moveToNext());


                }
            }
        }

        db.close();


        return conID;

    }



    public void saveConversationID(String userID,String conversationID,String userName){


        SQLiteDatabase db = this.getReadableDatabase();


        ContentValues cv = new ContentValues();
        cv.put("userID", userID);
        cv.put("conID", conversationID);
        cv.put("userName", userName);
        cv.put("status", "1");

        String ID = "ID";



        db.insert("conversation", null, cv);

        //db.update("STATUS", cv, ID + "=?", new String[] { "1" });
        db.close();


    }


    public ArrayList getMessageList() {
        Cursor c;
        String searchDate = "";


        ArrayList checkList = new ArrayList();

        SQLiteDatabase db = this.getReadableDatabase();
        String retrieve = "select * from conversation";

        Log.e("check", retrieve);

        ArrayList<NewMessageListItem> newMessageListItems = new ArrayList<>();

        c = db.rawQuery(retrieve, null);
        if (c.getCount() != 0) {

            if (c != null) {
                if (c.moveToFirst()) {
                    do {


                        String userID = c.getString(c.getColumnIndex("userID"));

                        String conID = c.getString(c.getColumnIndex("conID"));
                        String userName = c.getString(c.getColumnIndex("userName"));


                        NewMessageListItem newMessageListItem = new NewMessageListItem(userID, userName,conID);

                        newMessageListItems.add(newMessageListItem);


                    } while (c.moveToNext());

                }

            }

        }

        db.close();


        return newMessageListItems;

    }

//    public ArrayList getMessageList() {
//        Cursor c;
//        String searchDate = "";
//
//
//        ArrayList checkList = new ArrayList();
//
//        SQLiteDatabase db = this.getReadableDatabase();
//        String retrieve = "select * from inbox where myID='" +myID+"'";
//
//        Log.e("check", retrieve);
//
//
//        c = db.rawQuery(retrieve, null);
//        if (c.getCount() != 0) {
//
//            if (c != null) {
//                if (c.moveToFirst()) {
//                    do {
//
//
//                        String ID = c.getString(c.getColumnIndex("ID"));
//
//                        String shopID = c.getString(c.getColumnIndex("shopID"));
//                        String shopName = c.getString(c.getColumnIndex("shopName"));
//
//
//
//                        if(!checkList.contains(shopID)){
//                            messageListItem = new MessageListItem(ID, shopID, shopName);
//
//                            messageListItemArrayList.add(messageListItem);
//
//                            checkList.add(shopID);
//                        }
//
//
//                    } while (c.moveToNext());
//
//                }
//
//            }
//
//        }
//
//        db.close();
//
//
//        return messageListItemArrayList;
//
//    }

    public void updateUnRead(String userID) {
        SQLiteDatabase db = this.getReadableDatabase();

        String ID = userID;

        // db.update("cart","ID" + "=" + ID, null);
        ContentValues cv = new ContentValues();

        cv.put("status", "0");



        Log.e("update","updated");


        db.update("conversation", cv, "userID  =?", new String[]{userID});

        db.close();
    }


    public int checkUnread() {

        int count=0;
        Cursor c;
        String searchDate = "";


        ArrayList storeIdList = new ArrayList();

        SQLiteDatabase db = this.getReadableDatabase();
        String retrieve = "select * from conversation where status= 0";

        Log.e("check", retrieve);


        c = db.rawQuery(retrieve, null);
        if (c.getCount() != 0) {

//            if (c != null) {
//                if (c.moveToFirst()) {
//                    do {
//
//
//
//
//                    } while (c.moveToNext());
//
//                }
//
//            }

        }

        db.close();

        count = c.getCount();

        return count;

    }



}