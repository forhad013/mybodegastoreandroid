package com.bodega.owner.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Smartnjazzy on 8/30/2015.
 */
public class SharePref {

    public static final String PREF = "shopOwnerPref";
    static public String GCM_REGCODE="gcm_regCode";

    static public String USERID="userID";
    static public String USERFIRSTNAME="userFirstName";
    static public String USERLASTNAME="userLastName";

    static public String STORELEGALNAME="legalName";
    static public String PROFILEPIC="profilePic";
    static public String OFFICEPIC="officePic";
    static public String LEGALPIC="legalPic";
    static public String CALL_OPTION="callOption";

    static public String USEREMAIL="userEmail";
    static public String USERADDRESS="userAddress";
    static public String LATITUDE="latitude";
    static public String LONGITUDE="longitude";
    static public String TAXAMOUNT="taxAmount";
    static public String SOCKET_TOKEN="socket_token";

    static public String CURRENT_FRAGMENT="fragemnt";
    static public String SHOPSTREETNAME="streetName";
    static public String USERPASSWWORD="userPassword";
    static public String USERPHONE="userNumber";
    static public String USERLANDPHONE="userLandPhone";
    static public String LOGEDIN="logedIn";
    static public String WEBNAME="webName";
    static public String STORENAME="storeName";

    static public String SUNDAY="sunday";
    static public String MONDAY="monday";
    static public String TUESDAY="tuesday";
    static public String WEDNESSDAY="wednesday";
    static public String THUSDAY="thursday";
    static public String FRIDAY="friday";
    static public String SATURDAY="saturday";

    static public String D_OPTION0="d0";
    static public String D_OPTION1="d1";
    static public String D_OPTION2="d2";
    static public String D_OPTION3="d3";
    static public String D_OPTION4="d4";

    static public String D_OPTION2_VALUE="d2Value";
    static public String D_OPTION3_VALUE="d3Value";
    static public String D_OPTION4_VALUE="d4Value";

    static public String PERSONAL_IMAGE_PATH="personalPath";
    static public String STORE_IMAGE_PATH="storePath";
    static public String LEGAL_IMAGE_PATH="legalPath";


    SharedPreferences settings;

    public SharePref(Context mContext){
        settings = mContext.getSharedPreferences(PREF,Context.MODE_PRIVATE);
    }
    public int getshareprefdata(String KEY){
        return settings.getInt(KEY, 160);
    }

    public void setshareprefdata(String KEY, int value){

        settings.edit().putInt(KEY,value).commit();
    }


    public String getshareprefdatastring(String KEY){
        return settings.getString(KEY, "");
    }

    public void setshareprefdatastring(String KEY, String values){

        settings.edit().putString(KEY, values).commit();
    }
    public Boolean getshareprefdataBoolean(String KEY){
        return settings.getBoolean(KEY, false);

    }

    public void setshareprefdataBoolean(String KEY, Boolean values){

        settings.edit().putBoolean(KEY, values).commit();
    }
}
