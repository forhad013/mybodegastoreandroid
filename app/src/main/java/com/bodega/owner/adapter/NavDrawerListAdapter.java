package com.bodega.owner.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import com.bodega.owner.R;

import java.util.ArrayList;

public class NavDrawerListAdapter extends BaseAdapter {
	
	private Context context;
	private ArrayList<String> navDrawerItems;

	private ArrayList<Integer> navDrawerItemsImages;

	Typeface typeface;
	
	public NavDrawerListAdapter(Context context, ArrayList<String> navDrawerItems, ArrayList<Integer> navDrawerItemsImages){
		this.context = context;
		this.navDrawerItems = navDrawerItems;
		this.navDrawerItemsImages = navDrawerItemsImages;
		typeface = Typeface.createFromAsset(context.getAssets(), "fonts/HelveticaNeueLTStd-Md.otf");

	}

	@Override
	public int getCount() {
		return navDrawerItems.size();
	}

	@Override
	public Object getItem(int position) {
		return navDrawerItems.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {





		if (convertView == null) {
			convertView =  LayoutInflater.from(context).inflate(
					R.layout.drawer_item, parent, false);
		}


            
            ImageView imgIcon = (ImageView) convertView.findViewById(R.id.icon);
            TextView txtTitle = (TextView) convertView.findViewById(R.id.title);

		txtTitle.setTypeface(typeface);

             
           // imgIcon.setImageResource(navDrawerItemsImages.get(position));
            txtTitle.setText(navDrawerItems.get(position));
		imgIcon.setImageResource(navDrawerItemsImages.get(position));

	//	if(position==0 ||position==6 ||position==7 ||position==8 ||position==0 ||position==10 || position==11 ||position==12 ||position==13 ||position==14 ){
			txtTitle.setTypeface(null, Typeface.BOLD);
	//	}
            


    
        
        return convertView;
	}
	
	

}
