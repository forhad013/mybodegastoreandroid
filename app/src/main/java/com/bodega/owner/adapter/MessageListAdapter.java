package com.bodega.owner.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.bodega.owner.R;
import com.bodega.owner.utils.FontChangeCrawler;
import com.bodega.owner.utils.SharePref;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;


public class MessageListAdapter extends BaseAdapter {


    ArrayList<NewMessageListItem> messageListItems;
    String videoLink;

    private Context context;

    TextView title1, details;
    int success;
    String CID,vote;
    SharePref sharePref;
    String currentUserID;
    int voteNumber;
    TextView vote1;


    FontChangeCrawler fontChanger;
    boolean isInternetPresent;

    public MessageListAdapter(Context context,
                              ArrayList<NewMessageListItem> messageListItems) {


        // TODO Auto-generated constructor stub
        this.context = context;

        this.messageListItems = messageListItems;

        sharePref = new SharePref(context);

       // currentUserID = sharePref.getshareprefdatastring(SharePref.USERID);




    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return messageListItems.size();
    }

    @Override
    public Object getItem(int arg0) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public long getItemId(int arg0) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        View view = convertView;




            view = LayoutInflater.from(context).inflate(
                    R.layout.message_list_item, parent, false);


          fontChanger = new FontChangeCrawler(context .getAssets(), "fonts/HelveticaNeueLTStd-LtCn.otf");


    //    fontChanger.replaceFonts((ViewGroup)view);

        TextView userName1 = (TextView) view.findViewById(R.id.userName);



        String userName = messageListItems.get(position).getUserName();



        userName1.setText(userName);







        return view;
    }



    public static String getFormattedDateFromTimestamp(long timestampInMilliSeconds)
    {
        Date date = new Date();
        date.setTime(timestampInMilliSeconds);
        String formattedDate=new SimpleDateFormat("MMM d, yyyy hh:mm:ss").format(date);
        return formattedDate;

    }



}