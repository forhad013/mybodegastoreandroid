package com.bodega.owner.adapter;

/**
 * Created by Smartnjazzy on 8/31/2015.
 */
public class MessageListItem {

    public String ID;
    public String userID;
    public String userName;







    public String getID() {
        return  ID;
    }

    public void setID(String ID) {
        this. ID =  ID;
    }



    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }



    public MessageListItem(String ID, String userID, String userName) {
        this.userID = userID;
        this.userName = userName;
        this.ID = ID;


    }



}
