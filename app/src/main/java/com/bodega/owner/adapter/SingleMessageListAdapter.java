package com.bodega.owner.adapter;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.bodega.owner.R;
import com.bodega.owner.databasemanager.MessageItem;
import com.bodega.owner.utils.FontChangeCrawler;

import java.util.ArrayList;


public class SingleMessageListAdapter extends BaseAdapter {



	FontChangeCrawler fontChanger ;

	int tag;
    String token;
	ArrayList<MessageItem> messageItemArrayList;

	private Context context;

	public SingleMessageListAdapter(Context context,
									ArrayList<MessageItem> listItemModelsArrayList) {

		// TODO Auto-generated constructor stub
		this.context = context;
		this.messageItemArrayList = listItemModelsArrayList;






	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return messageItemArrayList.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View view = convertView;



			view = LayoutInflater.from(context).inflate(
					R.layout.message_item, parent, false);


		TextView messageTextOther = (TextView) view.findViewById(R.id.messageText);
		TextView messageTextMe = (TextView) view.findViewById(R.id.messageTextMe);
		TextView timeOther = (TextView) view.findViewById(R.id.time);
		TextView timetMe = (TextView) view.findViewById(R.id.timeMe);
		 fontChanger = new FontChangeCrawler(context .getAssets(), "fonts/HelveticaNeueLTStd-LtCn.otf");


	//	fontChanger.replaceFonts((ViewGroup)view);

//		CircleImageView person2 = (CircleImageView) view.findViewById(R.id.secondPerson);
//		CircleImageView person1 = (CircleImageView) view.findViewById(R.id.firstPerson);





		String toID = messageItemArrayList.get(position).getMyID();

		String fromID = messageItemArrayList.get(position).getFromID();

		String message = messageItemArrayList.get(position).getMessage();


		String time = messageItemArrayList.get(position).getTime();


		String type = messageItemArrayList.get(position).getType();

		if(type.equals("outgoing")){
			timetMe.setText(time);
			messageTextMe.setText(Html.fromHtml(message));
			messageTextOther.setVisibility(View.GONE);

		}else{
			timeOther.setText(time);
			messageTextOther.setText(Html.fromHtml(message));
			messageTextMe.setVisibility(View.GONE);
		}



		return view;
	}



}