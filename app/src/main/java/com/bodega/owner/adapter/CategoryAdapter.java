package com.bodega.owner.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import com.bodega.owner.R;
import com.bodega.owner.activity.AddCategory;
import com.bodega.owner.activity.DrawerActivity;
import com.bodega.owner.fragment.Fragment_product;
import com.bodega.owner.model.CateogryModel;
import com.bodega.owner.utils.FontChangeCrawler;

import java.util.ArrayList;


public class CategoryAdapter extends BaseAdapter {




	int tag;
    String token;
	ArrayList<CateogryModel> cateogryModelArrayList;

	private Context context;

	FontChangeCrawler fontChanger;

	public CategoryAdapter(Context context,
						   ArrayList<CateogryModel> cateogryModelArrayList) {

		// TODO Auto-generated constructor stub
		this.context = context;
		this.cateogryModelArrayList = cateogryModelArrayList;



	//	fontChanger = new FontChangeCrawler(context.getAssets(), "fonts/HelveticaNeueLTStd-LtCn.otf");



	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return cateogryModelArrayList.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(final int position, final View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View view = convertView;


	//	if (view == null) {

			view = LayoutInflater.from(context).inflate(
					R.layout.category_item, parent, false);
	//	}

		TextView nameTxt = (TextView) view.findViewById(R.id.categoryName);
		ImageButton edit = (ImageButton) view.findViewById(R.id.edit);
		ImageButton forward = (ImageButton) view.findViewById(R.id.forward);
	 		nameTxt.setText(cateogryModelArrayList.get(position).catName);


		edit.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(context,AddCategory.class);

				i.putExtra("token","edit");
				i.putExtra("ID",cateogryModelArrayList.get(position).getCatID());
				i.putExtra("name",cateogryModelArrayList.get(position).getCatName());
				i.putExtra("status",cateogryModelArrayList.get(position).getStatus());
				i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				context.startActivity(i);
			}
		});


		forward.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {

				Fragment fragment = new Fragment_product();

				((DrawerActivity)context).mFragmentManager = ((DrawerActivity)context).getSupportFragmentManager();

				FragmentTransaction ft = ((DrawerActivity)context).mFragmentManager.beginTransaction();

				Bundle args = new Bundle();
				args.putString("catID", cateogryModelArrayList.get(position).getCatID());
				args.putString("catTitle", cateogryModelArrayList.get(position).getCatName());

				fragment.setArguments(args);


				ft.add(R.id.frame_container, fragment);
				((DrawerActivity)context).fragmentStack.push(fragment);
				ft.commit();
			}
		});


		return view;
	}



}