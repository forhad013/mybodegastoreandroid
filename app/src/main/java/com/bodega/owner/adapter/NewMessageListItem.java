package com.bodega.owner.adapter;

/**
 * Created by Smartnjazzy on 8/31/2015.
 */
public class NewMessageListItem {

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String userID;

    public NewMessageListItem(String userID, String userName, String date) {
        this.userID = userID;
        this.userName = userName;
        this.date = date;
    }

    public String userName;
    public String date;








}
