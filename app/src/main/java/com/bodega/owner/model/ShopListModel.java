package com.bodega.owner.model;

/**
 * Created by Smartnjazzy on 8/31/2015.
 */
public class ShopListModel {

    public String shopID;
    public String shopName;
    public String shopLocation;
    public double shopLatitude;
    public double shopLongitude;
    public String shopContact;






    public String getShopID() {
        return shopID;
    }

    public void setShopID(String shopID) {
        this.shopID = shopID;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getShopLocation() {
        return shopLocation;
    }

    public void setShopLocation(String shopLocation) {
        this.shopLocation = shopLocation;
    }

    public double getShopLatitude() {
        return shopLatitude;
    }

    public void setShopLatitude(double shopLatitude) {
        this.shopLatitude = shopLatitude;
    }

    public double getShopLongitude() {
        return shopLongitude;
    }

    public void setShopLongitude(double shopLongitude) {
        this.shopLongitude = shopLongitude;
    }

    public String getShopContact() {
        return shopContact;
    }


    public void setShopContact(String shopContact) {
        this.shopContact = shopContact;
    }






    public ShopListModel(String shopID, String shopName, String shopContact, String shopLocation, double shopLatitude, double shopLongitude ) {
        this.shopID = shopID;
        this.shopName = shopName;
        this.shopContact = shopContact;
        this.shopLocation = shopLocation;
        this.shopLatitude = shopLatitude;
        this.shopLongitude = shopLongitude;


    }



}
