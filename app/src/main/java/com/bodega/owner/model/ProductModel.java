package com.bodega.owner.model;

/**
 * Created by HP on 02/08/2015.
 */
public final class ProductModel {


  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public String getPrice() {
    return price;
  }

  public void setPrice(String price) {
    this.price = price;
  }

  public String getCategoryId() {
    return categoryId;
  }

  public void setCategoryId(String categoryId) {
    this.categoryId = categoryId;
  }

  public String getStoreID() {
    return storeID;
  }

  public void setStoreID(String storeID) {
    this.storeID = storeID;
  }

  public  String id;

  public ProductModel(String id, String title, String status, String price, String categoryId, String storeID,String categoryName) {
    this.id = id;
    this.title = title;
    this.status = status;
    this.price = price;
    this.categoryId = categoryId;
    this.categoryName = categoryName;
    this.storeID = storeID;
  }

  public String title;
  public String status;
  public  String price;
  public String categoryId;

  public String getCategoryName() {
    return categoryName;
  }

  public void setCategoryName(String categoryName) {
    this.categoryName = categoryName;
  }

  public String categoryName;
  public String storeID;


}




