package com.bodega.owner.model;

/**
 * Created by Smartnjazzy on 8/31/2015.
 */
public class NewMessageItem {

    public String ID;

    public String senderID;

    public NewMessageItem(String ID, String senderID, String senderName, String message, String time, String date, int status, String receiverID, String receiverName) {
        this.ID = ID;
        this.senderID = senderID;
        this.senderName = senderName;
        this.message = message;
        this.time = time;
        this.date = date;
        this.status = status;
        this.receiverID = receiverID;
        this.receiverName = receiverName;
    }

    public String senderName;
    public String message;

    public String time;
    public String date;

    public int status;

    public String receiverID;
    public String receiverName;


    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getSenderID() {
        return senderID;
    }

    public void setSenderID(String senderID) {
        this.senderID = senderID;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getReceiverID() {
        return receiverID;
    }

    public void setReceiverID(String receiverID) {
        this.receiverID = receiverID;
    }

    public String getReceiverName() {
        return receiverName;
    }

    public void setReceiverName(String receiverName) {
        this.receiverName = receiverName;
    }



}
