/*
 *  Copyright (c) 2011 by Twilio, Inc., all rights reserved.
 *
 *  Use of this software is subject to the terms and conditions of 
 *  the Twilio Terms of Service located at http://www.twilio.com/legal/tos
 */
package com.bodega.owner.twillio;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.twilio.client.Connection;
import com.twilio.client.Device;
import com.twilio.client.Twilio;
import com.twilio.client.impl.net.EventStream;

import java.util.HashMap;
import java.util.Map;

public class MonkeyPhone implements Twilio.InitListener,EventStream.Listener
{
    private static final String TAG = "MonkeyPhone";

    Connection connection;
    private Device device;

    public MonkeyPhone(Context context)
    {
        Twilio.initialize(context, this /* Twilio.InitListener */);
    }

    /* Twilio.InitListener method */
    @Override
    public void onInitialized()
    {


        new RetrieveCapabilityToken().execute("https://lit-bayou-97655.herokuapp.com/token");
    }

    @Override
    public void onConnected() {

    }

    @Override
    public void onDisconnected() {

      // TwillioCall twillioCall = new TwillioCall();
        //twillioCall.endCall();
    }

    @Override
    public void onDisconnected(Exception e, boolean b) {

    }

    @Override
    public boolean onMessageReceived(Map<String, Object> map) {
        return false;
    }

    @Override
    public void onFeaturesUpdated() {

    }

    private class RetrieveCapabilityToken extends AsyncTask<String, Void, String> {

		@Override
		protected String doInBackground(String... params) {
			try{ 
				String capabilityToken = com.bodega.customer.twillio.HttpHelper.httpGet(params[0]);
               // Log.e("asd", capabilityToken);
				return capabilityToken; 
			} catch( Exception e ){
				 Log.e(TAG, "Failed to obtain capability token: " + e.getLocalizedMessage());
				 return null;
			}
			
		}
    	
		@Override
		protected void onPostExecute(String capabilityToken ){
			MonkeyPhone.this.setCapabilityToken(capabilityToken);
		}
    }

    protected void setCapabilityToken(String capabilityToken){
    	 device = Twilio.createDevice(capabilityToken, null /* DeviceListener */);
    }
    
    /* Twilio.InitListener method */
    @Override
    public void onError(Exception e)
    {
        Log.e(TAG, "Twilio SDK couldn't start: " + e.getLocalizedMessage());
    }

    @Override
    protected void finalize()
    {
        if (device != null)
            device.release();
    }


    public void connect(String phoneNumber)
    {
       // new RetrieveCapabilityToken().execute("https://lit-bayou-97655.herokuapp.com/token");

        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put("To", phoneNumber);
        connection = device.connect(parameters, null /* ConnectionListener */);
        if (connection == null)
            Log.w(TAG, "Failed to create new connection");
    }

    public void disconnect()
    {
        if (connection != null) {
            connection.disconnect();
            connection = null;
        }


    }

    public void destory(){
        Twilio.shutdown();
    }


}