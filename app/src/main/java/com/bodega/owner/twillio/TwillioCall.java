package com.bodega.owner.twillio;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bodega.owner.R;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.util.Timer;
import java.util.TimerTask;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class TwillioCall extends Activity implements View.OnClickListener
{
    protected boolean _active = true;
    protected int _splashTime = 300000;
    private MonkeyPhone phone;
    private EditText numberField;
   public TextView title;
    String ownerName,ownerPic,contact;
   // CircleImageView image;
  public  ImageButton dialButton,hangupButton;
    LinearLayout lin1,lin2;

    Thread thread;

    ImageButton backBtn;

    SweetAlertDialog progressSweetAlertDialog;
    @Override
    public void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        setContentView(R.layout.activity_twillio_call);

        phone = new MonkeyPhone(getApplicationContext());
       // image  = (CircleImageView)findViewById(R.id.image);
        title =  (TextView)findViewById(R.id.title);

        backBtn = (ImageButton) findViewById(R.id.backBtn);
        ownerName = getIntent().getStringExtra("ownerName");
        ownerPic = getIntent().getStringExtra("image");
        contact = getIntent().getStringExtra("storeContact");



        contact = contact.replace("(","");
        contact = contact.replace(")","");
        contact = contact.replace("-","");
        contact = contact.replace("+","");
        contact = contact.replaceAll(" ","");
        //contact = "+"+contact;
       // contact = "9177839161";
        lin2 = (LinearLayout) findViewById(R.id.lin2);

        Log.e("contact",contact);

          hangupButton = (ImageButton)findViewById(R.id.hangupButton);
        lin2.setOnClickListener(this);
        //setData();

        //String s ="Are you sure to call "  + System.getProperty ("line.separator") +ownerName+" ? ";
       // title.setText(s);




        progressSweetAlertDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        progressSweetAlertDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        progressSweetAlertDialog.setTitleText("Preparing Call To " + System.getProperty("line.separator") + ownerName );
                progressSweetAlertDialog.setCancelable(false);
       progressSweetAlertDialog.show();


        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        final Timer t = new Timer();
        t.schedule(new TimerTask() {
            public void run() {
//                progressSweetAlertDialog.dismiss();
//                makeCall();

                // when the task active then close the dialog
                  // also just top the timer thread, otherwise, you may receive a crash report
            }
        }, 5000); // after 2 second (or 2000 miliseconds), the task will be active.


        thread = new Thread(){
            @Override
            public void run() {
                try {
                    synchronized (this) {
                        wait(5000);

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                progressSweetAlertDialog.dismiss();
                                makeCall();
                            }
                        });

                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            };
        };
        thread.start();

    }

    @Override
    protected void onStop() {
     //   phone.destory();
       // phone.finalize();
        super.onStop();
    }

    @Override
    protected void onDestroy() {
      //  phone.destory();
      //  phone.finalize();
        super.onDestroy();
    }


    public void makeCall(){

        try {

            phone.connect(contact);


         //   lin1.setBackground(getApplicationContext().getResources().getDrawable(R.drawable.button_background));

         //   dialButton.setEnabled(false);

            title.setText("Call In Progress.....");
        }catch (Exception e){
            e.printStackTrace();
            title.setText("Call Failed");
           // Toast.makeText(getApplicationContext(),"Can't call on this number",Toast.LENGTH_SHORT);
        }

            }

    public void endCall(){

        dialButton.setEnabled(true);
        title.setText("Call Finished");
        phone.disconnect();
    }

    public void errorCall(){

        dialButton.setEnabled(true);
        title.setText("Something Wrong. Try again");
        phone.disconnect();
    }

    @Override
    public void onClick(View view)
    {

       if (view.getId() == R.id.lin2) {

        //    dialButton.setEnabled(true);
            title.setText("Call Finished");
            phone.disconnect();
        }
    }

    public void setData(){



        DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .build();
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getApplicationContext())
                .defaultDisplayImageOptions(defaultOptions)
                .build();
        ImageLoader.getInstance().init(config);



      //  ImageLoader.getInstance().displayImage(ownerPic, image, defaultOptions);




    }

    public void timer(){

        Thread splashTread = new Thread() {
                @Override
                public void run() {
                    try {
                        int waited = 0;
                        while (_active && (waited < _splashTime)) {
                            sleep(100);
                            if (_active) {
                                waited += 100;
                            }
                        }
                    } catch (Exception e) {

                    } finally {

                        phone = new MonkeyPhone(getApplicationContext());

                    }
                };
            };
            splashTread.start();
    }
}